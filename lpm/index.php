<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CANECAS MECOLOUR</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/material-kit.css?v=1.1.0" rel="stylesheet" />

</head>

<body>

	<div class="wrapper">
		<div class="header-1">
			<nav class="navbar navbar-transparent navbar-absolute">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
                		<span class="sr-only">Toggle navigation</span>
    		            <span class="icon-bar"></span>
    		            <span class="icon-bar"></span>
    		            <span class="icon-bar"></span>
            		</button>
						<a class="navbar-brand" href="#"><img src="assets/img/imgLogoMecolour.png" width="150px"></a>
					</div>


				</div>
			</nav>



			<div class="page-header" style="background-color: #ccc">
				<div class="container">
					<div class="text-center">
						<h1> <b>QUEM COMPRA MECOLOUR<Br> VENDE QUALIDADE</b></h1>
						<h5 class="text-center" style="font-weight: 800"> VEJA COMO É FÁCIL ESTAMPAR UMA CANECA </h5>
						<div class="col-md-12 text-center">
							<br><br>
							<iframe width="660" height="370" src="https://www.youtube.com/embed/Nz9kNf0jxug" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							<br><br>
							<div class="alert alert-danger">
								<div class="container-fluid">
									<div class="alert-icon">
										<i class="material-icons">error_outline</i>
									</div>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true"><i class="material-icons">clear</i></span>
  </button>
									<b>ALERTA:</b> É de nosso conhecimento que algumas empresas e lojas estão falsificando nossas CANECAS BRANCAS, por isso, para comprovar a autenticidade de sua caneca, procure o selo oficial mecolour gravado na base.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



	</div>
	<div class="main" style="background-color: #E2973A; padding: 50px">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<img src="assets/img/imgCanecaPremium.png" width="800px" class="hidden-xs" style="right:100px; position:relative">
					<img src="assets/img/imgCanecaPremium.png" class="img-responsive hidden-lg hidden-sm hidden-md">
				</div>
				<div class="col-md-6 text-right pull-right">
					<h1 style="color:#fff"> CANECA <strong>PREMIUM</strong><br><span style="font-size: 20px; color:#fff" class="text-left">QUALIDADE AAA</span></h1>
					<br>
					<p style="color:#fff"> Caneca de cerâmica resinada para sublimação, sem imperfeições, ondulações ou falha na resina, definindo seu bom acabamento e realce de cores, tornando-se referência no mercado.</p>
					<p style="color:#fff"> <strong>Tamanho:</strong> 11oz / 325ml <img src="assets/img/imgTamanho.png" width="20px"></p>
					<p style="color:#fff"> <strong>Material:</strong> Cerâmica <img src="assets/img/imgMaterial.png" width="20px"></p>
					<p style="color:#fff"> <strong>Dimensões Aproximadas do Produto:</strong> A: 9,5 cm * D: 8,2cm <img src="assets/img/imgDimensao.png" width="20px"></p>
					<p style="color:#fff"> <strong>Área de impressão:</strong> 20 * 8.5cm <img src="assets/img/imgArea.png" width="20px"></p>
					<p style="color:#fff"> <strong>Embalagem spec.:</strong> 36pcs/ctn <img src="assets/img/imgEmbalagem.png" width="20px"></p>
					<p style="color:#fff"> <strong>Peso bruto:</strong> 14kgs <img src="assets/img/imgPeso.png" width="20px"></p>
					<a href="https://www.mecolour.com.br/politica-de-venda" class="btn btn-primary btn-md" target="_blank">ONDE COMPRAR</a>
					<a href="https://www.mecolour.com.br/politica-de-venda" data-toggle="modal" data-target="#modalPremium" class="btn btn-success btn-md" target="_blank">REVENDEDOR</a>

				</div>
			</div>
		</div>
	</div>

	<div class="main" style="background-color: #223184; padding: 50px">
		<div class="container">
			<div class="row">
				<div class="col-md-6 text-left">
					<h1 style="color:#fff"> CANECA <strong>ECONOMIC</strong><br><span style="font-size: 20px" class="text-left">QUALIDADE AA</span></h1>
					<br>
					<p style="color:#fff"> Caneca de cerâmica resinada para sublimação, sem imperfeições, ondulações ou falha na resina, definindo seu bom acabamento e realce de cores, tornando-se referência no mercado.</p>
					<p style="color:#fff"> <img src="assets/img/imgTamanho.png" width="20px"> <strong>Tamanho:</strong> 11oz / 325ml </p>
					<p style="color:#fff"> <img src="assets/img/imgMaterial.png" width="20px"> <strong>Material:</strong> Cerâmica </p>
					<p style="color:#fff"> <img src="assets/img/imgDimensao.png" width="20px"> <strong>Dimensões Aproximadas do Produto:</strong> A: 9,5 cm * D: 8,2cm </p>
					<p style="color:#fff"> <img src="assets/img/imgArea.png" width="20px"> <strong>Área de impressão:</strong> 20 * 8.5cm </p>
					<p style="color:#fff"> <img src="assets/img/imgEmbalagem.png" width="20px"> <strong>Embalagem spec.:</strong> 36pcs/ctn </p>
					<p style="color:#fff"> <img src="assets/img/imgPeso.png" width="20px"> <strong>Peso bruto:</strong> 14kgs </p>
					<a href="https://www.mecolour.com.br/politica-de-venda" class="btn btn-primary btn-md" target="_blank">ONDE COMPRAR</a>
					<a href="#" data-toggle="modal" data-target="#modalEconomic" class="btn btn-success btn-md" target="_blank">REVENDEDOR</a>
				</div>
				<div class="col-md-6 text-left">
					<img src="assets/img/imgCanecaEconomic.png" width="900px" style="position: relative; right: 290px;" class="hidden-xs">
					<img src="assets/img/imgCanecaEconomic.png" width="900px" class="img-responsive hidden-lg hidden-sm hidden-md">
				</div>
			</div>
		</div>
	</div>

	</div>

	<div class="modal fade" id="modalEconomic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<i class="material-icons">clear</i>
					</button>
				</div>
				<div class="modal-body">
					<p>
						<div class="card card-contact">
							<form role="form" id="contact-form" method="post">
								<div class="header header-raised header-primary text-center">
									<h4 class="card-title">Caneca Economic</h4>
								</div>
								<div class="card-content">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group label-floating is-empty">
												<label class="control-label">Nome</label>
												<input type="text" name="name" class="form-control">
												<span class="material-input"></span></div>
										</div>
										<div class="col-md-6">
											<div class="form-group label-floating is-empty">
												<label class="control-label">Empresa</label>
												<input type="text" name="name" class="form-control">
												<span class="material-input"></span></div>
										</div>
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Telefone</label>
										<input type="email" name="email" class="form-control">
										<span class="material-input"></span></div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Email</label>
										<input type="email" name="email" class="form-control">
										<span class="material-input"></span></div>

									<div class="row">

										<div class="col-md-12">
											<button type="submit" class="btn btn-primary pull-right">Send Message</button>
										</div>
									</div>
								</div>

							</form>
						</div>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalPremium" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<i class="material-icons">clear</i>
					</button>
				</div>
				<div class="modal-body">
					<p>
						<div class="card card-contact">
							<form role="form" id="contact-form" method="post">
								<div class="header header-raised header-primary text-center">
									<h4 class="card-title">Caneca Economic</h4>
								</div>
								<div class="card-content">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group label-floating is-empty">
												<label class="control-label">Nome</label>
												<input type="text" name="name" class="form-control">
												<span class="material-input"></span></div>
										</div>
										<div class="col-md-6">
											<div class="form-group label-floating is-empty">
												<label class="control-label">Empresa</label>
												<input type="text" name="name" class="form-control">
												<span class="material-input"></span></div>
										</div>
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Telefone</label>
										<input type="email" name="email" class="form-control">
										<span class="material-input"></span></div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Email</label>
										<input type="email" name="email" class="form-control">
										<span class="material-input"></span></div>

									<div class="row">

										<div class="col-md-12">
											<button type="submit" class="btn btn-primary pull-right">Send Message</button>
										</div>
									</div>
								</div>

							</form>
						</div>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>


</body>
<!--   Core JS Files   -->
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js"></script>

<!--    Plugin for Date Time Picker and Full Calendar Plugin   -->
<script src="assets/js/moment.min.js"></script>

<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/   -->
<script src="assets/js/nouislider.min.js" type="text/javascript"></script>

<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker   -->
<script src="assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script>

<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select   -->
<script src="assets/js/bootstrap-selectpicker.js" type="text/javascript"></script>

<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/   -->
<script src="assets/js/bootstrap-tagsinput.js"></script>

<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput   -->
<script src="assets/js/jasny-bootstrap.min.js"></script>

<!--    Plugin For Google Maps   -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!--    Plugin for 3D images animation effect, full documentation here: https://github.com/drewwilson/atvImg    -->
<script src="assets/js/atv-img-animation.js" type="text/javascript"></script>

<!--    Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc    -->
<script src="assets/js/material-kit.js?v=1.1.0" type="text/javascript"></script>

</html>