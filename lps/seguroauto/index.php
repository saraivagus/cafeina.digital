
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Seguro Auto</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <!--     Fonts and icons     -->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,800|Material+Icons' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css" />

        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/lpSeguroAutoNF.css" rel="stylesheet"/>
          <link rel="stylesheet" type="text/css" href="assets/css/lpSeguroAutoNF-form.css" />
    </head>
    <body>
        <nav class="navbar navbar-primary navbar-fixed-top menuTopHome" id="sectionsNav">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a href="#topo">
                        <div class="logo-container">
                            <div class="logo">
                                <img src="assets/img/imgLogoSeguroVeiculoBomCotar.png" alt="BomCotar">
                            </div>
                        </div>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#seguradoras" >
                            <span class="hvr-underline-from-left">Seguradoras</span>
                            </a>
                        </li>
                        <li>
                            <a href="#vantagens">
                            <span class="hvr-underline-from-left">Vantagens</span>
                            </a>
                        </li>
                        <li>
                            <a href="#como-cotar" class="txt">
                            <span class="hvr-underline-from-left">Como cotar</span>
                            </a>
                        </li>
                        <li>
                            <a href="#quem-somos" class="txt">
                            <span class="hvr-underline-from-left">Quem somos</span>
                            </a>
                        </li>
                        <li>
                            <a href="#depoimentos" class="txt">
                            <span class="hvr-underline-from-left">Depoimentos</span>
                            </a>
                        </li>
                        <li class="button-container txt" >
                            <button class="btn btnTopoProposta btn-md  " style="top: 4px">
                              <i class="material-icons">play_circle_outline</i> Iniciar cotação
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="firstStep">
        <div class="wrapper" id="topo">
            <div class="page-header" style="background-image: url('assets/img/bg12.jpg');">
                <div class="container">
                  <?php if(isset($_GET['c'])){ ?>
                    <script type="text/javascript">
                        function sweet() {
                            swal(
                                'Obrigado por sua cotação!',
                                'Em breve nossos consultores entrarão em contato',
                                'success'
                            );
                        }
                        window.onload = sweet;
                    </script>
                <?php } elseif(isset($_GET['e'])){ ?>
                  <script type="text/javascript">
                                         function sweet() {
                                             swal(
                                                 '<?php echo $mensagensAlerta ?>',
                                                 'Tente novamente',
                                                 'error'
                                             );
                                         }
                                         window.onload = sweet;
                                     </script>
                                   <?php }?>



                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="tituloHeader  text-center">TRABALHAMOS COM AS <span class="fw800">MAIORES</span>  <span class="fw800">SEGURADORAS</span> DO MERCADO</h1>
                            <h2 class="tituloHeaderM text-center" style="padding-top:20px">Cote o valor do  <span class="fw800-iso">seguro do seu veículo</span>  agora mesmo<br>
                              <button class="btn btnIniciarCotacao btn-lg " >  <i class="material-icons">play_circle_outline</i> Iniciar cotação</button>
                          </h2>
                            <div class="text-center" style="position: relative;">
                                <img src="assets/img/imgHeaderCarros.png" alt="BomCotar" class="hidden-xs">
                                <img src="assets/img/imgHeaderCarrosMobile.png" alt="BomCotar" class="hidden-lg hidden-sm hidden-md img-responsive" >

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main" >
            <div class="container">
                <div class="features-1">
                    <div class="cd-section text-center" id="seguradoras">
                        <div class="col-md-12">
                            <h2 class="title tituloSeguradoras" style="padding-bottom:20px"> CONFIRA ABAIXO AS  <span class="fw800sub">SEGURADORAS </span>COM QUEM TRABALHAMOS!</h2>
                        </div>
                        <div class="row">
                          <div class="col-lg-3">
                            <img src="assets/img/imgSeguradorasItau.png" alt="BomCotar"><br>
                            <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                            <button class="btn btn-1 btn-xs " id="botaoCarro">COTAR AGORA<div class="ripple-container"></div></button>
                          </div>
                          <div class="col-lg-3">
                            <img src="assets/img/imgSeguradorasAzul.png" alt="BomCotar">
                            <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                            <button class="btn btn-2 btn-xs " id="botaoCarro">COTAR AGORA<div class="ripple-container"></div></button>
                          </div>
                          <div class="col-lg-3">
                            <img src="assets/img/imgSeguradorasPorto.png" alt="BomCotar">
                            <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                            <button class="btn btn-3 btn-xs " id="botaoCarro">COTAR AGORA<div class="ripple-container"></div></button>
                          </div>
                          <div class="col-lg-3">
                            <img src="assets/img/imgSeguradorasGenerali.png" alt="BomCotar">
                            <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                            <button class="btn btn-4 btn-xs " id="botaoCarro">COTAR AGORA<div class="ripple-container"></div></button>
                          </div>

                        </div>
                        <div class="row"  style="padding-top:40px; padding-bottom:40px">

                          <div class="col-lg-3">
                            <img src="assets/img/imgSeguradorasTokio.png" alt="BomCotar">
                            <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                            <button class="btn btn-5 btn-xs " id="botaoCarro">COTAR AGORA<div class="ripple-container"></div></button>
                          </div>
                          <div class="col-lg-3">
                            <img src="assets/img/imgSeguradorasZurich.png" alt="BomCotar">
                            <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                            <button class="btn btn-6 btn-xs " id="botaoCarro">COTAR AGORA<div class="ripple-container"></div></button>
                          </div>
                          <div class="col-lg-3">
                            <img src="assets/img/imgSeguradorasLiberty.png" alt="BomCotar">
                            <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                            <button class="btn btn-7 btn-xs " id="botaoCarro">COTAR AGORA<div class="ripple-container"></div></button>
                          </div>
                          <div class="col-lg-3">
                            <img src="assets/img/imgSeguradorasSulamerica.png" alt="BomCotar">
                            <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                            <button class="btn btn-8 btn-xs " id="botaoCarro">COTAR AGORA<div class="ripple-container"></div></button>
                          </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="main-white">
            <div class="container">
                <div class="features-1">
                    <div class="cd-section text-center" id="vantagens">
                        <div class="col-md-12">
                            <h2 class="title tituloSeguradoras"> VANTAGENS EM COTAR COM A   <span class="fw800sub">BOMCOTAR </span></h2>
                            <h3 class="tituloSeguradorasSub"> Saiba porque é tão   <span class="fw800sub">fácil e rápido </span>cotar conosco.</h3>

                        </div>
                        <div class="row">
                        <div class="col-lg-3 box-vantagens">
                          <img src="assets/img/imgVantagensAgilidade.png" alt="BomCotar"><br>
                          <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                        </div>
                        <div class="col-lg-3 box-vantagens">
                          <img src="assets/img/imgVantagensOpcoes.png" alt="BomCotar"><br>
                          <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                        </div>
                        <div class="col-lg-3 box-vantagens">
                          <img src="assets/img/imgVantagensCoberturas.png" alt="BomCotar"><br>
                          <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                        </div>
                        <div class="col-lg-3 box-vantagens">
                          <img src="assets/img/imgVantagensPersonalizada.png" alt="BomCotar"><br>
                          <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="container">
                <div class="features-1">
                    <div class="cd-section text-center" id="como-cotar">
                        <div class="col-md-12">
                            <h2 class="title tituloSeguradoras" style="padding-bottom:20px"> VEJA COMO É FACIL COTAR COM A <span class="fw800sub">BOMCOTAR </span></h2>

                        </div>
                        <div class="row">
                        <div class="col-lg-3 box-comocotar">
                          <img src="assets/img/imgComoCotar01.png" alt="BomCotar"><br>
                            <h4>FORMULÁRIO </h4>
                          <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                        </div>
                        <div class="col-lg-3 box-comocotar">
                          <img src="assets/img/imgComoCotar02.png" alt="BomCotar"><br>
                            <h4>PROPOSTAS </h4>
                          <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                        </div>
                        <div class="col-lg-3 box-comocotar">
                          <img src="assets/img/imgComoCotar03.png" alt="BomCotar"><br>
                            <h4>ESCOLHA </h4>
                          <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                        </div>
                        <div class="col-lg-3 box-comocotar">
                          <img src="assets/img/imgComoCotar04.png" alt="BomCotar"><br>
                            <h4>PROTEÇÃO </h4>
                          <p class="text-justify descricaoSeguradoras">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus ante non tempor. Proin in posuere libero, at ultricies elit. Ut nec sapien tellus</p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="features-1">
                    <div class="cd-section text-center" id="quem-somos">
                        <div class="col-md-12">
                            <h2 class="title tituloSeguradoras" style="padding-bottom:20px"> QUEM <span class="fw800sub">SOMOS </span> NÓS</h2>

                        </div>
                        <div class="row">
                        <div class="col-lg-5">
                          <img src="assets/img/imgQuemSomosDevice.png" alt="BomCotar" class="hidden-xs"><br>
                        </div>
                        <div class="col-lg-7 text-justify box-quemsomos">
                          Somos a corretora BomCotar, uma plataforma online que possibilita aos consumidores obter cotações de Seguro Auto com as principais corretoras do mercado. Nosso objetivo é aproximar consumidores e corretores a realizarem bons negócios.
<br><bR>
Para tornar esse sonho possível, criamos uma plataforma moderna, que oferece as melhores opções em seguro auto. Opções que cabem no seu bolso! Faça você também sua cotação e comprove a eficiência de nossa plataforma rápida e segura.

                        </div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="main-white bgDepoimentos">
                <div class="container">
                    <div class="features-1">
                        <div class="cd-section text-center" id="depoimentos">

                                <h2 class="title tituloSeguradoras" style="padding-bottom:20px"><span class="fw800sub">DEPOIMENTOS </span>DE NOSSOS CLIENTES</h2>

                            <div class="row" style="padding-top: 20px">
                            <div class="col-md-4">
              	    						<div class="card card-profile">
              	    							<div class="card-avatar">

              	    									<img class="img" src="assets/img/imgDepoimentos01.png">

              	    							</div>
              	    							<div class="card-content">
              	    								<h4 class="card-title">Mussum</h4>
              	    								<p class="card-description">
              	    									Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
              	    								</p>
              	    							</div>
              	    						</div>
	    					            </div>
                            <div class="col-md-4">
              	    						<div class="card card-profile">
              	    							<div class="card-avatar">

              	    									<img class="img" src="assets/img/imgDepoimentos02.png">

              	    							</div>
              	    							<div class="card-content">
              	    								<h4 class="card-title">Didi</h4>
              	    								<p class="card-description">
              	    									Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
              	    								</p>
              	    							</div>
              	    						</div>
	    					            </div>
                            <div class="col-md-4">
              	    						<div class="card card-profile">
              	    							<div class="card-avatar">

              	    									<img class="img" src="assets/img/imgDepoimentos03.png">

              	    							</div>
              	    							<div class="card-content">
              	    								<h4 class="card-title">Zacarias</h4>
              	    								<p class="card-description">
              	    									Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
              	    								</p>
              	    							</div>
              	    						</div>
	    					            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<div class="secondStep hide">
  <nav class="navbar navbar-primary navbar-fixed-top " id="sectionsNav">
      <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
              <a href="#topo">
                  <div class="logo-container">
                      <div class="logo">
                          <img src="assets/img/imgLogoSeguroVeiculoBomCotar.png" alt="BomCotar">
                      </div>
                  </div>
              </a>
          </div>
          <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">

                  <li class="button-container txt" >
                      <button onclick="history.go(0)" class="btn btnTopoProposta btn-md  " style="top: 4px">
                        <i class="material-icons">refresh</i> Voltar
                      </button>
                  </li>
              </ul>
          </div>
      </div>
  </nav>
<div class="wrapper" id="topo">
    <div class="page-header">
        <div class="container">


          <div class="features-1">
              <div class="cd-section text-center" id="vantagens">
                  <div class="col-md-12">
                      <h2 class="tituloform"> SELECIONE O <span class="fw800sub">TIPO DO VEÍCULO</span> QUE VOCÊ DESEJA COTAR   </h2>

                  </div>
                  <div class="row">
                  <div class="col-lg-4 box-form-seleciona-veiculo">
                    <img src="assets/img/imgEscolhaCotacaoCarro.png" alt="BomCotar"><br>
                    <p class="text-center descricaoSeguradoras">Quero cotar para o meu carro</p>
                    <button class="btn btnTopoProposta btn-md btnIniciaCotacaoCarro" id="botaoCarro" style="top: 4px">
                      <i class="material-icons">play_circle_outline</i> Iniciar cotação
                    </button>
                  </div>
                  <div class="col-lg-4 box-form-seleciona-veiculo">
                    <img src="assets/img/imgEscolhaCotacaoMoto.png" alt="BomCotar"><br>
                    <p class="text-center descricaoSeguradoras">Quero cotar para a minha moto</p>
                    <button class="btn btnTopoProposta btn-md  " style="top: 4px">
                      <i class="material-icons">play_circle_outline</i> Iniciar cotação
                    </button>
                  </div>
                  <div class="col-lg-4 box-form-seleciona-veiculo">
                    <img src="assets/img/imgEscolhaCotacaoOutros.png" alt="BomCotar"><br>
                    <p class="text-center descricaoSeguradoras">Tenho outro tipo de veículo</p>
                    <button class="btn btnTopoProposta btn-md  " style="top: 4px">
                      <i class="material-icons">play_circle_outline</i> Iniciar cotação
                    </button>
                  </div>

                </div>
              </div>
          </div>

        </div>
    </div>
</div>
</div>
<div class="thirdStep hide">
  <div class="container-form">
  <div class="fs-form-wrap" id="fs-form-wrap">
  <div class="fs-title">
      <div class="codrops-top">
          <a class="codrops-icon codrops-icon-prev btnVoltaCategoria" href="javascript:void(0)"></a>
      </div>
      <h1><img src="assets/img/imgLogoSeguroVeiculoBomCotar.png" class="bomcotarlogo" width="100" height="auto" alt="LOGO">Seguro Auto</h1>
      <span class="small_head">Selecionou a categoria errada? <a href="#" class="btnVoltaCategorias btnVoltaCategoria">Voltar</a></span>
  </div>
  <form id="myform" class="fs-form fs-form-full" autocomplete="off" method="post" action="#">
      <ol class="fs-fields">
          <li>
              <label class="fs-field-label fs-anim-upper" for="strMarca">Qual a marca do seu carro?</label>
              <input class="fs-anim-lower" id="q1" name="strMarca" type="text" placeholder="Ex. Honda" required/> <br>
              <label class="fs-field-label fs-anim-upper" for="strModelo">Qual o modelo?</label>
              <input class="fs-anim-lower" id="q2" name="strModelo" type="text" placeholder="Ex. Honda Fit EX" required/> <br>
              <label class="fs-field-label fs-anim-upper" for="strAno">Qual o ano?</label>
              <input class="fs-anim-lower" id="q3" name="strAno" type="text" placeholder="Ex. 2009" required/>
          </li>
          <li data-input-trigger>
              <label class="fs-field-label fs-anim-upper" for="possuiveiculo">Você já possui o veículo ?</label>
              <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                  <span><input id="possuisim" name="q1" type="radio" value="sim" required tabindex="4"/><label class="radio-conversion" for="possuisim" >Sim</label></span>
                  <span><input id="possuinao" name="q1" type="radio" value="não" required  tabindex="5"/><label class="radio-social" for="possuinao" >Não</label></span>
              </div>
          </li>
          <li data-input-trigger>

                  <label class="fs-field-label fs-anim-upper " for="renovacaoveiculo">Este seguro é uma renovação ?</label>
                  <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower ">
                      <span><input id="renovasim" name="q2" type="radio" value="Sim" tabindex="12" required="required"/><label class="radio-conversion" for="renovasim" >Sim</label></span>
                      <span><input id="renovanao" name="q2" type="radio" value="Não" tabindex="13" required="required"/><label class="radio-social" for="renovanao" >Não</label></span>
                    </div>
          </li>
          <li data-input-trigger>
              <label class="fs-field-label fs-anim-upper" for="strSexo">Qual o seu sexo?</label>
              <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                  <span><input id="strSexoMasc" name="q3" type="radio" value="sim" required tabindex="17"/><label class="radio-boy" for="strSexoMasc" >Masculino</label></span>
                  <span><input id="strSexoFem" name="q3" type="radio" value="não" required  tabindex="18"/><label class="radio-girl" for="strSexoFem" >feminino</label></span>
              </div>
          </li>
          <li>
          <label class="fs-field-label fs-anim-upper" for="q2" data-info="Preencha seus dados pessoais">Dados pessoais</label>
          <input class="fs-anim-lower" id="strNome" name="strNome" type="text" placeholder="Nome Completo" required/><br>
          <input class="fs-anim-lower" id="strEmail" name="strEmail" type="email" placeholder="E-mail" required/><br>
          <input class="fs-anim-lower date" id="strDataNascimento" name="strDataNascimento" type="text" placeholder="Data de Nascimento" required/><br>
          <input class="fs-anim-lower phone_with_ddd" id="strTelefoneMovel" name="strTelefoneMovel" type="text" placeholder="Telefone Celular" required/><br>
          <input class="fs-anim-lower fixo_with_ddd" id="strTelefoneFixo" name="strTelefoneFixo" type="text" placeholder="Telefone Fixo"/><br>
          <input class="fs-anim-lower cep" id="strCEP" name="strCEP" type="text" placeholder="CEP" required/><br>
          <input class="fs-anim-lower cpf" id="strCPF" name="strCPF" type="text" placeholder="CPF" required/><br>
          <input class="fs-anim-lower" id="strEstado" name="strEstado" type="text" placeholder="Estado" required/><br>
          <input class="fs-anim-lower" id="strCidade" name="strCidade" type="text" placeholder="Cidade" required/><br>
          </li>
      </ol>
      <!-- /fs-fields -->
      <button class="fs-submit" type="submit">Enviar</button>
  </form>
  <!-- /fs-form -->
  </div><!-- /fs-form-wrap -->
  </div><!-- /container -->
</div>

        <!-- <div class="footer text-center">
            <div class="container">
                <div class="row padding-content ">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                        <input type="checkbox" name="optionsCheckboxes" checked> <span style="color: #808186; font-size: 10px; padding-top:20px; padding-bottom: 10px">Ao informar os seus dados nesta página, você concorda que a BomCotar, possa utilizá-los para o envio de ofertas de produtos e serviços de parceiros, comprometendo-se a mesma a resguardar e proteger a privacidade destas informações.</span>
                    </div>
                </div>
            </div>
        </div> -->

    </body>
<script src="assets/js/modernizr.custom.js"></script>
    <!--   Core JS Files   -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/selectFx.js"></script>
    <script src="assets/js/fullscreenForm.js"></script>
    <script src="assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/material.min.js"></script>
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/nouislider.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-selectpicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-tagsinput.js"></script>
    <script src="assets/js/jasny-bootstrap.min.js"></script>
    <script src="assets/js/atv-img-animation.js" type="text/javascript"></script>
    <script src="assets/js/material-kit.js?v=1.1.0" type="text/javascript"></script>
    <script src="assets/js/scroll.js"></script>

        <script>
            (function() {
              var formWrap = document.getElementById( 'fs-form-wrap' );

              [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
                new SelectFx( el, {
                  stickyPlaceholder: false,
                  onChange: function(val){
                    document.querySelector('span.cs-placeholder').style.backgroundColor = val;
                  }
                });
              } );

              new FForm( formWrap, {
                onReview : function() {
                  classie.add( document.body, 'overview' ); // for demo purposes only
                }
              } );
            })();
        </script>
        <script>
            $(document).ready(function(){
            $('.date').mask('00/00/0000');
            $('.cep').mask('00000-000');
            $('.phone_with_ddd').mask('(00) 00000-0000');
            $('.fixo_with_ddd').mask('(00) 0000-0000');
            $('.cpf').mask('000.000.000-00', {reverse: true});

            });
        </script>
<script>
    $(document).ready(function() {

        $('.btnTopoProposta').click(function(){
          $('.firstStep').addClass('hide');
          $('.secondStep').removeClass('hide');
          $('.menuTopHome').addClass('hide');
        });
        $('.btnIniciarCotacao').click(function(){
          $('.firstStep').addClass('hide');
          $('.secondStep').removeClass('hide');
          $('.menuTopHome').addClass('hide');
        });
        $('.btnIniciaCotacaoCarro').click(function(){
          $('.firstStep').addClass('hide');
          $('.secondStep').addClass('hide');
          $('.thirdStep').removeClass('hide');
        });
        $('.btnVoltaCategoria').click(function(){
          $('.firstStep').addClass('hide');
          $('.secondStep').removeClass('hide');
          $('.thirdStep').addClass('hide');
        });
    });
</script>
</html>
