<?php

try{
    $conexao = new PDO('mysql:host=localhost;dbname=cafeina_mecolour', 'cafeina_mecolour', 'DB13579**#');
    $conexao ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo 'ERROR:' . $e->getMessage();


};
if(isset($_POST['cadastrarcf'])){
    $dbClassNome = trim(strip_tags($_POST['dbClassNome']));
    $dbClassEmail = trim(strip_tags($_POST['dbClassEmail']));
    $dbClassInsumosCompraMC = trim(strip_tags($_POST['dbClassInsumosCompraMC']));
    $dbClassInsumosEntregaMC = trim(strip_tags($_POST['dbClassInsumosEntregaMC']));
    $dbClassInsumosSuporteMC = trim(strip_tags($_POST['dbClassInsumosSuporteMC']));
    $dbClassInsumosQualidadeMC = trim(strip_tags($_POST['dbClassInsumosQualidadeMC']));
    $dbClassInsumosCompraOT = trim(strip_tags($_POST['dbClassInsumosCompraOT']));
    $dbClassInsumosEntregaOT = trim(strip_tags($_POST['dbClassInsumosEntregaOT']));
    $dbClassInsumosSuporteOT = trim(strip_tags($_POST['dbClassInsumosSuporteOT']));
    $dbClassInsumosQualidadeOT = trim(strip_tags($_POST['dbClassInsumosQualidadeOT']));
    $dbClassMaquinasCompraMC = trim(strip_tags($_POST['dbClassMaquinasCompraMC']));
    $dbClassMaquinasEntregaMC = trim(strip_tags($_POST['dbClassMaquinasEntregaMC']));
    $dbClassMaquinasSuporteMC = trim(strip_tags($_POST['dbClassMaquinasSuporteMC']));
    $dbClassMaquinasQualidadeMC = trim(strip_tags($_POST['dbClassMaquinasQualidadeMC']));
    $dbClassMaquinasCompraOT = trim(strip_tags($_POST['dbClassMaquinasCompraOT']));
    $dbClassMaquinasEntregaOT = trim(strip_tags($_POST['dbClassMaquinasEntregaOT']));
    $dbClassMaquinasSuporteOT = trim(strip_tags($_POST['dbClassMaquinasSuporteOT']));
    $dbClassMaquinasQualidadeOT = trim(strip_tags($_POST['dbClassMaquinasQualidadeOT']));
    $dbClassSuporteTecnicoMC = trim(strip_tags($_POST['dbClassSuporteTecnicoMC']));
    $dbClassAtendimentoMC = trim(strip_tags($_POST['dbClassAtendimentoMC']));
    $dbClassSuporteTecnicoOT = trim(strip_tags($_POST['dbClassSuporteTecnicoOT']));
    $dbClassAtendimentoOT = trim(strip_tags($_POST['dbClassAtendimentoOT']));


    $insert = "INSERT INTO dbclassrv (dbClassNome, dbClassEmail, dbClassInsumosCompraMC, dbClassInsumosEntregaMC, dbClassInsumosSuporteMC, dbClassInsumosQualidadeMC, dbClassInsumosCompraOT, dbClassInsumosEntregaOT, dbClassInsumosSuporteOT, dbClassInsumosQualidadeOT, dbClassMaquinasCompraMC, dbClassMaquinasEntregaMC, dbClassMaquinasSuporteMC, dbClassMaquinasQualidadeMC, dbClassMaquinasCompraOT, dbClassMaquinasEntregaOT, dbClassMaquinasSuporteOT, dbClassMaquinasQualidadeOT, dbClassSuporteTecnicoMC, dbClassAtendimentoMC, dbClassSuporteTecnicoOT, dbClassAtendimentoOT ) VALUES (:dbClassNome, :dbClassEmail, :dbClassInsumosCompraMC, :dbClassInsumosEntregaMC, :dbClassInsumosSuporteMC, :dbClassInsumosQualidadeMC, :dbClassInsumosCompraOT, :dbClassInsumosEntregaOT, :dbClassInsumosSuporteOT, :dbClassInsumosQualidadeOT, :dbClassMaquinasCompraMC, :dbClassMaquinasEntregaMC, :dbClassMaquinasSuporteMC, :dbClassMaquinasQualidadeMC, :dbClassMaquinasCompraOT, :dbClassMaquinasEntregaOT, :dbClassMaquinasSuporteOT, :dbClassMaquinasQualidadeOT, :dbClassSuporteTecnicoMC, :dbClassAtendimentoMC, :dbClassSuporteTecnicoOT, :dbClassAtendimentoOT  )";
    try{

        $result = $conexao->prepare($insert);
        $result->bindParam(':dbClassNome', $dbClassNome, PDO::PARAM_STR);
        $result->bindParam(':dbClassEmail', $dbClassEmail, PDO::PARAM_STR);
        $result->bindParam(':dbClassInsumosCompraMC', $dbClassInsumosCompraMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassInsumosEntregaMC', $dbClassInsumosEntregaMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassInsumosSuporteMC', $dbClassInsumosSuporteMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassInsumosQualidadeMC', $dbClassInsumosQualidadeMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassInsumosCompraOT', $dbClassInsumosCompraOT, PDO::PARAM_STR);
        $result->bindParam(':dbClassInsumosEntregaOT', $dbClassInsumosEntregaOT, PDO::PARAM_STR);
        $result->bindParam(':dbClassInsumosSuporteOT', $dbClassInsumosSuporteOT, PDO::PARAM_STR);
        $result->bindParam(':dbClassInsumosQualidadeOT', $dbClassInsumosQualidadeOT, PDO::PARAM_STR);
        $result->bindParam(':dbClassMaquinasCompraMC', $dbClassMaquinasCompraMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassMaquinasEntregaMC', $dbClassMaquinasEntregaMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassMaquinasSuporteMC', $dbClassMaquinasSuporteMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassMaquinasQualidadeMC', $dbClassMaquinasQualidadeMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassMaquinasCompraOT', $dbClassMaquinasCompraOT, PDO::PARAM_STR);
        $result->bindParam(':dbClassMaquinasEntregaOT', $dbClassMaquinasEntregaOT, PDO::PARAM_STR);
        $result->bindParam(':dbClassMaquinasSuporteOT', $dbClassMaquinasSuporteOT, PDO::PARAM_STR);
        $result->bindParam(':dbClassMaquinasQualidadeOT', $dbClassMaquinasQualidadeOT, PDO::PARAM_STR);
        $result->bindParam(':dbClassSuporteTecnicoMC', $dbClassSuporteTecnicoMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassAtendimentoMC', $dbClassAtendimentoMC, PDO::PARAM_STR);
        $result->bindParam(':dbClassSuporteTecnicoOT', $dbClassSuporteTecnicoOT, PDO::PARAM_STR);
        $result->bindParam(':dbClassAtendimentoOT', $dbClassAtendimentoOT, PDO::PARAM_STR);



        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                echo ' <script type="text/javascript">
                        function sweet() {
                            swal(
                                \'Sucesso!\',
                                \'Pesquisa eviada com êxito\',
                                \'success\'
                            );
                        }
                        window.onload = sweet;
                    </script>';
            }
        }else{
            echo '  <script type="text/javascript">
                                         function sweet() {
                                             swal(
                                                 \'Ops, algo de errado\',
                                                 \'contate o suporte \',
                                                 \'error\'
                                             );
                                         }
                                         window.onload = sweet;
                                     </script>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mecolour Pesquisa Qualitativa</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- App css -->
    <!-- Sweet Alert css -->
    <link href="assets/plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>
</head>
<body>
<div id="wrapper">
    <div class="content-page">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <a href="index.php"> <h4 class="m-t-0 header-title text-center"><img src="assets/images/logo.png" width="200px"></h4></a>
                            <p class="text-muted m-b-30 font-14 text-center">
                                PESQUISA QUALITATIVA MECOLOUR
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <form class="form-signin" action="#" method="post" enctype="multipart/form-data">

                <div class="col-md-12 text-center">
                    <div class="card-box">

                            <div class="col-lg-12 col-xl-12">
                                <div class="file-man-box">
                                    <h3 style="font-size: 20px;" > Pensando no Mercado de sublimação de forma geral, classifique os itens abaixo de 1 a 5.<br> Qual sua avaliação para acesso aos seguintes componentes necessários para o negócio ? </h3>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="strNome" class="col-form-label pull-left" style="font-size: 15px">Nome</label>
                                            <input type="text" class="form-control" id="strNome" name="dbClassNome" placeholder="Insira seu Nome" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="strEmail" class="col-form-label pull-left" style="font-size: 15px">E-mail</label>
                                            <input type="text" class="form-control" id="strEmail" name="dbClassEmail" placeholder="Insira seu E-mail" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                        <h3 style="font-size: 20px;" > MECOLOUR - Insumos </h3>

                                    <div class="mt-2">
                                        <p class="text-muted font-15 mt-3 mb-3">Compra</p>

                                        <div class="checkbox checkbox-danger form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoCompra1MC" name="dbClassInsumosCompraMC" value="1">
                                            <label for="checkboxInsumoCompra1MC"> 1 </label>
                                        </div>
                                        <div class="checkbox checkbox-warning form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoCompra2MC" name="dbClassInsumosCompraMC" value="2">
                                            <label for="checkboxInsumoCompra2MC"> 2 </label>
                                        </div>
                                        <div class="checkbox checkbox-info form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoCompra3MC" name="dbClassInsumosCompraMC" value="3">
                                            <label for="checkboxInsumoCompra3MC"> 3 </label>
                                        </div>
                                        <div class="checkbox checkbox-custom form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoCompra4MC" name="dbClassInsumosCompraMC" value="4">
                                            <label for="checkboxInsumoCompra4MC"> 4 </label>
                                        </div>
                                        <div class="checkbox checkbox-success form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoCompra5MC" name="dbClassInsumosCompraMC" value="5">
                                            <label for="checkboxInsumoCompra5MC"> 5 </label>
                                        </div>

                                    </div>
                                    <div class="mt-2">
                                        <p class="text-muted font-15 mt-3 mb-3">Entrega</p>

                                        <div class="checkbox checkbox-danger form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoEntrega1MC" name="dbClassInsumosEntregaMC" value="1">
                                            <label for="checkboxInsumoEntregaMC1"> 1 </label>
                                        </div>
                                        <div class="checkbox checkbox-warning form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoEntrega2MC" name="dbClassInsumosEntregaMC" value="2">
                                            <label for="checkboxInsumoEntrega2MC"> 2 </label>
                                        </div>
                                        <div class="checkbox checkbox-info form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoEntrega3MC" name="dbClassInsumosEntregaMC" value="3">
                                            <label for="checkboxInsumoEntrega3MC"> 3 </label>
                                        </div>
                                        <div class="checkbox checkbox-custom form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoEntrega4MC" name="dbClassInsumosEntregaMC" value="4">
                                            <label for="checkboxInsumoEntrega4MC"> 4 </label>
                                        </div>
                                        <div class="checkbox checkbox-success form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoEntrega5MC" name="dbClassInsumosEntregaMC" value="5">
                                            <label for="checkboxInsumoEntrega5MC"> 5 </label>
                                        </div>

                                    </div>
                                    <div class="mt-2">
                                        <p class="text-muted font-15 mt-3 mb-3">Suporte</p>

                                        <div class="checkbox checkbox-danger form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoSuporte1MC" name="dbClassInsumosSuporteMC" value="1">
                                            <label for="checkboxInsumoSuporte1MC"> 1 </label>
                                        </div>
                                        <div class="checkbox checkbox-warning form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoSuporte2MC" name="dbClassInsumosSuporteMC" value="2">
                                            <label for="checkboxInsumoSuporte3MC"> 2 </label>
                                        </div>
                                        <div class="checkbox checkbox-info form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoSuporte4MC" name="dbClassInsumosSuporteMC" value="3">
                                            <label for="checkboxInsumoSuporte4MC"> 3 </label>
                                        </div>
                                        <div class="checkbox checkbox-custom form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoSuporte4MC" name="dbClassInsumosSuporteMC" value="4">
                                            <label for="checkboxInsumoSuporte4MC"> 4 </label>
                                        </div>
                                        <div class="checkbox checkbox-success form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoSuporte5MC" name="dbClassInsumosSuporteMC" value="5">
                                            <label for="checkboxInsumoSuporte5MC"> 5 </label>
                                        </div>

                                    </div>
                                    <div class="mt-2">
                                        <p class="text-muted font-15 mt-3 mb-3">Qualidade</p>

                                        <div class="checkbox checkbox-danger form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoQualidade1MC" name="dbClassInsumosQualidadeMC" value="1">
                                            <label for="checkboxInsumoQualidade1MC"> 1 </label>
                                        </div>
                                        <div class="checkbox checkbox-warning form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoQualidade2MC" name="dbClassInsumosQualidadeMC"  value="2">
                                            <label for="checkboxInsumoQualidade2MC"> 2 </label>
                                        </div>
                                        <div class="checkbox checkbox-info form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoQualidade3MC" name="dbClassInsumosQualidadeMC" value="3">
                                            <label for="checkboxInsumoQualidade3MC"> 3 </label>
                                        </div>
                                        <div class="checkbox checkbox-custom form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoQualidade4MC" name="dbClassInsumosQualidadeMC" value="4">
                                            <label for="checkboxInsumoQualidade4MC"> 4 </label>
                                        </div>
                                        <div class="checkbox checkbox-success form-check-inline">
                                            <input type="checkbox" id="checkboxInsumoQualidade5MC" name="dbClassInsumosQualidadeMC" value="5">
                                            <label for="checkboxInsumoQualidade5MC"> 5 </label>
                                        </div>

                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h3 style="font-size: 20px;" > OUTROS - Insumos </h3>

                                        <div class="mt-2">
                                            <p class="text-muted font-15 mt-3 mb-3">Compra</p>

                                            <div class="checkbox checkbox-danger form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoCompra1OT" name="dbClassInsumosCompraOT" value="1">
                                                <label for="checkboxInsumoCompra1OT"> 1 </label>
                                            </div>
                                            <div class="checkbox checkbox-warning form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoCompra2OT" name="dbClassInsumosCompraOT" value="2">
                                                <label for="checkboxInsumoCompra2OT"> 2 </label>
                                            </div>
                                            <div class="checkbox checkbox-info form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoCompra3OT" name="dbClassInsumosCompraOT" value="3">
                                                <label for="checkboxInsumoCompra3OT"> 3 </label>
                                            </div>
                                            <div class="checkbox checkbox-custom form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoCompra4OT" name="dbClassInsumosCompraOT" value="4">
                                                <label for="checkboxInsumoCompra4OT"> 4 </label>
                                            </div>
                                            <div class="checkbox checkbox-success form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoCompra5OT" name="dbClassInsumosCompraOT" value="5">
                                                <label for="checkboxInsumoCompra5OT"> 5 </label>
                                            </div>

                                        </div>
                                        <div class="mt-2">
                                            <p class="text-muted font-15 mt-3 mb-3">Entrega</p>

                                            <div class="checkbox checkbox-danger form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoEntrega1OT" name="dbClassInsumosEntregaOT" value="1">
                                                <label for="checkboxInsumoEntrega1OT"> 1 </label>
                                            </div>
                                            <div class="checkbox checkbox-warning form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoEntrega2OT" name="dbClassInsumosEntregaOT" value="2">
                                                <label for="checkboxInsumoEntrega2OT"> 2 </label>
                                            </div>
                                            <div class="checkbox checkbox-info form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoEntrega3OT" name="dbClassInsumosEntregaOT" value="3">
                                                <label for="checkboxInsumoEntrega3OT"> 3 </label>
                                            </div>
                                            <div class="checkbox checkbox-custom form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoEntrega4OT" name="dbClassInsumosEntregaOT" value="4">
                                                <label for="checkboxInsumoEntrega4OT"> 4 </label>
                                            </div>
                                            <div class="checkbox checkbox-success form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoEntrega5OT" name="dbClassInsumosEntregaOT" value="5">
                                                <label for="checkboxInsumoEntrega5OT"> 5 </label>
                                            </div>

                                        </div>
                                        <div class="mt-2">
                                            <p class="text-muted font-15 mt-3 mb-3">Suporte</p>

                                            <div class="checkbox checkbox-danger form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoSuporte1OT" name="dbClassInsumosSuporteOT" value="1">
                                                <label for="checkboxInsumoSuporte1OT"> 1 </label>
                                            </div>
                                            <div class="checkbox checkbox-warning form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoSuporte2OT" name="dbClassInsumosSuporteOT" value="2">
                                                <label for="checkboxInsumoSuporte2OT"> 2 </label>
                                            </div>
                                            <div class="checkbox checkbox-info form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoSuporte3OT" name="dbClassInsumosSuporteOT" value="3">
                                                <label for="checkboxInsumoSuporte3OT"> 3 </label>
                                            </div>
                                            <div class="checkbox checkbox-custom form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoSuporte4OT" name="dbClassInsumosSuporteOT" value="4">
                                                <label for="checkboxInsumoSuporte4OT"> 4 </label>
                                            </div>
                                            <div class="checkbox checkbox-success form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoSuporte5OT"  name="dbClassInsumosSuporteOT" value="5">
                                                <label for="checkboxInsumoSuporte5OT"> 5 </label>
                                            </div>

                                        </div>
                                        <div class="mt-2">
                                            <p class="text-muted font-15 mt-3 mb-3">Qualidade</p>

                                            <div class="checkbox checkbox-danger form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoQualidade1OT" name="dbClassInsumosQualidadeOT" value="1">
                                                <label for="checkboxInsumoQualidade1OT"> 1 </label>
                                            </div>
                                            <div class="checkbox checkbox-warning form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoQualidade2OT" name="dbClassInsumosQualidadeOT" value="2">
                                                <label for="checkboxInsumoQualidade2OT"> 2 </label>
                                            </div>
                                            <div class="checkbox checkbox-info form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoQualidade3OT" name="dbClassInsumosQualidadeOT" value="3">
                                                <label for="checkboxInsumoQualidade3OT"> 3 </label>
                                            </div>
                                            <div class="checkbox checkbox-custom form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoQualidade4OT" name="dbClassInsumosQualidadeOT" value="4">
                                                <label for="checkboxInsumoQualidade4OT"> 4 </label>
                                            </div>
                                            <div class="checkbox checkbox-success form-check-inline">
                                                <input type="checkbox" id="checkboxInsumoQualidade5OT" name="dbClassInsumosQualidadeOT" value="5">
                                                <label for="checkboxInsumoQualidade5OT"> 5 </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                    <!-- MAQUINAS -->

                                    <div class="row mt-4">
                                        <div class="col-md-6">
                                            <h3 style="font-size: 20px;" > MECOLOUR - Máquinas </h3>

                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Compra</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaCompra1MC" name="dbClassMaquinasCompraMC" value="1">
                                                    <label for="checkboxMaquinaCompra1MC"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaCompra2MC" name="dbClassMaquinasCompraMC" value="2">
                                                    <label for="checkboxMaquinaCompra2MC"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaCompra3MC" name="dbClassMaquinasCompraMC" value="3">
                                                    <label for="checkboxMaquinaCompra3MC"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaCompra4MC" name="dbClassMaquinasCompraMC" value="4">
                                                    <label for="checkboxMaquinaCompra4MC"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaCompra5MC" name="dbClassMaquinasCompraMC" value="5">
                                                    <label for="checkboxMaquinaCompra5MC"> 5 </label>
                                                </div>

                                            </div>
                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Entrega</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaEntrega1MC" name="dbClassMaquinasEntregaMC" value="1">
                                                    <label for="checkboxMaquinaEntrega1MC"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaEntrega2MC" name="dbClassMaquinasEntregaMC" value="2">
                                                    <label for="checkboxMaquinaEntrega2MC"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaEntrega3MC" name="dbClassMaquinasEntregaMC" value="3">
                                                    <label for="checkboxMaquinaEntrega3MC"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaEntrega1MC" name="dbClassMaquinasEntregaMC" value="4">
                                                    <label for="checkboxMaquinaEntrega4MC"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaEntrega5MC" name="dbClassMaquinasEntregaMC" value="5">
                                                    <label for="checkboxMaquinaEntrega5MC"> 5 </label>
                                                </div>

                                            </div>
                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Suporte</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaSuporte1MC" name="dbClassMaquinasSuporteMC" value="1">
                                                    <label for="checkboxMaquinaSuporte1MC"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaSuporte2MC" name="dbClassMaquinasSuporteMC"  value="2">
                                                    <label for="checkboxMaquinaSuporte2MC"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaSuporte3MC" name="dbClassMaquinasSuporteMC" value="3">
                                                    <label for="checkboxMaquinaSuporte3MC"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaSuporte4MC" name="dbClassMaquinasSuporteMC" value="4">
                                                    <label for="checkboxMaquinaSuporte4MC"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaSuporte5MC" name="dbClassMaquinasSuporteMC" value="5">
                                                    <label for="checkboxMaquinaSuporte5MC"> 5 </label>
                                                </div>

                                            </div>
                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Qualidade</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaQualidade1MC" name="dbClassMaquinasQualidadeMC" value="1">
                                                    <label for="checkboxMaquinaQualidade1MC"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaQualidade2MC" name="dbClassMaquinasQualidadeMC" value="2">
                                                    <label for="checkboxMaquinaQualidade2MC"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaQualidade3MC" name="dbClassMaquinasQualidadeMC" value="3">
                                                    <label for="checkboxMaquinaQualidade3MC"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaQualidade4MC" name="dbClassMaquinasQualidadeMC" value="4">
                                                    <label for="checkboxMaquinaQualidade4MC"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="checkboxMaquinaQualidade5MC" name="dbClassMaquinasQualidadeMC" value="5">
                                                    <label for="checkboxMaquinaQualidade5MC"> 5 </label>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 20px;" > OUTROS - Máquinas </h3>

                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Compra</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox1aa" name="dbClassMaquinasCompraOT" value="1">
                                                    <label for="inlineCheckbox1aa"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox2aa" name="dbClassMaquinasCompraOT" value="2">
                                                    <label for="inlineCheckbox2aa"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox3aa" name="dbClassMaquinasCompraOT" value="3">
                                                    <label for="inlineCheckbox3aa"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox4aa" name="dbClassMaquinasCompraOT" value="4">
                                                    <label for="inlineCheckbox4a"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox5aa" name="dbClassMaquinasCompraOT" value="5">
                                                    <label for="inlineCheckbox5aa"> 5 </label>
                                                </div>

                                            </div>
                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Entrega</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox1bb" name="dbClassMaquinasEntregaOT" value="1">
                                                    <label for="inlineCheckbox1bb"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox2bb" name="dbClassMaquinasEntregaOT" value="2">
                                                    <label for="inlineCheckbox2bb"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox3bb" name="dbClassMaquinasEntregaOT" value="3">
                                                    <label for="inlineCheckbox3bb"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox4bb" name="dbClassMaquinasEntregaOT" value="4">
                                                    <label for="inlineCheckbox4bb"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox5bb" name="dbClassMaquinasEntregaOT" value="5">
                                                    <label for="inlineCheckbox5bb"> 5 </label>
                                                </div>

                                            </div>
                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Suporte</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox1cc" name="dbClassMaquinasSuporteOT" value="1">
                                                    <label for="inlineCheckbox1cc"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox2cc" name="dbClassMaquinasSuporteOT" value="2">
                                                    <label for="inlineCheckbox2cc"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox3cc" name="dbClassMaquinasSuporteOT" value="3">
                                                    <label for="inlineCheckbox3cc"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox4cc" name="dbClassMaquinasSuporteOT" value="4">
                                                    <label for="inlineCheckbox4cc"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox5cc" name="dbClassMaquinasSuporteOT" value="5">
                                                    <label for="inlineCheckbox5cc"> 5 </label>
                                                </div>

                                            </div>
                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Qualidade</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox1dd" name="dbClassMaquinasQualidadeOT" value="1">
                                                    <label for="inlineCheckbox1dd"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox2dd" name="dbClassMaquinasQualidadeOT" value="2">
                                                    <label for="inlineCheckbox2dd"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox3dd" name="dbClassMaquinasQualidadeOT" value="3">
                                                    <label for="inlineCheckbox3dd"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox4dd" name="dbClassMaquinasQualidadeOT" value="4">
                                                    <label for="inlineCheckbox4dd"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="inlineCheckbox5dd" name="dbClassMaquinasQualidadeOT" value="5">
                                                    <label for="inlineCheckbox5dd"> 5 </label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <!-- MAQUINAS -->

                                    <div class="row mt-4">
                                        <div class="col-md-6">
                                            <h3 style="font-size: 20px;" > MECOLOUR  </h3>

                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Suporte Técnico</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote1MC" name="dbClassSuporteTecnicoMC" value="1">
                                                    <label for="checkboxSuporote1MC"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote2MC" name="dbClassSuporteTecnicoMC" value="2">
                                                    <label for="checkboxSuporote2MC"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote3MC" name="dbClassSuporteTecnicoMC" value="3">
                                                    <label for="checkboxSuporote3MC"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote4MC" name="dbClassSuporteTecnicoMC" value="4">
                                                    <label for="checkboxSuporote4MC"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote5MC" name="dbClassSuporteTecnicoMC" value="5">
                                                    <label for="checkboxSuporote5MC"> 5 </label>
                                                </div>

                                            </div>
                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Atendimento</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento1MC" name="dbClassAtendimentoMC" value="1">
                                                    <label for="checkboxAtendimento1MC"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento2MC" name="dbClassAtendimentoMC" value="2">
                                                    <label for="checkboxAtendimento2MC"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento3MC" name="dbClassAtendimentoMC" value="3">
                                                    <label for="checkboxAtendimento3MC"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento4MC" name="dbClassAtendimentoMC" value="4">
                                                    <label for="checkboxAtendimento4MC"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento5MC" name="dbClassAtendimentoMC" value="5">
                                                    <label for="checkboxAtendimento5MC"> 5 </label>
                                                </div>

                                            </div>


                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 20px;" > OUTROS </h3>

                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Suporte Técnico</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote1OT" name="dbClassSuporteTecnicoOT" value="1">
                                                    <label for="checkboxSuporote1OT"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote2OT" name="dbClassSuporteTecnicoOT" value="2">
                                                    <label for="checkboxSuporote2OT"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote3OT" name="dbClassSuporteTecnicoOT" value="3">
                                                    <label for="checkboxSuporote3OT"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote4OT" name="dbClassSuporteTecnicoOT" value="4">
                                                    <label for="checkboxSuporote4OT"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="checkboxSuporote5OT" name="dbClassSuporteTecnicoOT" value="5">
                                                    <label for="checkboxSuporote5OT"> 5 </label>
                                                </div>

                                            </div>
                                            <div class="mt-2">
                                                <p class="text-muted font-15 mt-3 mb-3">Atendimento</p>

                                                <div class="checkbox checkbox-danger form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento1OT" name="dbClassAtendimentoOT" value="1">
                                                    <label for="checkboxAtendimento1OT"> 1 </label>
                                                </div>
                                                <div class="checkbox checkbox-warning form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento2OT" name="dbClassAtendimentoOT" value="2">
                                                    <label for="checkboxAtendimento2OT"> 2 </label>
                                                </div>
                                                <div class="checkbox checkbox-info form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento3OT" name="dbClassAtendimentoOT" value="3">
                                                    <label for="checkboxAtendimento3OT"> 3 </label>
                                                </div>
                                                <div class="checkbox checkbox-custom form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento4OT" name="dbClassAtendimentoOT"value="4">
                                                    <label for="checkboxAtendimento4OT"> 4 </label>
                                                </div>
                                                <div class="checkbox checkbox-success form-check-inline">
                                                    <input type="checkbox" id="checkboxAtendimento5OT" name="dbClassAtendimentoOT" value="5">
                                                    <label for="checkboxAtendimento5OT"> 5 </label>
                                                </div>

                                            </div>


                                        </div>
                                    </div>




                                </div>
                                <button type="submit" class="btn btn-lg btn-success waves-effect waves-light"  name="cadastrarcf" id="cadastrarcf"> <i class="fa fa-rocket m-r-5"></i> <span>ENVIAR</span> </button>

                            </div>

                                </div>
                            </div>


                    </div>
                </div>



            </form>
        </div>


    </div>
    <footer class="footer text-center">
        2018 © Desenvolvido por - Cafeína Digital
    </footer>
</div>
<script src="assets/js/jquery-1.7.1.js"></script>
<script src="assets/js/CustomClienteFinal.js"></script>
<script src="assets/js/CustomNegocioProprio.js"></script>
<script src="assets/js/CustomRevenda.js"></script>
<script src="assets/js/populaCamposEstadoCidade.js"></script>

<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/metisMenu.min.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<!-- Sweet Alert Js  -->
<script src="assets/plugins/sweet-alert/sweetalert2.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- App js -->
<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>
</body>
</html>