
$(document).ready(function() {

    handleStatusChanged8();
});

function handleStatusChanged8() {
    $('#checkboxRV').click(function() {
        toggleStatus8();
    });
}

function toggleStatus8() {
    if ($('#checkboxRV').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraRV").removeClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraRV, #divComoSoubeDaMecolourRV, #divQualSuaAtuacaoRV, #divQualSuaAtuacaoRV, #divQualEquipamentoVoceUsaRV, #divPossuiDuvidasEmRelacaoAoNegocioRV, #divObrigadoRV").addClass("d-none");
    }

}




handleStatusChanged9();


function handleStatusChanged9() {
    $('#checkboxConheceRVSim').click(function() {
        toggleStatus9();
    });
}

function toggleStatus9() {
    if ($('#checkboxConheceRVSim').is(':checked')) {
        $("#divComoSoubeDaMecolourRV").removeClass("d-none");
    } else {
        $("#divComoSoubeDaMecolourRV ").addClass("d-none");
    }

}

handleStatusChanged10();


function handleStatusChanged10() {
    $('#checkboxConheceRVNao').click(function() {
        toggleStatus10();
    });
}

function toggleStatus10() {
    if ($('#checkboxConheceRVNao').is(':checked')) {
        $("#divQualSuaAtuacaoRV").removeClass("d-none");
    } else {
        $("#divQualSuaAtuacaoRV, #divComoSoubeDaMecolourRV ").addClass("d-none");
    }

}

handleStatusChanged11();


function handleStatusChanged11() {
    $('#checkboxComoSoubeMecolourRV1, #checkboxComoSoubeMecolourRV2, #checkboxComoSoubeMecolourRV3').click(function() {
        toggleStatus11();
    });
}

function toggleStatus11() {
    if ($('#checkboxComoSoubeMecolourRV1, #checkboxComoSoubeMecolourRV2, #checkboxComoSoubeMecolourRV3').is(':checked')) {
        $("#divQualSuaAtuacaoRV").removeClass("d-none");
    } else {
        $("#divQualSuaAtuacaoRV ").addClass("d-none");
    }

}

handleStatusChanged12();


function handleStatusChanged12() {
    $('#checkboxNegocioProprioRV6').click(function() {
        toggleStatus12();
    });
}

function toggleStatus12() {
    if ($('#checkboxNegocioProprioRV6').is(':checked')) {
        $("#divQualEquipamentoVoceUsaRV").removeClass("d-none");
    } else {
        $("#divQualEquipamentoVoceUsaRV ").addClass("d-none");
    }

}




handleStatusChanged1w2();


function handleStatusChanged1w2() {
    $('#checkboxNegocioProprioRV1, #checkboxNegocioProprioRV3, #checkboxNegocioProprioRV5').click(function() {
        toggleStatus1w2();
    });
}

function toggleStatus1w2() {
    if ($('#checkboxNegocioProprioRV1, #checkboxNegocioProprioRV3, #checkboxNegocioProprioRV5').is(':checked')) {
        $("#divQualEquipamentoVoceUsaRV2").removeClass("d-none");
    } else {
        $("#divQualEquipamentoVoceUsaRV2 ").addClass("d-none");
    }

}


handleStatusChanged1ww2();


function handleStatusChanged1ww2() {
    $('#checkboxNegocioProprioRV4, #checkboxNegocioProprioRV2').click(function() {
        toggleStatus1ww2();
    });
}

function toggleStatus1ww2() {
    if ($('#checkboxNegocioProprioRV4, #checkboxNegocioProprioRV2').is(':checked')) {
        $("#divQualEquipamentoVoceUsaRV3").removeClass("d-none");
    } else {
        $("#divQualEquipamentoVoceUsaRV3 ").addClass("d-none");
    }

}

handleStatusChanged13();


function handleStatusChanged13() {
    $('#checkboxQualEquipamentoVoceUsaRV1, #checkboxQualEquipamentoVoceUsaRV7, #checkboxQualEquipamentoVoceUsaRV8, #checkboxQualEquipamentoVoceUsaRV2, #checkboxQualEquipamentoVoceUsaRV3, #checkboxQualEquipamentoVoceUsaRV4').click(function() {
        toggleStatus13();
    });
}

function toggleStatus13() {
    if ($('#checkboxQualEquipamentoVoceUsaRV1, #checkboxQualEquipamentoVoceUsaRV7, #checkboxQualEquipamentoVoceUsaRV8, #checkboxQualEquipamentoVoceUsaRV2, #checkboxQualEquipamentoVoceUsaRV3, #checkboxQualEquipamentoVoceUsaRV4').is(':checked')) {
        $("#divPossuiDuvidasEmRelacaoAoNegocioRV").removeClass("d-none");
    } else {
        $("#divPossuiDuvidasEmRelacaoAoNegocioRV ").addClass("d-none");
    }

}

handleStatusChanged14();


function handleStatusChanged14() {
    $('#checkboxPossuiDuvidasEmRelacaoAoNegocioRV1, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV2, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV3, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV4, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV5, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV6, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV7').click(function() {
        toggleStatus14();
    });
}

function toggleStatus14() {
    if ($('#checkboxPossuiDuvidasEmRelacaoAoNegocioRV1, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV2, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV3, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV4, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV5, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV6, #checkboxPossuiDuvidasEmRelacaoAoNegocioRV7').is(':checked')) {
        $("#divObrigadoRV").removeClass("d-none");
    } else {
        $("#divObrigadoRV ").addClass("d-none");
    }

}

handleStatusChangedOutrosRV1();

function handleStatusChangedOutrosRV1() {
    $('#checkboxComoSoubeMecolourRV4').click(function() {
        toggleStatusOutrosRV1();
    });
}

function toggleStatusOutrosRV1() {
    if ($('#checkboxComoSoubeMecolourRV4').is(':checked')) {
        $("#OutrosComoSoubeMecolour, #divQualSuaAtuacaoRV").removeClass("d-none");
    } else {
        $("#OutrosComoSoubeMecolour, #divQualSuaAtuacaoRV").addClass("d-none");
    }

}
handleStatusChangedOutrosRV2();

function handleStatusChangedOutrosRV2() {
    $('#checkboxNegocioProprioRV7').click(function() {
        toggleStatusOutrosRV2();
    });
}

function toggleStatusOutrosRV2() {
    if ($('#checkboxNegocioProprioRV7').is(':checked')) {
        $("#OutrosNegocioProprio, #divQualEquipamentoVoceUsaRV").removeClass("d-none");
    } else {
        $("#OutrosNegocioProprio, #divQualEquipamentoVoceUsaRV").addClass("d-none");
    }

}

handleStatusChangedOutrosRV3();

function handleStatusChangedOutrosRV3() {
    $('#checkboxPossuiDuvidasEmRelacaoAoNegocioRV7').click(function() {
        toggleStatusOutrosRV3();
    });
}

function toggleStatusOutrosRV3() {
    if ($('#checkboxPossuiDuvidasEmRelacaoAoNegocioRV7').is(':checked')) {
        $("#OutrosPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoRV").removeClass("d-none");
    } else {
        $("#OutrosPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoRV").addClass("d-none");
    }

}