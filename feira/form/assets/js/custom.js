
$(document).ready(function() {

    handleStatusChanged();
});

function handleStatusChanged() {
    $('#checkbox1').click(function() {
        toggleStatus();
    });
}

function toggleStatus() {
    if ($('#checkbox1').is(':checked')) {
        $("#elementsToOperateOn").removeClass("d-none");
    } else {
        $("#elementsToOperateOn, #elementsToOperateOn2, #elementsToOperateOn3, #elementsToOperateOn4, #elementsToOperateOn5, #elementsToOperateOn6, #elementsToOperateOn7, #elementsToOperateOn8, #elementsToOperateOn9, #elementsToOperateOn11 ").addClass("d-none");
    }

}

handleStatusChanged9();

function handleStatusChanged9() {
    $('#checkbox2').click(function() {
        toggleStatus9();
    });
}

function toggleStatus9() {
    if ($('#checkbox2').is(':checked')) {
        $("#formCFBuscandoFeira").removeClass("d-none");
    } else {
        $("#formCFBuscandoFeira").addClass("d-none");
    }

}


handleStatusChanged1();

function handleStatusChanged1() {
    $('#checkbox4').click(function() {
        toggleStatus1();
    });
}

function toggleStatus1() {
    if ($('#checkbox4').is(':checked')) {
        $("#elementsToOperateOn2").removeClass("d-none");
    }
    else {
        $("#elementsToOperateOn2, #elementsToOperateOn3, #elementsToOperateOn4, #elementsToOperateOn5, #elementsToOperateOn6, #elementsToOperateOn7, #elementsToOperateOn8, #elementsToOperateOn9").addClass("d-none");
    }
}

handleStatusChanged2();

function handleStatusChanged2() {
    $('#checkbox5').click(function() {
        toggleStatus2();
    });
}

function toggleStatus2() {
    if ($('#checkbox5').is(':checked')) {
        $("#elementsToOperateOn3").removeClass("d-none");
    }
    else {
        $("#elementsToOperateOn3").addClass("d-none");
    }
}

handleStatusChanged3();

function handleStatusChanged3() {
    $('#checkbox6, #checkbox7, #checkbox8').click(function() {
        toggleStatus3();
    });
}

function toggleStatus3() {
    if ($('#checkbox6, #checkbox7,#checkbox8 ').is(':checked')) {
        $("#elementsToOperateOn3").removeClass("d-none");
    }
    else {
        $("#elementsToOperateOn3").addClass("d-none");
    }
}

handleStatusChanged4();

function handleStatusChanged4() {
    $('#checkbox9').click(function() {
        toggleStatus4();
    });
}

function toggleStatus4() {
    if ($('#checkbox9 ').is(':checked')) {
        $("#elementsToOperateOn5").removeClass("d-none");
    }
    else {
        $("#elementsToOperateOn5").addClass("d-none");
    }
}

handleStatusChanged5();

function handleStatusChanged5() {
    $('#checkbox10').click(function() {
        toggleStatus5();
    });
}

function toggleStatus5() {
    if ($('#checkbox10 ').is(':checked')) {
        $("#elementsToOperateOn10").removeClass("d-none");
    }
    else {
        $("#elementsToOperateOn10").addClass("d-none");
    }
}

handleStatusChanged6();

function handleStatusChanged6() {
    $('#checkbox11, #checkbox12, #checkbox13').click(function() {
        toggleStatus6();
    });
}

function toggleStatus6() {
    if ($('#checkbox11, #checkbox12, #checkbox13').is(':checked')) {
        $("#elementsToOperateOn8").removeClass("d-none");
    }
    else {
        $("#elementsToOperateOn8").addClass("d-none");
    }
}

handleStatusChanged7();

function handleStatusChanged7() {
    $('#checkbox16, #checkbox17, #checkbox18, #checkbox19, #checkbox20, #checkbox21').click(function() {
        toggleStatus7();
    });
}

function toggleStatus7() {
    if ($('#checkbox16, #checkbox17, #checkbox18, #checkbox19, #checkbox20, #checkbox21').is(':checked')) {
        $("#elementsToOperateOn9").removeClass("d-none");
    }
    else {
        $("#elementsToOperateOn9").addClass("d-none");
    }
}

handleStatusChanged8();

function handleStatusChanged8() {
    $('#checkboxConheceCF1').click(function() {
        toggleStatus8();
    });
}

function toggleStatus8() {
    if ($('#checkboxConheceCF1').is(':checked')) {
        $("#formCFComoSoube").removeClass("d-none");
    }
    else {
        $("#formCFComoSoube").addClass("d-none");
    }
}

handleStatusChanged14();

function handleStatusChanged14() {
    $('#checkboxConheceCF2').click(function() {
        toggleStatus14();
    });
}

function toggleStatus14() {
    if ($('#checkboxConheceCF2').is(':checked')) {
        $("#formCFAtuacao").removeClass("d-none");
    }
    else {
        $("#formCFAtuacao").addClass("d-none");
    }
}

handleStatusChanged10();

function handleStatusChanged10() {
    $('#checkboxConheceComoSoubeCF1, #checkboxConheceComoSoubeCF2, #checkboxConheceComoSoubeCF3, #checkboxConheceComoSoubeCF4').click(function() {
        toggleStatus10();
    });
}

function toggleStatus10() {
    if ($('#checkboxConheceComoSoubeCF1, #checkboxConheceComoSoubeCF2, #checkboxConheceComoSoubeCF3, #checkboxConheceComoSoubeCF4').is(':checked')) {
        $("#formCFAtuacao").removeClass("d-none");
    }
    else {
        $("#formCFAtuacao").addClass("d-none");
    }
}

handleStatusChanged11();

function handleStatusChanged11() {
    $('#checkboxAtuacaoCF1, #checkboxAtuacaoCF2, #checkboxAtuacaoCF3, #checkboxAtuacaoCF4, #checkboxAtuacaoCF5, #checkboxAtuacaoCF6, #checkboxAtuacaoCF7').click(function() {
        toggleStatus11();
    });
}

function toggleStatus11() {
    if ($('#checkboxAtuacaoCF1, #checkboxAtuacaoCF2, #checkboxAtuacaoCF3, #checkboxAtuacaoCF4, #checkboxAtuacaoCF5, #checkboxAtuacaoCF6, #checkboxAtuacaoCF7').is(':checked')) {
        $("#formCFEquipamento").removeClass("d-none");
    }
    else {
        $("#formCFEquipamentos").addClass("d-none");
    }
}

handleStatusChanged12();

function handleStatusChanged12() {
    $('#checkboxInvestimentoCF1, #checkboxInvestimentoCF2, #checkboxInvestimentoCF3, #checkboxInvestimentoCF4').click(function() {
        toggleStatus12();
    });
}

function toggleStatus12() {
    if ($('#checkboxInvestimentoCF1, #checkboxInvestimentoCF2, #checkboxInvestimentoCF3, #checkboxInvestimentoCF4').is(':checked')) {
        $("#formCFDuvida").removeClass("d-none");
    }
    else {
        $("#formCFDuvida").addClass("d-none");
    }
}

handleStatusChanged13();

function handleStatusChanged13() {
    $('#checkboxDuvidaCF1, #checkboxDuvidaCF2, #checkboxDuvidaCF3, #checkboxDuvidaCF4, #checkboxDuvidaCF5, #checkboxDuvidaCF6').click(function() {
        toggleStatus13();
    });
}

function toggleStatus13() {
    if ($('#checkboxDuvidaCF1, #checkboxDuvidaCF2, #checkboxDuvidaCF3, #checkboxDuvidaCF4, #checkboxDuvidaCF5, #checkboxDuvidaCF6').is(':checked')) {
        $("#formCFObrigado").removeClass("d-none");
    }
    else {
        $("#formCFObrigado").addClass("d-none");
    }
}
