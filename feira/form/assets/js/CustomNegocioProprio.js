
$(document).ready(function() {

    handleStatusChanged();
});

function handleStatusChanged() {
    $('#checkboxNP').click(function() {
        toggleStatus();
    });
}

function toggleStatus() {
    if ($('#checkboxNP').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraNP").removeClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraNP, #divComoSoubeDaMecolourNP, #divEstaBuscandoAlgoParaInvestir, #divEstaBuscandoAlgoParaInvestir, #divNegocioProprioNP, #divObrigadoNP, #divPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoNP2 ").addClass("d-none");
    }

}
handleStatusChanged1();

function handleStatusChanged1() {
    $('#checkboxConheceNp1Sim').click(function() {
        toggleStatus1();
    });
}

function toggleStatus1() {
    if ($('#checkboxConheceNp1Sim').is(':checked')) {
        $("#divComoSoubeDaMecolourNP").removeClass("d-none");
    } else {
        $("#divComoSoubeDaMecolourNP ").addClass("d-none");
    }

}

handleStatusChanged2();

function handleStatusChanged2() {
    $('#checkboxConheceNp2Nao').click(function() {
        toggleStatus2();
    });
}

function toggleStatus2() {
    if ($('#checkboxConheceNp2Nao').is(':checked')) {
        $("#divEstaBuscandoAlgoParaInvestir").removeClass("d-none");
    } else {
        $("#divEstaBuscandoAlgoParaInvestir ").addClass("d-none");
    }

}

handleStatusChanged3();

function handleStatusChanged3() {
    $('#checkboxComoSoubeMecolourNP1, #checkboxComoSoubeMecolourNP2, #checkboxComoSoubeMecolourNP3').click(function() {
        toggleStatus3();
    });
}

function toggleStatus3() {
    if ($('#checkboxComoSoubeMecolourNP1, #checkboxComoSoubeMecolourNP2, #checkboxComoSoubeMecolourNP3').is(':checked')) {
        $("#divEstaBuscandoAlgoParaInvestir").removeClass("d-none");
    } else {
        $("#divEstaBuscandoAlgoParaInvestir").addClass("d-none");
    }

}

handleStatusChanged4();

function handleStatusChanged4() {
    $('#checkboxEstaBuscandoAlgoParaInvestirSim').click(function() {
        toggleStatus4();
    });
}

function toggleStatus4() {
    if ($('#checkboxEstaBuscandoAlgoParaInvestirSim').is(':checked')) {
        $("#divNegocioProprioNP").removeClass("d-none");
    } else {
        $("#divNegocioProprioNP ").addClass("d-none");
    }

}
handleStatusChanged5();

function handleStatusChanged5() {
    $('#checkboxEstaBuscandoAlgoParaInvestirNao').click(function() {
        toggleStatus5();
    });
}

function toggleStatus5() {
    if ($('#checkboxEstaBuscandoAlgoParaInvestirNao').is(':checked')) {
        $("#divObrigadoNP").removeClass("d-none");
    } else {
        $("#divObrigadoNP ").addClass("d-none");
    }

}

handleStatusChanged6();

function handleStatusChanged6() {
    $('#checkboxNegocioProprioNP1, #checkboxNegocioProprioNP2, #checkboxNegocioProprioNP3').click(function() {
        toggleStatus6();
    });
}

function toggleStatus6() {
    if ($('#checkboxNegocioProprioNP1, #checkboxNegocioProprioNP2, #checkboxNegocioProprioNP3').is(':checked')) {
        $("#divPossuiDuvidasEmRelacaoAoNegocio").removeClass("d-none");
    } else {
            $("#divPossuiDuvidasEmRelacaoAoNegocio").addClass("d-none");
    }

}

handleStatusChanged7();

function handleStatusChanged7() {
    $('#checkboxPossuiDuvidasEmRelacaoAoNegocioNP1, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP2, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP3, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP4, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP5, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP6, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP7').click(function() {
        toggleStatus7();
    });
}

function toggleStatus7() {
    if ($('#checkboxPossuiDuvidasEmRelacaoAoNegocioNP1, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP2, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP3, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP4, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP5, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP6, #checkboxPossuiDuvidasEmRelacaoAoNegocioNP7').is(':checked')) {
        $("#divObrigadoNP2").removeClass("d-none");
    } else {
        $("#divObrigadoNP2 ").addClass("d-none");
    }

}

handleStatusChanged119();


function handleStatusChanged119() {
    $('#checkboxCF').click(function() {
        toggleStatus119();
    });
}

function toggleStatus119() {
    if ($('#checkboxCF').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraNP, #divComoSoubeDaMecolourNP, #divEstaBuscandoAlgoParaInvestir, #divEstaBuscandoAlgoParaInvestir, #divNegocioProprioNP, #divObrigadoNP, #divPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoNP2 ").addClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraNP, #divComoSoubeDaMecolourNP, #divEstaBuscandoAlgoParaInvestir, #divEstaBuscandoAlgoParaInvestir, #divNegocioProprioNP, #divObrigadoNP, #divPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoNP2 ").addClass("d-none");
    }

}

handleStatusChanged125();


function handleStatusChanged125() {
    $('#checkboxCF').click(function() {
        toggleStatus125();
    });
}

function toggleStatus125() {
    if ($('#checkboxCF').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraRV, #divComoSoubeDaMecolourRV, #divQualSuaAtuacaoRV, #divQualSuaAtuacaoRV, #divQualEquipamentoVoceUsaRV, #divPossuiDuvidasEmRelacaoAoNegocioRV, #divObrigadoRV ").addClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraRV, #divComoSoubeDaMecolourRV, #divQualSuaAtuacaoRV, #divQualSuaAtuacaoRV, #divQualEquipamentoVoceUsaRV, #divPossuiDuvidasEmRelacaoAoNegocioRV, #divObrigadoRV ").addClass("d-none");
    }

}

handleStatusChanged120();


function handleStatusChanged120() {
    $('#checkboxRV').click(function() {
        toggleStatus120();
    });
}

function toggleStatus120() {
    if ($('#checkboxRV').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraNP, #divComoSoubeDaMecolourNP, #divEstaBuscandoAlgoParaInvestir, #divEstaBuscandoAlgoParaInvestir, #divNegocioProprioNP, #divObrigadoNP, #divPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoNP2 ").addClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraNP, #divComoSoubeDaMecolourNP, #divEstaBuscandoAlgoParaInvestir, #divEstaBuscandoAlgoParaInvestir, #divNegocioProprioNP, #divObrigadoNP, #divPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoNP2 ").addClass("d-none");
    }

}


handleStatusChanged121();


function handleStatusChanged121() {
    $('#checkboxRV').click(function() {
        toggleStatus121();
    });
}

function toggleStatus121() {
    if ($('#checkboxRV').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraCF, #divComoSoubeDaMecolourCF, #divQualSuaAtuacaoCF, #divQualSuaAtuacaoCF, #divQualEquipamentoVoceUsaCF, #divPossuiDuvidasEmRelacaoAoNegocioCF, #divObrigadoCF,  ").addClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraCF, #divComoSoubeDaMecolourCF, #divQualSuaAtuacaoCF, #divQualSuaAtuacaoCF, #divQualEquipamentoVoceUsaCF, #divPossuiDuvidasEmRelacaoAoNegocioCF, #divObrigadoCF,  ").addClass("d-none");
    }

}

handleStatusChanged122();


function handleStatusChanged122() {
    $('#checkboxRV').click(function() {
        toggleStatus122();
    });
}

function toggleStatus122() {
    if ($('#checkboxRV').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraNP, #divComoSoubeDaMecolourNP, #divEstaBuscandoAlgoParaInvestir, #divEstaBuscandoAlgoParaInvestir, #divNegocioProprioNP, #divObrigadoNP, #divPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoNP2 ").addClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraNP, #divComoSoubeDaMecolourNP, #divEstaBuscandoAlgoParaInvestir, #divEstaBuscandoAlgoParaInvestir, #divNegocioProprioNP, #divObrigadoNP, #divPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoNP2 ").addClass("d-none");
    }

}

handleStatusChanged123();


function handleStatusChanged123() {
    $('#checkboxNP').click(function() {
        toggleStatus123();
    });
}

function toggleStatus123() {
    if ($('#checkboxRV').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraRV, #divComoSoubeDaMecolourRV, #divQualSuaAtuacaoRV, #divQualSuaAtuacaoRV, #divQualEquipamentoVoceUsaRV, #divPossuiDuvidasEmRelacaoAoNegocioRV, #divObrigadoRV ").addClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraRV, #divComoSoubeDaMecolourRV, #divQualSuaAtuacaoRV, #divQualSuaAtuacaoRV, #divQualEquipamentoVoceUsaRV, #divPossuiDuvidasEmRelacaoAoNegocioRV, #divObrigadoRV ").addClass("d-none");
    }

}


handleStatusChanged124();


function handleStatusChanged124() {
    $('#checkboxNP').click(function() {
        toggleStatus124();
    });
}

function toggleStatus124() {
    if ($('#checkboxRV').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraCF, #divComoSoubeDaMecolourCF, #divQualSuaAtuacaoCF, #divQualSuaAtuacaoCF, #divQualEquipamentoVoceUsaCF, #divPossuiDuvidasEmRelacaoAoNegocioCF, #divObrigadoCF,  ").addClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraCF, #divComoSoubeDaMecolourCF, #divQualSuaAtuacaoCF, #divQualSuaAtuacaoCF, #divQualEquipamentoVoceUsaCF, #divPossuiDuvidasEmRelacaoAoNegocioCF, #divObrigadoCF,  ").addClass("d-none");
    }

}


handleStatusChangedOutros1();

function handleStatusChangedOutros1() {
    $('#checkboxComoSoubeMecolourNP4').click(function() {
        toggleStatusOutros1();
    });
}

function toggleStatusOutros1() {
    if ($('#checkboxComoSoubeMecolourNP4').is(':checked')) {
        $("#outrosOqueBusca, #divEstaBuscandoAlgoParaInvestir").removeClass("d-none");
    } else {
        $("#outrosOqueBusca, #divEstaBuscandoAlgoParaInvestir ").addClass("d-none");
    }

}

handleStatusChangedOutros2();

function handleStatusChangedOutros2() {
    $('#checkboxPossuiDuvidasEmRelacaoAoNegocioNP7  ').click(function() {
        toggleStatusOutros2();
    });
}

function toggleStatusOutros2() {
    if ($('#checkboxPossuiDuvidasEmRelacaoAoNegocioNP7').is(':checked')) {
        $("#outrosPossuiDuvidasEmRelacaoAoNegocioNP7, #divObrigadoNP2").removeClass("d-none");
    } else {
        $("#outrosPossuiDuvidasEmRelacaoAoNegocioNP7, #divObrigadoNP2 ").addClass("d-none");
    }

}

handleStatusChangedOutros3();

function handleStatusChangedOutros3() {
    $('#checkboxNegocioProprioNP4  ').click(function() {
        toggleStatusOutros3();
    });
}

function toggleStatusOutros3() {
    if ($('#checkboxNegocioProprioNP4').is(':checked')) {
        $("#outrosNegocioProprioNP, #divPossuiDuvidasEmRelacaoAoNegocio").removeClass("d-none");
    } else {
        $("#outrosNegocioProprioNP, #divPossuiDuvidasEmRelacaoAoNegocio").addClass("d-none");
    }

}