
$(document).ready(function() {

    handleStatusChanged8x();
});

function handleStatusChanged8x() {
    $('#checkboxCF').click(function() {
        toggleStatus8x();
    });
}

function toggleStatus8x() {
    if ($('#checkboxCF').is(':checked')) {
        $("#divOqueEstaBuscandoNaFeiraCF").removeClass("d-none");
    } else {
        $("#divOqueEstaBuscandoNaFeiraCF, #divComoSoubeDaMecolourCF, #divQualSuaAtuacaoCF, #divQualSuaAtuacaoCF, #divQualEquipamentoVoceUsaCF, #divPossuiDuvidasEmRelacaoAoNegocioCF, #divObrigadoCF").addClass("d-none");
    }

}




handleStatusChanged9x();


function handleStatusChanged9x() {
    $('#checkboxConheceCFSim').click(function() {
        toggleStatus9x();
    });
}

function toggleStatus9x() {
    if ($('#checkboxConheceCFSim').is(':checked')) {
        $("#divComoSoubeDaMecolourCF").removeClass("d-none");
    } else {
        $("#divComoSoubeDaMecolourCF ").addClass("d-none");
    }

}

handleStatusChanged10x();


function handleStatusChanged10x() {
    $('#checkboxConheceCFNao').click(function() {
        toggleStatus10x();
    });
}

function toggleStatus10x() {
    if ($('#checkboxConheceCFNao').is(':checked')) {
        $("#divQualSuaAtuacaoCF").removeClass("d-none");
    } else {
        $("#divQualSuaAtuacaoCF, #divComoSoubeDaMecolourCF ").addClass("d-none");
    }

}

handleStatusChanged11x();


function handleStatusChanged11x() {
    $('#checkboxComoSoubeMecolourCF1, #checkboxComoSoubeMecolourCF2, #checkboxComoSoubeMecolourCF3').click(function() {
        toggleStatus11x();
    });
}

function toggleStatus11x() {
    if ($('#checkboxComoSoubeMecolourCF1, #checkboxComoSoubeMecolourCF2, #checkboxComoSoubeMecolourCF3').is(':checked')) {
        $("#divQualSuaAtuacaoCF").removeClass("d-none");
    } else {
        $("#divQualSuaAtuacaoCF ").addClass("d-none");
    }

}

handleStatusChanged12x();


function handleStatusChanged12x() {
    $('#checkboxNegocioProprioCF6').click(function() {
        toggleStatus12x();
    });
}

function toggleStatus12x() {
    if ($('#checkboxNegocioProprioCF6').is(':checked')) {
        $("#divQualEquipamentoVoceUsaCF").removeClass("d-none");
    } else {
        $("#divQualEquipamentoVoceUsaCF ").addClass("d-none");
    }

}




handleStatusChanged1w2x();


function handleStatusChanged1w2x() {
    $('#checkboxNegocioProprioCF1, #checkboxNegocioProprioCF3, #checkboxNegocioProprioCF5').click(function() {
        toggleStatus1w2x();
    });
}

function toggleStatus1w2x() {
    if ($('#checkboxNegocioProprioCF1, #checkboxNegocioProprioCF3, #checkboxNegocioProprioCF5').is(':checked')) {
        $("#divQualEquipamentoVoceUsaCF2").removeClass("d-none");
    } else {
        $("#divQualEquipamentoVoceUsaCF2 ").addClass("d-none");
    }

}


handleStatusChanged1ww2x();


function handleStatusChanged1ww2x() {
    $('#checkboxNegocioProprioCF4, #checkboxNegocioProprioCF2').click(function() {
        toggleStatus1ww2x();
    });
}

function toggleStatus1ww2x() {
    if ($('#checkboxNegocioProprioCF4, #checkboxNegocioProprioCF2').is(':checked')) {
        $("#divQualEquipamentoVoceUsaCF3").removeClass("d-none");
    } else {
        $("#divQualEquipamentoVoceUsaCF3 ").addClass("d-none");
    }

}

handleStatusChanged13x();


function handleStatusChanged13x() {
    $('#checkboxQualEquipamentoVoceUsaCF1, #checkboxQualEquipamentoVoceUsaCF7, #checkboxQualEquipamentoVoceUsaCF8, #checkboxQualEquipamentoVoceUsaCF2, #checkboxQualEquipamentoVoceUsaCF3, #checkboxQualEquipamentoVoceUsaCF4').click(function() {
        toggleStatus13x();
    });
}

function toggleStatus13x() {
    if ($('#checkboxQualEquipamentoVoceUsaCF1, #checkboxQualEquipamentoVoceUsaCF7, #checkboxQualEquipamentoVoceUsaCF8, #checkboxQualEquipamentoVoceUsaCF2, #checkboxQualEquipamentoVoceUsaCF3, #checkboxQualEquipamentoVoceUsaCF4').is(':checked')) {
        $("#divPossuiDuvidasEmRelacaoAoNegocioCF").removeClass("d-none");
    } else {
        $("#divPossuiDuvidasEmRelacaoAoNegocioCF ").addClass("d-none");
    }

}

handleStatusChanged14x();


function handleStatusChanged14x() {
    $('#checkboxPossuiDuvidasEmRelacaoAoNegocioCF1, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF2, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF3, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF4, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF5, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF6, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF7').click(function() {
        toggleStatus14x();
    });
}

function toggleStatus14x() {
    if ($('#checkboxPossuiDuvidasEmRelacaoAoNegocioCF1, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF2, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF3, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF4, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF5, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF6, #checkboxPossuiDuvidasEmRelacaoAoNegocioCF7').is(':checked')) {
        $("#divObrigadoCF").removeClass("d-none");
    } else {
        $("#divObrigadoCF ").addClass("d-none");
    }

}

handleStatusChangedOutrosCF1x();

function handleStatusChangedOutrosCF1x() {
    $('#checkboxComoSoubeMecolourCF4').click(function() {
        toggleStatusOutrosCF1x();
    });
}

function toggleStatusOutrosCF1x() {
    if ($('#checkboxComoSoubeMecolourCF4').is(':checked')) {
        $("#OutrosComoSoubeMecolour, #divQualSuaAtuacaoCF").removeClass("d-none");
    } else {
        $("#OutrosComoSoubeMecolour, #divQualSuaAtuacaoCF").addClass("d-none");
    }

}
handleStatusChangedOutrosCF2x();

function handleStatusChangedOutrosCF2x() {
    $('#checkboxNegocioProprioCF7').click(function() {
        toggleStatusOutrosCF2x();
    });
}

function toggleStatusOutrosCF2x() {
    if ($('#checkboxNegocioProprioCF7').is(':checked')) {
        $("#OutrosNegocioProprio, #divQualEquipamentoVoceUsaCF").removeClass("d-none");
    } else {
        $("#OutrosNegocioProprio, #divQualEquipamentoVoceUsaCF").addClass("d-none");
    }

}

handleStatusChangedOutrosCF3x();

function handleStatusChangedOutrosCF3x() {
    $('#checkboxPossuiDuvidasEmRelacaoAoNegocioCF7').click(function() {
        toggleStatusOutrosCF3x();
    });
}

function toggleStatusOutrosCF3x() {
    if ($('#checkboxPossuiDuvidasEmRelacaoAoNegocioCF7').is(':checked')) {
        $("#OutrosPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoCF").removeClass("d-none");
    } else {
        $("#OutrosPossuiDuvidasEmRelacaoAoNegocio, #divObrigadoCF").addClass("d-none");
    }

}