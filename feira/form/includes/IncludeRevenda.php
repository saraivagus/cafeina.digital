<?php


try{
    $conexao = new PDO('mysql:host=localhost;dbname=cafeina_mecolour', 'cafeina_mecolour', 'DB13579**#');
    $conexao ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo 'ERROR:' . $e->getMessage();


};

if(isset($_POST['cadastrarRV'])){
    $strNome = trim(strip_tags($_POST['strNome']));
    $strEmail = trim(strip_tags($_POST['strEmail']));
    $strEstado = trim(strip_tags($_POST['strEstado']));
    $strCidade = trim(strip_tags($_POST['strCidade']));
    $strCheckbox = trim(strip_tags($_POST['strCheckbox']));
    $dbFormOqueBuscaRV = trim(strip_tags($_POST['dbFormOqueBuscaRV']));
    $dbFormConheceMecolourRV = trim(strip_tags($_POST['dbFormConheceMecolourRV']));
    $dbFormComoSoubeRV = trim(strip_tags($_POST['dbFormComoSoubeRV']));
    $dbFormOutrosComoSoubeRV = trim(strip_tags($_POST['dbFormOutrosComoSoubeRV']));
    $dbFormNegocioProprioRV = trim(strip_tags($_POST['dbFormNegocioProprioRV']));
    $dbFormOutrosNegocioProprioRV = trim(strip_tags($_POST['dbFormOutrosNegocioProprioRV']));
    $dbFormEquipamentoRV = trim(strip_tags($_POST['dbFormEquipamentoRV']));
    $dbFormEquipamentoRVOutro = trim(strip_tags($_POST['dbFormEquipamentoRVOutro']));
    $dbFormEquipamentoRVMarca = trim(strip_tags($_POST['dbFormEquipamentoRVMarca']));
    $dbFormEquipamentoPapelGramaturaRV = trim(strip_tags($_POST['dbFormEquipamentoPapelGramaturaRV']));
    $dbFormEquipamentoPapelMarcaRV = trim(strip_tags($_POST['dbFormEquipamentoPapelMarcaRV']));
    $dbFormPossuiDuvidasEmRelacaoAoNegocioRV = trim(strip_tags($_POST['dbFormPossuiDuvidasEmRelacaoAoNegocioRV']));
    $dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioRV = trim(strip_tags($_POST['dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioRV']));



    $insert = "INSERT INTO dbformrv (strNome, strEmail, strEstado, strCidade, strCheckbox, dbFormOqueBuscaRV, dbFormConheceMecolourRV, dbFormComoSoubeRV, dbFormOutrosComoSoubeRV, dbFormNegocioProprioRV, dbFormOutrosNegocioProprioRV, dbFormEquipamentoRV, dbFormEquipamentoRVOutro, dbFormEquipamentoRVMarca, dbFormEquipamentoPapelGramaturaRV, dbFormEquipamentoPapelMarcaRV, dbFormPossuiDuvidasEmRelacaoAoNegocioRV, dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioRV ) VALUES (:strNome, :strEmail, :strEstado, :strCidade, :strCheckbox, :dbFormOqueBuscaRV, :dbFormConheceMecolourRV, :dbFormComoSoubeRV, :dbFormOutrosComoSoubeRV, :dbFormNegocioProprioRV, :dbFormOutrosNegocioProprioRV, :dbFormEquipamentoRV, :dbFormEquipamentoRVOutro, :dbFormEquipamentoRVMarca, :dbFormEquipamentoPapelGramaturaRV, :dbFormEquipamentoPapelMarcaRV, :dbFormPossuiDuvidasEmRelacaoAoNegocioRV, :dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioRV )";
    try{

        $result = $conexao->prepare($insert);
        $result->bindParam(':strNome', $strNome, PDO::PARAM_STR);
        $result->bindParam(':strEmail', $strEmail, PDO::PARAM_STR);
        $result->bindParam(':strEstado', $strEstado, PDO::PARAM_STR);
        $result->bindParam(':strCidade', $strCidade, PDO::PARAM_STR);
        $result->bindParam(':strCheckbox', $strCheckbox, PDO::PARAM_STR);
        $result->bindParam(':dbFormOqueBuscaRV', $dbFormOqueBuscaRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormConheceMecolourRV', $dbFormConheceMecolourRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormComoSoubeRV', $dbFormComoSoubeRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormOutrosComoSoubeRV', $dbFormOutrosComoSoubeRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormNegocioProprioRV', $dbFormNegocioProprioRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormOutrosNegocioProprioRV', $dbFormOutrosNegocioProprioRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoRV', $dbFormEquipamentoRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoRVOutro', $dbFormEquipamentoRVOutro, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoRVMarca', $dbFormEquipamentoRVMarca, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoPapelGramaturaRV', $dbFormEquipamentoPapelGramaturaRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoPapelMarcaRV', $dbFormEquipamentoPapelMarcaRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormPossuiDuvidasEmRelacaoAoNegocioRV', $dbFormPossuiDuvidasEmRelacaoAoNegocioRV, PDO::PARAM_STR);
        $result->bindParam(':dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioRV', $dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioRV, PDO::PARAM_STR);

        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                echo ' <script type="text/javascript">
                        function sweet() {
                            swal(
                                \'Sucesso!\',
                                \'Pesquisa eviada com êxito\',
                                \'success\'
                            );
                        }
                        window.onload = sweet;
                        window.setTimeout(function() {
        window.location.href=\'http://feira.cafeina.digital/form/ClassRV.php\';
    }, 5000);
                    </script>';
            }
        }else{
            echo '  <script type="text/javascript">
                                         function sweet() {
                                             swal(
                                                 \'Ops, algo de errado\',
                                                 \'contate o suporte \',
                                                 \'error\'
                                             );
                                         }
                                         window.onload = sweet;
                                     </script>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>
<div class="row">


    <div class="col-md-12 text-center d-none" id="divOqueEstaBuscandoNaFeiraRV">
        <div class="card-box">


            <div class="row">
                <div class="col-lg-12 col-xl-7">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/target.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-pink">O QUE VOCÊ ESTÁ BUSCANDO NA FEIRA HOJE?</h4>
                            <input type="text" class="form-control" id="dbFormOqueBuscaRV" name="dbFormOqueBuscaRV" placeholder="Especifique">
                        </div>

                    </div>

                </div>
                <div class="col-lg-12 col-xl-5">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/megaphone.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-pink">VOCÊ CONHECE A MECOLOUR?</h4>
                        </div>
                        <div class="checkbox checkbox-pink">
                            <input id="checkboxConheceRVSim" type="checkbox"  name="dbFormConheceMecolourRV" value="Sim">
                            <label for="checkboxConheceRVSim">SIM </label>
                            <input id="checkboxConheceRVNao" type="checkbox" name="dbFormConheceMecolourRV" value="Não">
                            <label for="checkboxConheceRVNao">NÃO </label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divComoSoubeDaMecolourRV">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/megaphone.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-pink">COMO SOUBE DA MECOLOUR?</h4>
                        </div>
                        <div class="checkbox checkbox-pink">
                            <input id="checkboxComoSoubeMecolourRV1" type="checkbox" name="dbFormComoSoubeRV" value="Pesquisa" >
                            <label for="checkboxComoSoubeMecolourRV1">PESQUISA </label>
                            <input id="checkboxComoSoubeMecolourRV2" type="checkbox" name="dbFormComoSoubeRV" value="Indicação">
                            <label for="checkboxComoSoubeMecolourRV2">INDICAÇÃO </label>
                            <input id="checkboxComoSoubeMecolourRV4" type="checkbox" name="dbFormComoSoubeRV" value="Outros">
                            <label for="checkboxComoSoubeMecolourRV4">OUTROS </label>
                            <input type="text" id="OutrosComoSoubeMecolourRV" class="form-control d-none" name="dbFormOutrosComoSoubeRV" placeholder="Especifique">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12 text-center d-none" id="divQualSuaAtuacaoRV">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/brain.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-pink">QUAL SUA ATUAÇÃO?</h4>
                        </div>
                        <div class="checkbox checkbox-pink">
                            <input id="checkboxNegocioProprioRV1" type="checkbox" name="dbFormNegocioProprioRV" value="Brindes">
                            <label for="checkboxNegocioProprioRV1">BRINDES </label>
                            <input id="checkboxNegocioProprioRV2" type="checkbox" name="dbFormNegocioProprioRV" value="Transfer">
                            <label for="checkboxNegocioProprioRV2">TRANSFER </label>
                            <input id="checkboxNegocioProprioRV3" type="checkbox" name="dbFormNegocioProprioRV" value="Silk">
                            <label for="checkboxNegocioProprioRV3">SILK </label>
                            <input id="checkboxNegocioProprioRV4" type="checkbox" name="dbFormNegocioProprioRV" value="Sublimação">
                            <label for="checkboxNegocioProprioRV4">SUBLIMAÇÃO </label>
                            <input id="checkboxNegocioProprioRV5" type="checkbox" name="dbFormNegocioProprioRV" value="Canecas/Geral">
                            <label for="checkboxNegocioProprioRV5">CANECAS/GERAL </label>
                            <input id="checkboxNegocioProprioRV6" type="checkbox" name="dbFormNegocioProprioRV" value="Confecção">
                            <label for="checkboxNegocioProprioRV6">CONFECÇÃO </label>
                            <input id="checkboxNegocioProprioRV7" type="checkbox" name="dbFormNegocioProprioRV" value="Outros">
                            <label for="checkboxNegocioProprioRV7">OUTROS </label>
                            <input type="text" id="OutrosNegocioProprioRV" class="form-control d-none" name="dbFormOutrosNegocioProprioRV" placeholder="Especifique">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divQualEquipamentoVoceUsaRV">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/brain.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-pink">QUAL EQUIPAMENTO VOCÊ USA HOJE?</h4>
                        </div>
                        <div class="checkbox checkbox-pink">
                            <input id="checkboxQualEquipamentoVoceUsaRV1" type="checkbox" name="dbFormEquipamentoRV" value="PEQ. A3/A4">
                            <label for="checkboxQualEquipamentoVoceUsaRV1">PEQ. A3/A4</label>
                            <input id="checkboxQualEquipamentoVoceUsaRV2" type="checkbox" name="dbFormEquipamentoRV" value="GDE 1,60M">
                            <label for="checkboxQualEquipamentoVoceUsaRV2">GDE 1,60M</label>
                            <input id="checkboxQualEquipamentoVoceUsaRV3" type="checkbox" name="dbFormEquipamentoRV" value="GDE 1,80M">
                            <label for="checkboxQualEquipamentoVoceUsaRV3">GDE 1,80</label>
                            <input id="checkboxQualEquipamentoVoceUsaRV4" type="checkbox" name="dbFormEquipamentoRV" value="GDE 2,60M">
                            <label for="checkboxQualEquipamentoVoceUsaRV4">GDE 2,60</label>
                            <div class="form-row">
                                <h5 class="mb-0 text-pink">Outro</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoRVOutro" name="dbFormEquipamentoRVOutro" placeholder="Especifique">
                                <h5 class="mb-0 text-pink">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoRVMarca" name="dbFormEquipamentoRVMarca" placeholder="Especifique">
                            </div>
                            <h4 class="mb-0 text-pink">QUAL PAPEL VOCÊ USA HOJE?</h4>
                            <div class="form-row">

                                <h5 class="mb-0 ">Gramatura</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoPapelGramaturaRV" name="dbFormEquipamentoPapelGramaturaRV" placeholder="Especifique">
                                <h5 class="mb-0 ">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoPapelMarcaRV" name="dbFormEquipamentoPapelMarcaRV" placeholder="Especifique">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12 text-center d-none" id="divQualEquipamentoVoceUsaRV2">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/brain.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-pink">QUAL EQUIPAMENTO VOCÊ USA HOJE?</h4>
                        </div>
                        <div class="checkbox checkbox-pink">

                            <div class="form-row">
                                <h5 class="mb-0 text-pink">Tipo de Produto</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoRVOutro" name="dbFormEquipamentoRVOutro" placeholder="Especifique">
                                <h5 class="mb-0 text-pink">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoRVMarca" name="dbFormEquipamentoRVMarca" placeholder="Especifique">

                            </div>
                            <input id="checkboxQualEquipamentoVoceUsaRV7" type="checkbox" >
                            <label for="checkboxQualEquipamentoVoceUsaRV7">Avançar</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divQualEquipamentoVoceUsaRV3">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/brain.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-pink">QUAL EQUIPAMENTO VOCÊ USA HOJE?</h4>
                        </div>
                        <div class="checkbox checkbox-pink">

                            <div class="form-row">
                                <h5 class="mb-0 text-pink">Tipo de Produto</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoRVOutro" name="dbFormEquipamentoRVOutro" placeholder="Especifique">
                                <h5 class="mb-0 text-pink">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoRVMarca" name="dbFormEquipamentoRVMarca" placeholder="Especifique">
                            </div>
                            <h4 class="mb-0 ">QUAL PAPEL VOCÊ USA HOJE?</h4>
                            <div class="form-row">

                                <h5 class="mb-0 text-pink">Gramatura</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoPapelGramaturaRV" name="dbFormEquipamentoPapelGramaturaRV" placeholder="Especifique">
                                <h5 class="mb-0 text-pink">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoPapelMarcaRV" name="dbFormEquipamentoPapelMarcaRV" placeholder="Especifique">


                            </div>
                            <input id="checkboxQualEquipamentoVoceUsaRV8" type="checkbox" >
                            <label for="checkboxQualEquipamentoVoceUsaRV8">Avançar</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divPossuiDuvidasEmRelacaoAoNegocioRV">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/presentation.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h5 class="mb-0 text-pink">POSSUI DÚVIDAS  EM RELAÇÃO AO NEGÓCIO?</h5>
                        </div>
                        <div class="checkbox checkbox-pink">
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioRV1" type="checkbox"  name="dbFormPossuiDuvidasEmRelacaoAoNegocioRV" value="Capacitação em Geral">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioRV1">CAPACITAÇÃO EM GERAL</label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioRV2" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioRV" value="Canecas/Porcelana em Geral">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioRV2">CANECAS/PORCELANAS EM GERAL</label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioRV3" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioRV" value="Confecção">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioRV3">CONFECÇÃO </label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioRV4" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioRV" value="Insumos">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioRV4">INSUMOS </label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioRV5" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioRV" value="Prensas">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioRV5">PRENSAS </label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioRV6" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioRV" value="Plotter de Recorte">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioRV6">PLOTTER DE RECORTE</label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioRV7" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioRV" value="Outros">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioRV7">OUTRO</label>
                            <input type="text" id="OutrosPossuiDuvidasEmRelacaoAoNegocioRV" class="form-control d-none" name="dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioRV" placeholder="Especifique">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divObrigadoRV">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/clipboard.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h5 class="mb-0 text-pink">MUITO OBRIGADO POR SEU TEMPO E ATENÇÃO</h5>
                        </div>
                        <button type="submit" class="btn btn-success waves-effect waves-light"  name="cadastrarRV" id="cadastrarRV"> <i class="fa fa-rocket m-r-5"></i> <span>ENVIAR</span> </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
