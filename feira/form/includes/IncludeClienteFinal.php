<?php


try{
    $conexao = new PDO('mysql:host=localhost;dbname=cafeina_mecolour', 'cafeina_mecolour', 'DB13579**#');
    $conexao ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo 'ERROR:' . $e->getMessage();


};;

if(isset($_POST['cadastrarcf'])){
    $strNome = trim(strip_tags($_POST['strNome']));
    $strEmail = trim(strip_tags($_POST['strEmail']));
    $strEstado = trim(strip_tags($_POST['strEstado']));
    $strCidade = trim(strip_tags($_POST['strCidade']));
    $strCheckbox = trim(strip_tags($_POST['strCheckbox']));
    $dbFormOqueBuscaCF = trim(strip_tags($_POST['dbFormOqueBuscaCF']));
    $dbFormConheceMecolourCF = trim(strip_tags($_POST['dbFormConheceMecolourCF']));
    $dbFormComoSoubeCF = trim(strip_tags($_POST['dbFormComoSoubeCF']));
    $dbFormOutrosComoSoubeCF = trim(strip_tags($_POST['dbFormOutrosComoSoubeCF']));
    $dbFormNegocioProprioCF = trim(strip_tags($_POST['dbFormNegocioProprioCF']));
    $dbFormOutrosNegocioProprioCF = trim(strip_tags($_POST['dbFormOutrosNegocioProprioCF']));
    $dbFormEquipamentoCF = trim(strip_tags($_POST['dbFormEquipamentoCF']));
    $dbFormEquipamentoCFOutro = trim(strip_tags($_POST['dbFormEquipamentoCFOutro']));
    $dbFormEquipamentoCFMarca = trim(strip_tags($_POST['dbFormEquipamentoCFMarca']));
    $dbFormEquipamentoPapelGramaturaCF = trim(strip_tags($_POST['dbFormEquipamentoPapelGramaturaCF']));
    $dbFormEquipamentoPapelMarcaCF = trim(strip_tags($_POST['dbFormEquipamentoPapelMarcaCF']));
    $dbFormPossuiDuvidasEmRelacaoAoNegocioCF = trim(strip_tags($_POST['dbFormPossuiDuvidasEmRelacaoAoNegocioCF']));
    $dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioCF = trim(strip_tags($_POST['dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioCF']));



    $insert = "INSERT INTO dbformcf (strNome, strEmail, strEstado, strCidade, strCheckbox, dbFormOqueBuscaCF, dbFormConheceMecolourCF, dbFormComoSoubeCF, dbFormOutrosComoSoubeCF, dbFormNegocioProprioCF, dbFormOutrosNegocioProprioCF, dbFormEquipamentoCF, dbFormEquipamentoCFOutro, dbFormEquipamentoCFMarca, dbFormEquipamentoPapelGramaturaCF, dbFormEquipamentoPapelMarcaCF, dbFormPossuiDuvidasEmRelacaoAoNegocioCF, dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioCF ) VALUES (:strNome, :strEmail, :strEstado, :strCidade, :strCheckbox, :dbFormOqueBuscaCF, :dbFormConheceMecolourCF, :dbFormComoSoubeCF, :dbFormOutrosComoSoubeCF, :dbFormNegocioProprioCF, :dbFormOutrosNegocioProprioCF, :dbFormEquipamentoCF, :dbFormEquipamentoCFOutro, :dbFormEquipamentoCFMarca, :dbFormEquipamentoPapelGramaturaCF, :dbFormEquipamentoPapelMarcaCF, :dbFormPossuiDuvidasEmRelacaoAoNegocioCF, :dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioCF )";
    try{

        $result = $conexao->prepare($insert);
        $result->bindParam(':strNome', $strNome, PDO::PARAM_STR);
        $result->bindParam(':strEmail', $strEmail, PDO::PARAM_STR);
        $result->bindParam(':strEstado', $strEstado, PDO::PARAM_STR);
        $result->bindParam(':strCidade', $strCidade, PDO::PARAM_STR);
        $result->bindParam(':strCheckbox', $strCheckbox, PDO::PARAM_STR);
        $result->bindParam(':dbFormOqueBuscaCF', $dbFormOqueBuscaCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormConheceMecolourCF', $dbFormConheceMecolourCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormComoSoubeCF', $dbFormComoSoubeCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormOutrosComoSoubeCF', $dbFormOutrosComoSoubeCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormNegocioProprioCF', $dbFormNegocioProprioCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormOutrosNegocioProprioCF', $dbFormOutrosNegocioProprioCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoCF', $dbFormEquipamentoCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoCFOutro', $dbFormEquipamentoCFOutro, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoCFMarca', $dbFormEquipamentoCFMarca, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoPapelGramaturaCF', $dbFormEquipamentoPapelGramaturaCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormEquipamentoPapelMarcaCF', $dbFormEquipamentoPapelMarcaCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormPossuiDuvidasEmRelacaoAoNegocioCF', $dbFormPossuiDuvidasEmRelacaoAoNegocioCF, PDO::PARAM_STR);
        $result->bindParam(':dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioCF', $dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioCF, PDO::PARAM_STR);

        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                echo ' <script type="text/javascript">
                        function sweet() {
                            swal(
                                \'Sucesso!\',
                                \'Pesquisa eviada com êxito\',
                                \'success\'
                            );
                        }
                        window.onload = sweet;
                        window.setTimeout(function() {
        window.location.href=\'http://feira.cafeina.digital/form/ClassCF.php\';
    }, 5000);
                    </script>';
            }
        }else{
            echo '  <script type="text/javascript">
                                         function sweet() {
                                             swal(
                                                 \'Ops, algo de errado\',
                                                 \'contate o suporte \',
                                                 \'error\'
                                             );
                                         }
                                         window.onload = sweet;
                                     </script>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>
<div class="row">


    <div class="col-md-12 text-center d-none" id="divOqueEstaBuscandoNaFeiraCF">
        <div class="card-box">


            <div class="row">
                <div class="col-lg-12 col-xl-7">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/target.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-purple">O QUE VOCÊ ESTÁ BUSCANDO NA FEIRA HOJE?</h4>
                            <input type="text" class="form-control" id="dbFormOqueBuscaCF" name="dbFormOqueBuscaCF" placeholder="Especifique">
                        </div>

                    </div>

                </div>
                <div class="col-lg-12 col-xl-5">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/megaphone.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-purple">VOCÊ CONHECE A MECOLOUR?</h4>
                        </div>
                        <div class="checkbox checkbox-purple">
                            <input id="checkboxConheceCFSim" type="checkbox"  name="dbFormConheceMecolourCF" value="Sim">
                            <label for="checkboxConheceCFSim">SIM </label>
                            <input id="checkboxConheceCFNao" type="checkbox" name="dbFormConheceMecolourCF" value="Não">
                            <label for="checkboxConheceCFNao">NÃO </label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divComoSoubeDaMecolourCF">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/megaphone.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-purple">COMO SOUBE DA MECOLOUR?</h4>
                        </div>
                        <div class="checkbox checkbox-purple">
                            <input id="checkboxComoSoubeMecolourCF1" type="checkbox" name="dbFormComoSoubeCF" value="Pesquisa" >
                            <label for="checkboxComoSoubeMecolourCF1">PESQUISA </label>
                            <input id="checkboxComoSoubeMecolourCF2" type="checkbox" name="dbFormComoSoubeCF" value="Indicação">
                            <label for="checkboxComoSoubeMecolourCF2">INDICAÇÃO </label>
                            <input id="checkboxComoSoubeMecolourCF4" type="checkbox" name="dbFormComoSoubeCF" value="Internet">
                            <label for="checkboxComoSoubeMecolourCF4">OUTROS </label>
                            <input type="text" id="OutrosComoSoubeMecolourCF" class="form-control d-none" name="dbFormOutrosComoSoubeCF" placeholder="Especifique">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12 text-center d-none" id="divQualSuaAtuacaoCF">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/brain.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-purple">QUAL SUA ATUAÇÃO?</h4>
                        </div>
                        <div class="checkbox checkbox-purple">
                            <input id="checkboxNegocioProprioCF1" type="checkbox" name="dbFormNegocioProprioCF" value="Brindes">
                            <label for="checkboxNegocioProprioCF1">BRINDES </label>
                            <input id="checkboxNegocioProprioCF2" type="checkbox" name="dbFormNegocioProprioCF" value="Transfer">
                            <label for="checkboxNegocioProprioCF2">TRANSFER </label>
                            <input id="checkboxNegocioProprioCF3" type="checkbox" name="dbFormNegocioProprioCF" value="Silk">
                            <label for="checkboxNegocioProprioCF3">SILK </label>
                            <input id="checkboxNegocioProprioCF4" type="checkbox" name="dbFormNegocioProprioCF" value="Sublimação">
                            <label for="checkboxNegocioProprioCF4">SUBLIMAÇÃO </label>
                            <input id="checkboxNegocioProprioCF5" type="checkbox" name="dbFormNegocioProprioCF" value="Canecas/Geral">
                            <label for="checkboxNegocioProprioCF5">CANECAS/GERAL </label>
                            <input id="checkboxNegocioProprioCF6" type="checkbox" name="dbFormNegocioProprioCF" value="Confecção">
                            <label for="checkboxNegocioProprioCF6">CONFECÇÃO </label>
                            <input id="checkboxNegocioProprioCF7" type="checkbox" name="dbFormNegocioProprioCF" value="Outros">
                            <label for="checkboxNegocioProprioCF7">OUTROS </label>
                            <input type="text" id="OutrosNegocioProprioCF" class="form-control d-none" name="dbFormOutrosNegocioProprioCF" placeholder="Especifique">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divQualEquipamentoVoceUsaCF">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/brain.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-purple">QUAL EQUIPAMENTO VOCÊ USA HOJE?</h4>
                        </div>
                        <div class="checkbox checkbox-purple">
                            <input id="checkboxQualEquipamentoVoceUsaCF1" type="checkbox" name="dbFormEquipamentoCF" value="PEQ. A3/A4">
                            <label for="checkboxQualEquipamentoVoceUsaCF1">PEQ. A3/A4</label>
                            <input id="checkboxQualEquipamentoVoceUsaCF2" type="checkbox" name="dbFormEquipamentoCF" value="GDE 1,60M">
                            <label for="checkboxQualEquipamentoVoceUsaCF2">GDE 1,60M</label>
                            <input id="checkboxQualEquipamentoVoceUsaCF3" type="checkbox" name="dbFormEquipamentoCF" value="GDE 1,80M">
                            <label for="checkboxQualEquipamentoVoceUsaCF3">GDE 1,80</label>
                            <input id="checkboxQualEquipamentoVoceUsaCF4" type="checkbox" name="dbFormEquipamentoCF" value="GDE 2,60M">
                            <label for="checkboxQualEquipamentoVoceUsaCF4">GDE 2,60</label>
                            <div class="form-row">
                                <h5 class="mb-0 text-purple">Outro</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoCFOutro" name="dbFormEquipamentoCFOutro" placeholder="Especifique">
                                <h5 class="mb-0 text-purple">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoCFMarca" name="dbFormEquipamentoCFMarca" placeholder="Especifique">
                            </div>
                            <h4 class="mb-0 text-purple">QUAL PAPEL VOCÊ USA HOJE?</h4>
                            <div class="form-row">

                                <h5 class="mb-0 text-purple">Gramatura</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoPapelGramaturaCF" name="dbFormEquipamentoPapelGramaturaCF" placeholder="Especifique">
                                <h5 class="mb-0 text-purple">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoPapelMarcaCF" name="dbFormEquipamentoPapelMarcaCF" placeholder="Especifique">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12 text-center d-none" id="divQualEquipamentoVoceUsaCF2">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/brain.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-purple">QUAL EQUIPAMENTO VOCÊ USA HOJE?</h4>
                        </div>
                        <div class="checkbox checkbox-purple">

                            <div class="form-row">
                                <h5 class="mb-0 text-purple">Tipo de Produto</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoCFOutro" name="dbFormEquipamentoCFOutro" placeholder="Especifique">
                                <h5 class="mb-0 text-purple">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoCFMarca" name="dbFormEquipamentoCFMarca" placeholder="Especifique">

                            </div>
                            <input id="checkboxQualEquipamentoVoceUsaCF7" type="checkbox" >
                            <label for="checkboxQualEquipamentoVoceUsaCF7">Avançar</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divQualEquipamentoVoceUsaCF3">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/brain.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-purple">QUAL EQUIPAMENTO VOCÊ USA HOJE?</h4>
                        </div>
                        <div class="checkbox checkbox-purple">

                            <div class="form-row">
                                <h5 class="mb-0 text-purple">Tipo de Produto</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoCFOutro" name="dbFormEquipamentoCFOutro" placeholder="Especifique">
                                <h5 class="mb-0 text-purple">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoCFMarca" name="dbFormEquipamentoCFMarca" placeholder="Especifique">
                            </div>
                            <h4 class="mb-0 text-purple">QUAL PAPEL VOCÊ USA HOJE?</h4>
                            <div class="form-row">

                                <h5 class="mb-0 text-purple">Gramatura</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoPapelGramaturaCF" name="dbFormEquipamentoPapelGramaturaCF" placeholder="Especifique">
                                <h5 class="mb-0 text-purple">Marca</h5>
                                <input type="text" class="form-control" id="dbFormEquipamentoPapelMarcaCF" name="dbFormEquipamentoPapelMarcaCF" placeholder="Especifique">


                            </div>
                            <input id="checkboxQualEquipamentoVoceUsaCF8" type="checkbox" >
                            <label for="checkboxQualEquipamentoVoceUsaCF8">Avançar</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divPossuiDuvidasEmRelacaoAoNegocioCF">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/presentation.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h5 class="mb-0 text-purple">POSSUI DÚVIDAS  EM RELAÇÃO AO NEGÓCIO?</h5>
                        </div>
                        <div class="checkbox checkbox-purple">
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioCF1" type="checkbox"  name="dbFormPossuiDuvidasEmRelacaoAoNegocioCF" value="Capacitação em Geral">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioCF1">CAPACITAÇÃO EM GERAL</label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioCF2" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioCF" value="Canecas/Porcelana em Geral">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioCF2">CANECAS/PORCELANAS EM GERAL</label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioCF3" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioCF" value="Confecção">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioCF3">CONFECÇÃO </label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioCF4" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioCF" value="Insumos">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioCF4">INSUMOS </label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioCF5" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioCF" value="Prensas">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioCF5">PRENSAS </label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioCF6" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioCF" value="Plotter de Recorte">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioCF6">PLOTTER DE RECORTE</label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioCF7" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioCF" value="Outros">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioCF7">OUTRO</label>
                            <input type="text" id="OutrosPossuiDuvidasEmRelacaoAoNegocioCF" class="form-control d-none" name="dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioCF" placeholder="Especifique">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divObrigadoCF">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <!--                        <div class="file-img-box">-->
                        <!--                            <img src="assets/images/file_icons/clipboard.svg" alt="icon">-->
                        <!--                        </div>-->
                        <div class="file-man-title">
                            <h5 class="mb-0 text-purple">MUITO OBRIGADO POR SEU TEMPO E ATENÇÃO</h5>
                        </div>
                        <button type="submit" class="btn btn-success waves-effect waves-light"  name="cadastrarCF" id="cadastrarCF"> <i class="fa fa-rocket m-r-5"></i> <span>ENVIAR</span> </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
