<?php

try{
    $conexao = new PDO('mysql:host=localhost;dbname=cafeina_mecolour', 'cafeina_mecolour', 'DB13579**#');
    $conexao ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo 'ERROR:' . $e->getMessage();


};
if(isset($_POST['cadastrar'])){
    $strNome = trim(strip_tags($_POST['strNome']));
    $strEmail = trim(strip_tags($_POST['strEmail']));
    $strEstado = trim(strip_tags($_POST['strEstado']));
    $strCidade = trim(strip_tags($_POST['strCidade']));
    $strCheckbox = trim(strip_tags($_POST['strCheckbox']));
    $dbFormOqueBuscaNP = trim(strip_tags($_POST['dbFormOqueBuscaNP']));
    $dbFormConheceMecolourNP = trim(strip_tags($_POST['dbFormConheceMecolourNP']));
    $dbFormComoSoubeNP = trim(strip_tags($_POST['dbFormComoSoubeNP']));
    $dbFormEstaBuscandoAlgoParaInvestirNP = trim(strip_tags($_POST['dbFormEstaBuscandoAlgoParaInvestirNP']));
    $dbFormNegocioProprioNP = trim(strip_tags($_POST['dbFormNegocioProprioNP']));
    $dbFormPossuiDuvidasEmRelacaoAoNegocioNP = trim(strip_tags($_POST['dbFormPossuiDuvidasEmRelacaoAoNegocioNP']));
    $dbFormComoSoubeOutrosNP = trim(strip_tags($_POST['dbFormComoSoubeOutrosNP']));
    $dbFormOutrosNegocioProprioNP = trim(strip_tags($_POST['dbFormOutrosNegocioProprioNP']));
    $dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioNP = trim(strip_tags($_POST['dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioNP']));



    $insert = "INSERT INTO dbform (strNome, strEmail, strEstado, strCidade, strCheckbox, dbFormOqueBuscaNP, dbFormConheceMecolourNP, dbFormComoSoubeNP, dbFormEstaBuscandoAlgoParaInvestirNP, dbFormNegocioProprioNP, dbFormPossuiDuvidasEmRelacaoAoNegocioNP, dbFormComoSoubeOutrosNP, dbFormOutrosNegocioProprioNP, dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioNP ) VALUES ( :strNome, :strEmail, :strEstado, :strCidade, :strCheckbox, :dbFormOqueBuscaNP, :dbFormConheceMecolourNP, :dbFormComoSoubeNP, :dbFormEstaBuscandoAlgoParaInvestirNP, :dbFormNegocioProprioNP, :dbFormPossuiDuvidasEmRelacaoAoNegocioNP, :dbFormComoSoubeOutrosNP, :dbFormOutrosNegocioProprioNP, :dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioNP )";
    try{

        $result = $conexao->prepare($insert);
        $result->bindParam(':strNome', $strNome, PDO::PARAM_STR);
        $result->bindParam(':strEmail', $strEmail, PDO::PARAM_STR);
        $result->bindParam(':strEstado', $strEstado, PDO::PARAM_STR);
        $result->bindParam(':strCidade', $strCidade, PDO::PARAM_STR);
        $result->bindParam(':strCheckbox', $strCheckbox, PDO::PARAM_STR);
        $result->bindParam(':dbFormOqueBuscaNP', $dbFormOqueBuscaNP, PDO::PARAM_STR);
        $result->bindParam(':dbFormConheceMecolourNP', $dbFormConheceMecolourNP, PDO::PARAM_STR);
        $result->bindParam(':dbFormComoSoubeNP', $dbFormComoSoubeNP, PDO::PARAM_STR);
        $result->bindParam(':dbFormEstaBuscandoAlgoParaInvestirNP', $dbFormEstaBuscandoAlgoParaInvestirNP, PDO::PARAM_STR);
        $result->bindParam(':dbFormNegocioProprioNP', $dbFormNegocioProprioNP, PDO::PARAM_STR);
        $result->bindParam(':dbFormPossuiDuvidasEmRelacaoAoNegocioNP', $dbFormPossuiDuvidasEmRelacaoAoNegocioNP, PDO::PARAM_STR);
        $result->bindParam(':dbFormComoSoubeOutrosNP', $dbFormComoSoubeOutrosNP, PDO::PARAM_STR);
        $result->bindParam(':dbFormOutrosNegocioProprioNP', $dbFormOutrosNegocioProprioNP, PDO::PARAM_STR);
        $result->bindParam(':dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioNP', $dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioNP, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                echo ' <script type="text/javascript">
                        function sweet() {
                            swal(
                                \'Sucesso!\',
                                \'Pesquisa eviada com êxito\',
                                \'success\'
                            );
                        }
                        window.onload = sweet;
                    </script>';
            }
        }else{
           echo '  <script type="text/javascript">
                                         function sweet() {
                                             swal(
                                                 \'Ops, algo de errado\',
                                                 \'contate o suporte \',
                                                 \'error\'
                                             );
                                         }
                                         window.onload = sweet;
                                     </script>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>

<div class="row">


    <div class="col-md-12 text-center d-none" id="divOqueEstaBuscandoNaFeiraNP">
        <div class="card-box">


            <div class="row">
                <div class="col-lg-12 col-xl-7">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/target.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-success">O QUE VOCÊ ESTÁ BUSCANDO NA FEIRA HOJE?</h4>
                            <input type="text" class="form-control" id="dbFormOqueBuscaNP" name="dbFormOqueBuscaNP" placeholder="Especifique">
                        </div>

                    </div>

                </div>
                <div class="col-lg-12 col-xl-5">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/megaphone.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-success">VOCÊ CONHECE A MECOLOUR?</h4>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="checkboxConheceNp1Sim" type="checkbox"  name="dbFormConheceMecolourNP" value="Sim">
                            <label for="checkboxConheceNp1Sim">SIM </label>
                            <input id="checkboxConheceNp2Nao" type="checkbox" name="dbFormConheceMecolourNP" value="Não">
                            <label for="checkboxConheceNp2Nao">NÃO </label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divComoSoubeDaMecolourNP">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/megaphone.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-success">COMO SOUBE DA MECOLOUR?</h4>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="checkboxComoSoubeMecolourNP1" type="checkbox" name="dbFormComoSoubeNP" value="Pesquisa" >
                            <label for="checkboxComoSoubeMecolourNP1">PESQUISA </label>
                            <input id="checkboxComoSoubeMecolourNP2" type="checkbox" name="dbFormComoSoubeNP" value="Indicação">
                            <label for="checkboxComoSoubeMecolourNP2">INDICAÇÃO </label>
                            <input id="checkboxComoSoubeMecolourNP3" type="checkbox" name="dbFormComoSoubeNP" value="Internet">
                            <label for="checkboxComoSoubeMecolourNP3">INTERNET </label>
                            <input id="checkboxComoSoubeMecolourNP4" type="checkbox" name="dbFormComoSoubeNP" value="Outros">
                            <label for="checkboxComoSoubeMecolourNP4">OUTROS </label>
                            <input type="text" id="outrosOqueBusca" class="form-control d-none" name="dbFormComoSoubeOutrosNP" placeholder="Especifique">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divEstaBuscandoAlgoParaInvestir">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/handshake.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-success">ESTÁ BUSCANDO ALGO PARA INVESTIR?</h4>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="checkboxEstaBuscandoAlgoParaInvestirSim" type="checkbox"  name="dbFormEstaBuscandoAlgoParaInvestirNP" value="Sim">
                            <label for="checkboxEstaBuscandoAlgoParaInvestirSim">SIM </label>
                            <input id="checkboxEstaBuscandoAlgoParaInvestirNao" type="checkbox"  name="dbFormEstaBuscandoAlgoParaInvestirNP" value="Não">
                            <label for="checkboxEstaBuscandoAlgoParaInvestirNao">NÃO </label>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 text-center d-none" id="divNegocioProprioNP">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/brain.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-success">NEGÓCIO PRÓPRIO:</h4>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="checkboxNegocioProprioNP1" type="checkbox" name="dbFormNegocioProprioNP" value="Loja">
                            <label for="checkboxNegocioProprioNP1">LOJA </label>
                            <input id="checkboxNegocioProprioNP2" type="checkbox" name="dbFormNegocioProprioNP" value="Personalização de Brindes">
                            <label for="checkboxNegocioProprioNP2">PERSONALIZAÇÃO DE BRINDES </label>
                            <input id="checkboxNegocioProprioNP3" type="checkbox" name="dbFormNegocioProprioNP" value="Revenda">
                            <label for="checkboxNegocioProprioNP3">REVENDA </label>
                            <input id="checkboxNegocioProprioNP4" type="checkbox" name="dbFormNegocioProprioNP" value="Outro">
                            <label for="checkboxNegocioProprioNP4">OUTRO </label>
                            <input type="text" id="outrosNegocioProprioNP" class="form-control d-none"  name="dbFormOutrosNegocioProprioNP" placeholder="Especifique">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divObrigadoNP">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/clipboard.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-success">MUITO OBRIGADO POR SEU TEMPO E ATENÇÃO</h4>
                        </div>
                        <button type="submit" class="btn btn-success waves-effect waves-light" name="cadastrar" id="cadastrar"> <i class="fa fa-rocket m-r-5"></i> <span>ENVIAR</span> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="col-md-12 text-center d-none" id="divPossuiDuvidasEmRelacaoAoNegocio">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
<!--                        <div class="file-img-box">-->
<!--                            <img src="assets/images/file_icons/presentation.svg" alt="icon">-->
<!--                        </div>-->
                        <div class="file-man-title">
                            <h4 class="mb-0 text-success">POSSUI DÚVIDAS  EM RELAÇÃO AO NEGÓCIO?</h4>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioNP1" type="checkbox"  name="dbFormPossuiDuvidasEmRelacaoAoNegocioNP" value="Capacitação em Geral">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioNP1">CAPACITAÇÃO EM GERAL</label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioNP2" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioNP" value="Canecas/Porcelanas em Geral">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioNP2">CANECAS/PORCELANAS EM GERAL</label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioNP3" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioNP" value="Confecção">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioNP3">CONFECÇÃO </label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioNP4" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioNP" value="Isumos">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioNP4">INSUMOS </label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioNP5" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioNP" value="Prensas">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioNP5">PRENSAS </label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioNP6" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioNP" value="Plotter de Recorte">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioNP6">PLOTTER DE RECORTE</label>
                            <input id="checkboxPossuiDuvidasEmRelacaoAoNegocioNP7" type="checkbox" name="dbFormPossuiDuvidasEmRelacaoAoNegocioNP" value="Outros">
                            <label for="checkboxPossuiDuvidasEmRelacaoAoNegocioNP7">OUTRO</label>
                            <input type="text" id="outrosPossuiDuvidasEmRelacaoAoNegocioNP7" class="form-control d-none"  name="dbFormOutrosPossuiDuvidasEmRelacaoAoNegocioNP" placeholder="Especifique">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center d-none" id="divObrigadoNP2">
        <div class="card-box">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="file-man-box">
                        <div class="file-img-box">
                            <img src="assets/images/file_icons/clipboard.svg" alt="icon">
                        </div>
                        <div class="file-man-title">
                            <h4 class="mb-0 text-success">MUITO OBRIGADO POR SEU TEMPO E ATENÇÃO</4>
                        </div>
                        <button type="submit" class="btn btn-success waves-effect waves-light"  name="cadastrar" id="cadastrar"> <i class="fa fa-rocket m-r-5"></i> <span>ENVIAR</span> </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>