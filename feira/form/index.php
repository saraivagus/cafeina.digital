<?php ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mecolour Pesquisa Qualitativa</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- App css -->
    <!-- Sweet Alert css -->
    <link href="assets/plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <script src="assets/js/modernizr.min.js"></script>
</head>
<body>
<div id="wrapper">
    <div class="content-page">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <a href="index.php"> <h4 class="m-t-0 header-title text-center"><img src="assets/images/logo.png" width="200px"></h4></a>
                            <p class="text-muted m-b-30 font-14 text-center">
                                PESQUISA QUALITATIVA MECOLOUR
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <form class="form-signin" action="#" method="post" enctype="multipart/form-data">
                <div class="col-md-12 text-center">
                    <div class="card-box">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="strNome" class="col-form-label pull-left">Nome</label>
                                <input type="text" class="form-control" id="strNome" name="strNome" placeholder="Insira seu Nome" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="strEmail" class="col-form-label pull-left">E-mail</label>
                                <input type="email" class="form-control" id="strEmail" name="strEmail" placeholder="Insira seu E-mail" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="strCidade" class="col-form-label pull-left">Estado</label>
                                <select id="estados" name="strEstado" class="form-control" required><option> Choose</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="strCidade" class="col-form-label pull-left">Cidade</label>
                                <select id="cidades" name="strCidade" class="form-control" required>
                                    <option> Choose</option></select>
                            </div>

                        </div>
                        <div class="row ">
                            <div class="col-lg-12 col-xl-4">
                                <div class="file-man-box">
                                    <div class="file-img-box">
                                        <img src="assets/images/file_icons/brain.svg" alt="icon">
                                    </div>
                                    <div class="file-man-title">
                                        <h5 class="mb-0 text-success">BUSCANDO OPORTUNIDADE DE INVESTIMENTO</h5>
                                    </div>

                                    <div class="radio radio-success">
                                        <input type="radio" id="checkboxNP"   name="strCheckbox"  value="BUSCANDO INVESTIMENTO">
                                        <label for="checkboxNP">
                                        </label>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-4">
                                <div class="file-man-box">
                                    <div class="file-img-box">
                                        <img src="assets/images/file_icons/money-exchange.svg" alt="icon">
                                    </div>
                                    <div class="file-man-title">
                                        <h5 class="mb-0 text-purple">SOU CLIENTE <br>FINAL</h5>
                                    </div>
                                    <div class="radio radio-purple">
                                        <input type="radio" id="checkboxCF" name="strCheckbox"  value="CLIENTE FINAL">
                                        <label for="checkboxCF">

                                        </label>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-4">
                                <div class="file-man-box">
                                    <div class="file-img-box">
                                        <img src="assets/images/file_icons/meeting.svg" alt="icon">
                                    </div>
                                    <div class="file-man-title">
                                        <h5 class="mb-0 text-pink">SOU <br>REVENDEDOR</h5>
                                    </div>
                                    <div class="radio radio-pink">
                                        <input  type="radio"   id="checkboxRV" name="strCheckbox" value="REVENDEDOR">
                                        <label for="checkboxRV">

                                        </label>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

<!--                <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>-->
<!--                    <strong>Usuário</strong> cadastrado com sucesso-->
<!--                </div>-->
                <?php include_once 'includes/IncludeNegocioProprio.php'; ?>
                <?php include_once 'includes/IncludeClienteFinal.php'; ?>
                <?php include_once 'includes/IncludeRevenda.php'; ?>

            </form>
        </div>


    </div>
    <footer class="footer text-center">
        2018 © Desenvolvido por - Cafeína Digital
    </footer>
</div>
<script src="assets/js/jquery-1.7.1.js"></script>
<script src="assets/js/CustomClienteFinal.js"></script>
<script src="assets/js/CustomNegocioProprio.js"></script>
<script src="assets/js/CustomRevenda.js"></script>
<script src="assets/js/populaCamposEstadoCidade.js"></script>

<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/metisMenu.min.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<!-- Sweet Alert Js  -->
<script src="assets/plugins/sweet-alert/sweetalert2.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- App js -->
<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>
</body>
</html>