<!doctype html>
<html lang="en">
    <head>
        <?php include("includes/head.php"); ?>
    </head>
    <body>
    <?php include("includes/preloader.php"); ?>
        <div class="header-1">
            <?php include("includes/navbar.php"); ?>
            <div class="page-header" style="background-image: url('assets/img/imgBgHome.png');">
                <div class="container">
                    <div class="row">

                        <?php include("includes/header.php"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="header">
            </div>
            <div class="main">
                <div class="container">
                    <?php include("includes/conheca.php"); ?>
                </div>
                <?php include("includes/tipoCorpo.php"); ?>
                <?php include("includes/videos.php"); ?>
                <?php include("includes/missao.php"); ?>
            </div>
        </div>
        <?php include("includes/footer.php"); ?>
        <?php include("includes/modals.php"); ?>
    </body>
    <!--   Core JS Files   -->
    <?php include("includes/scripts.php"); ?>
</html>
