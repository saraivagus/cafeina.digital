<!doctype html>
<html lang="en">
<head>
    <?php include("includes/head.php"); ?>
</head>
<?php include("includes/preloader.php"); ?>
<body>
        <div class="header-1">
          <?php include("includes/navbar.php"); ?>

            <?php include("pages/tactical/header.php"); ?>
        </div>
        <div class="wrapper">
            <div class="header">
            </div>
            <!-- you can use the class main-raised if you want the main area to be as a page with shadows -->

            <div class="main">
                <div class="container">
                  <ul class="list-inline text-center" style="padding-top:30px">
                  <li data-toggle="modal" data-target="#modalTactical1"><a href="#myGallery" data-slide-to="0">
                          <img class="img-thumbnail" src="assets/img/pgProduto/tactical/produtos/modeloAggressive180.png" alt="CADEIRA MAXRACER TACTICAL DETALHE DEITADA" title="MAXRACER TACTICAL"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalTactical2"><a href="#myGallery" data-slide-to="1">
                          <img class="img-thumbnail" src="assets/img/pgProduto/tactical/produtos/modeloAggressiveFrente.png" alt="CADEIRA MAXRACER TACTICAL DETALHE FRENTE" title="MAXRACER TACTICAL"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalTactical3"><a href="#myGallery" data-slide-to="2">
                          <img class="img-thumbnail" src="assets/img/pgProduto/tactical/produtos/modeloAggressiveFrenteAlmofada.png" alt="CADEIRA MAXRACER TACTICAL DETALHE FRENTE ALMOFADA" title="MAXRACER TACTICAL"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalTactical4"><a href="#myGallery" data-slide-to="3">
                          <img class="img-thumbnail" src="assets/img/pgProduto/tactical/produtos/modeloAggressiveLateral.png" alt="CADEIRA MAXRACER TACTICAL DETALHE LATERAL" title="MAXRACER TACTICAL"><br>
                </a></li>
                <!--end of thumbnails-->
                </ul>
                </div>
                <?php include("pages/tactical/medidas.php"); ?>
            </div>


        </div>
        <?php include("includes/footer.php"); ?>
        <?php include("pages/tactical/modal/modalTactical.php"); ?>

    </body>
<?php include("includes/scripts.php"); ?>
</html>
