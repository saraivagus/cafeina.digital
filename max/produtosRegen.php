<!doctype html>
<html lang="en">
<head>
    <?php include("includes/head.php"); ?>
</head>
<?php include("includes/preloader.php"); ?>
<body>
<div class="header-1">
          <?php include("includes/navbar.php"); ?>
          <?php include("pages/regen/header.php"); ?>
        </div>
        <div class="wrapper">
            <div class="header">
            </div>
            <!-- you can use the class main-raised if you want the main area to be as a page with shadows -->
            <div class="main">
                <div class="container">


                  <ul class="list-inline text-center" style="padding-top:30px">
                  <li data-toggle="modal" data-target="#modalRegen1"><a href="#myGallery" data-slide-to="0">
                          <img class="img-thumbnail" src="assets/img/pgProduto/regen/produtos/modeloRegenThumbLateral.png" alt="CADEIRA MAXRACER REGEN DETALHE LATERAL" title="MAXRACER REGEN"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalRegen2"><a href="#myGallery" data-slide-to="1">
                          <img class="img-thumbnail" src="assets/img/pgProduto/regen/produtos/modeloRegenThumbCostas.png" alt="CADEIRA MAXRACER REGEN DETALHE COSTAS" title="MAXRACER REGEN"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalRegen3"><a href="#myGallery" data-slide-to="2">
                          <img class="img-thumbnail" src="assets/img/pgProduto/regen/produtos/modeloRegenThumb180.png" alt="CADEIRA MAXRACER REGEN DETALHE DEITADA 180 GRAUS" title="MAXRACER REGEN"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalRegen4"><a href="#myGallery" data-slide-to="3">
                          <img class="img-thumbnail" src="assets/img/pgProduto/regen/produtos/modeloRegenThumb45.png" alt="CADEIRA MAXRACER REGEN DETALHE DEITADA" title="MAXRACER REGEN"><br>
                </a></li>
                <!--end of thumbnails-->
                </ul>

                </div>
                <?php include("pages/regen/medidas.php"); ?>
            </div>
        </div>
        <?php include("includes/footer.php"); ?>
    </body>
          <?php include("pages/regen/modal/modalRegen.php"); ?>
<?php include("includes/scripts.php"); ?>
</html>
