


<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index"><img src="assets/img/logo.png"></a>
        </div>
        <div class="collapse navbar-collapse" id="navigation-example">
          <ul class="nav navbar-nav navbar-right">
              <li>
                  <a href="index">
                  Home
                  </a>
              </li>
              <li>
                  <a href="index#conheca" data-scroll>
                  Sobre a MAXRACER
                  </a>
              </li>
              <li>
                  <a href="http://maxracer.com.br/loja/contato/" target="_blank">
                  Contato
                  </a>
              </li>
              <li>
                <a href="http://www.maxracer.com.br/loja" target="_blank" class="btn btn-warning">Loja</a>
              </li>
              <li>
                  <a href="https://twitter.com/maxraceroficial" target="_blank">
                  <i class="fa fa-twitter"></i>
                  </a>
              </li>
              <li>
                  <a href="https://www.facebook.com/maxracerbrasil/" target="_blank">
                  <i class="fa fa-facebook-square"></i>
                  </a>
              </li>
              <li>
                  <a href="https://www.youtube.com/channel/UCv7dBHV8C8edO-UYPFgegEw" target="_blank">
                  <i class="fa fa-youtube"></i>
                  </a>
              </li>
          </ul>
        </div>
    </div>
</nav>
