<footer class="footer footer-black footer-big">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <a href="#pablo"><img src="assets/img/logo.png" alt="MAXRACER PREMIUM GAMING CHAIR" title="MAXRACER PREMIUM GAMING CHAIR" width="150px"></a><br><br>
                    <p>Rua Vidal Ramos – 579 SL 08
                        Guanabara / Joinville – SC<br><br>
                        Endereço Centro de Distribuição:
                        Alameda Itajubá, 2234 - Joapiranga ll
                        Valinhos – SP
                        Cep: 13278-530
                    </p>
                </div>
                <div class="col-md-3 col-md-offset-1">
                    <h5>MAPA DO SITE</h5>
                    <ul class="links-vertical">
                        <li>
                            <a href="index.php">
                            HOME
                            </a>
                        </li>
                        <li>
                            <a href="index.php#conheca">
                            SOBRE A MAXRACER
                            </a>
                        </li>
                        <li>
                            <a href="http://maxracer.com.br/loja/contato/" target="_blank">
                            CONTATO
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5>INFORMAÇÕES</h5>
                    <ul class="links-vertical">
                        <p> Entre em contato conosco!
                            Conheça nossos produtos e
                            cadastre-se para ser um revendedor
                            autorizado!
                            SAC@MAXRACER.COM.BR
                        </p>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <ul class="social-buttons">
            <li>
                <a href="https://twitter.com/maxraceroficial" class="btn btn-just-icon btn-simple btn-twitter" target="_blank">
                <i class="fa fa-twitter"></i>
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com/maxracerbrasil/" class="btn btn-just-icon btn-simple btn-facebook" target="_blank">
                <i class="fa fa-facebook-square"></i>
                </a>
            </li>
            <li>
                <a href="https://www.youtube.com/channel/UCv7dBHV8C8edO-UYPFgegEw" class="btn btn-just-icon btn-simple btn-youtube" target="_blank">
                <i class="fa fa-youtube-play"></i>
                </a>
            </li>
        </ul>
        <div class="copyright pull-left">
            Copyright © MAXRACER <script>document.write(new Date().getFullYear())</script> Todos direitos reservados
        </div>
        <div class="copyright pull-right">
            Desenvolvido por <a href="http://www.cafeina.digital">Cafeína Digital</a>
        </div>
    </div>
</footer>
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/material-kit.css?v=1.1.0" rel="stylesheet"/>
<link href="assets/css/custom.css" rel="stylesheet"/>
<link href="assets/css/elementFilter.css" rel="stylesheet"/>
<link href="assets/css/imgZindex.css" rel="stylesheet"/>
<link href="assets/css/pre.css" rel="stylesheet"/>


