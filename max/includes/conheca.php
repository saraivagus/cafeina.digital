<div class="features-1" id="conheca">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2 class="title">CONHEÇA A MAXRACER</h2>
            <h5 class="description">A <span class="boldTexto">MAXRACER</span> foi desenvolvida nos mínimos detalhes com paixão e dedicação para que você se sinta <span class="boldTexto">MAX</span>! Temos o objetivo de fabricar cadeiras de altíssimo padrão de qualidade e preço justo para que todos possam ter sua cadeira <span class="boldTexto">gamer</span>!</h5>
        </div>
    </div>
    <div class="section-story-overview">
        <div class="row">
            <div class="col-md-6">
                <div class="image-container image-left" style="background-image: url('assets/img/imgHomeMax1.jpg')">
                    <!-- First image on the left side -->
                    </p>
                </div>
                <!-- Second image on the left side of the article -->
            </div>
            <div class="col-md-6">
                <!-- First image on the right side, above the article -->
                <div class="image-container image-right" style="background-image: url('assets/img/imgHomeMax2.jpg')"></div>
            </div>
        </div>
    </div>
</div>
