<div class="col-md-12 text-center">
    <h1 class="title">BEM VINDO A NOVA <br>ERA GAMER</h1>
    <h4>Desenvolvida por <span class="boldTexto">mentes jovens, brilhantes e experientes</span>! A marca é direcionada para <span class="boldTexto">GAMERS</span> e<br> para pessoas que querem cada vez mais conforto, design, qualidade e modernidade.</h4>
    <br />
    <div class="col-md-4 col-md-offset-4 selecioneProdutoHome">SELECIONE ABAIXO UM DOS MODELOS
        E CONFIRA A LINHA COMPLETA
    </div>
    <div class="dropdown hidden-lg hidden-md hidden-sm">
        <a href="#" class="btn dropdown-toggle" data-toggle="dropdown">
        SELECIONE UMA CADEIRA
        <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="produtosRegen.php">MAXRACER REGEN</a></li>
            <li><a href="produtosPixel.php">MAXRACER PIXEL </a></li>
            <li><a href="produtosTactical.php">MAXRACER TACTICAL</a></li>
            <li><a href="produtosAggressive.php">MAXRACER AGGRESSIVE</a></li>
            <li><a href="produtosSkilled.php">MAXRACER SKILLED </a></li>
            <li><a href="produtosPodium.php">MAXRACER PODIUM</a></li>
        </ul>
    </div>
    <br>
    <div class="col-md-12 produtosHome" id="produtosHome">
        <div class="col-md-1 hidden-xs produtoHomeZoom"> <a href="produtosRegen">
                <img src="assets/img/produtosHome/regenHome.png" alt="MAXRACER REGEN"  data-toggle="tooltip" data-placement="top" title="<img src='assets/img/produtosHome/logoRegenHover.png' />" width="180"></a></div>
        <div class="col-md-1"> </div>
        <div class="col-md-1 hidden-xs produtoHomeZoom"> <a href="produtosPixel">
                <img src="assets/img/produtosHome/pixelHome.png" alt="MAXRACER REGEN"  data-toggle="tooltip" data-placement="top" title="<img src='assets/img/produtosHome/logoPixelHover.png' />" width="180"></a></div>
        <div class="col-md-1"> </div>
        <div class="col-md-1 hidden-xs produtoHomeZoom"> <a href="produtosTactical">
                <img src="assets/img/produtosHome/tacticalHome.png" alt="MAXRACER REGEN"  data-toggle="tooltip" data-placement="top" title="<img src='assets/img/produtosHome/logoTacticalHover.png' />" width="180"></a></div>
        <div class="col-md-1"> </div>
        <div class="col-md-1 hidden-xs produtoHomeZoom"> <a href="produtosAggressive">
                <img src="assets/img/produtosHome/aggressiveHome.png" alt="MAXRACER REGEN"  data-toggle="tooltip" data-placement="top" title="<img src='assets/img/produtosHome/logoAggressiveHover.png' />" width="180"></a></div>
        <div class="col-md-1"> </div>
        <div class="col-md-1 hidden-xs produtoHomeZoom"> <a href="produtosSkilled">
                <img src="assets/img/produtosHome/skilledHome.png" data-toggle="tooltip" data-placement="top" title="<img src='assets/img/produtosHome/logoSkilledHover.png' />" width="180"></a></div>
        <div class="col-md-1"> </div>
        <div class="col-md-1 hidden-xs produtoHomeZoom" style="vertical-align: text-bottom"> <a href="produtosPodium">
                <img src="assets/img/produtosHome/podiumHome.png" alt="MAXRACER REGEN"  data-toggle="tooltip" data-placement="top" title="<img src='assets/img/produtosHome/logoPodiumHover.png' />" ></a></div>
        <div class="container">
            <br/>
        </div>
    </div>
    <br>
</div>
