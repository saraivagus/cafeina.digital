<div class="features-5" style="background-color:#eee">
    <div class="col-md-8 col-md-offset-2 text-center">
        <h2 class=" colorBlackTitulo" >VÍDEOS DA MAX
        </h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card card-profile card-plain">
                    <div class="card-image">
                        <a href="#">
                        <img class="img" src="assets/img/videosMax/imgVideo01.png" alt="Vídeo unbox Bczz" title="Vídeo unbox Bczz">
                        </a>
                        <div style="background-image: url(&quot;assets/img/videosMax/imgVideo01.png&quot;); opacity: 1;"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title" >Unboxing Bczz</h4>
                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalVideoBczz">
                        <i class="material-icons">play_arrow</i>
                        ASSISTIR</button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-profile card-plain">
                    <div class="card-image">
                        <a href="#">
                        <img class="img" src="assets/img/videosMax/imgVideo02.png" alt="Vídeo unbox Gordox" title="Vídeo unbox Gordox">
                        </a>
                        <div style="background-image: url(&quot;assets/img/videosMax/imgVideo02.png&quot;); opacity: 1;"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Unboxing Gordox</h4>
                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalVideoGordox">
                        <i class="material-icons">play_arrow</i>
                        ASSISTIR</button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-profile card-plain">
                    <div class="card-image">
                        <a href="#">
                        <img class="img" src="assets/img/videosMax/imgVideo03.png" alt="Vídeo unbox Jota" title="Vídeo unbox Jota">
                        </a>
                        <div style="background-image: url(&quot;assets/img/videosMax/imgVideo03.png&quot;); opacity: 1;"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title" >Unboxing Jota</h4>
                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalVideoJota">
                        <i class="material-icons">play_arrow</i>
                        ASSISTIR</button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-profile card-plain">
                    <div class="card-image">
                        <a href="#">
                        <img class="img" src="assets/img/videosMax/imgVideo04.png" alt="Vídeo unbox MAXMRM" title="Vídeo unbox MAXMRM">
                        </a>
                        <div style="background-image: url(&quot;assets/img/videosMax/imgVideo01.png&quot;); opacity: 1;"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Unboxing MaxMRM</h4>
                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalVideoMax">
                        <i class="material-icons">play_arrow</i>
                        ASSISTIR</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card card-profile card-plain">
                    <div class="card-image">
                        <a href="#">
                        <img class="img" src="assets/img/videosMax/imgVideo05.png" alt="Vídeo unbox Clodovil do CS" title="Vídeo unbox Clodovil do CS">
                        </a>
                        <div style="background-image: url(&quot;assets/img/videosMax/imgVideo05.png&quot;); opacity: 1;"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Unboxing Clodovil do CS</h4>
                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalVideoClodo">
                        <i class="material-icons">play_arrow</i>
                        ASSISTIR</button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-profile card-plain">
                    <div class="card-image">
                        <a href="#">
                        <img class="img" src="assets/img/videosMax/imgVideo06.png" alt="Vídeo unbox TudoSobrePB" title="Vídeo unbox TudoSobrePB">
                        </a>
                        <div style="background-image: url(&quot;assets/img/videosMax/imgVideo06.png&quot;); opacity: 1;"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Unboxing TudoSobrePB</h4>
                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalVideoT">
                        <i class="material-icons">play_arrow</i>
                        ASSISTIR</button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-profile card-plain">
                    <div class="card-image">
                        <a href="#">
                        <img class="img" src="assets/img/videosMax/imgVideo07.png" alt="Vídeo unbox Jota" title="Vídeo unbox Jota">
                        </a>
                        <div style="background-image: url(&quot;assets/img/videosMax/imgVideo07.png&quot;); opacity: 1;"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Unboxing Jota</h4>
                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalVideoJota">
                        <i class="material-icons">play_arrow</i>
                        ASSISTIR</button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-profile card-plain">
                    <div class="card-image">
                        <a href="#">
                        <img class="img" src="assets/img/videosMax/imgVideo08.png" alt="Vídeo unbox Casa Nerd" title="Vídeo unbox Casa Nerd">
                        </a>
                        <div style="background-image: url(&quot;assets/img/videosMax/imgVideo08.png&quot;); opacity: 1;"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Unboxing Casa Nerd </h4>
                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalVideoCasaNerd">
                        <i class="material-icons">play_arrow</i>
                        ASSISTIR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
