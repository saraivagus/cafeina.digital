<meta property="og:locale" content="pt_BR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="MAXRACER - Cadeira gamer" />
<meta property="og:description" content="MAXRACER CADEIRA GAMER, marca desenvolvida para GAMERS e para pessoas que querem cada vez mais conforto, mais design, mais qualidade e mais modernidade." />
<meta property="og:url" content="https://maxracer.com.br/" />
<meta property="og:site_name" content="MAXRACER" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="MAXRACER CADEIRA GAMER, marca desenvolvida para GAMERS e para pessoas que querem cada vez mais conforto, mais design, mais qualidade e mais modernidade." />
<meta name="twitter:title" content="MAXRACER - Cadeira gamer" />
