<!--  MODAL BCZZ -->
<div class="modal fade" id="modalVideoBczz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">UNBOXIN BCZZ - MAXRACER REGEN</h4>
            </div>
            <div class="modal-body">

                <iframe width="560" height="315" src="https://www.youtube.com/embed/EksapygEssE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">FECHAR</button>
            </div>
        </div>
    </div>
</div>
<!--  MODAL BCZZ -->
<!--  MODAL GORDOX -->
<div class="modal fade" id="modalVideoGordox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">UNBOXIN GORDOX - MAXRACER REGEN</h4>
            </div>
            <div class="modal-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/AC9a_2pVZdQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">FECHAR</button>
            </div>
        </div>
    </div>
</div>
<!--  MODAL GORDOX -->
<!--  MODAL JOTA -->
<div class="modal fade" id="modalVideoJota" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">UNBOXIN JOTA - MAXRACER AGGRESSIVE</h4>
            </div>
            <div class="modal-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/raUdPb_mINI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">FECHAR</button>
            </div>
        </div>
    </div>
</div>
<!--  MODAL JOTA -->
<!--  MODAL MaxMRM -->
<div class="modal fade" id="modalVideoMax" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">UNBOXIN MaxMRM - MAXRACER SKILLED</h4>
            </div>
            <div class="modal-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/tMLj9cX6HW0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">FECHAR</button>
            </div>
        </div>
    </div>
</div>
<!--  MODAL MaxMRM -->
<!--  MODAL Clodovil -->
<div class="modal fade" id="modalVideoClodo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">UNBOXIN Clodovil Do Cs - MAXRACER SKILLED</h4>
            </div>
            <div class="modal-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/TqwAob9yG6w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">FECHAR</button>
            </div>
        </div>
    </div>
</div>
<!--  MODAL Clodovil -->
<!--  MODAL TSPB -->
<div class="modal fade" id="modalVideoTSPB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">UNBOXIN TSPB - MAXRACER AGGRESSIVE</h4>
            </div>
            <div class="modal-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/9bp69Z00PyA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">FECHAR</button>
            </div>
        </div>
    </div>
</div>
<!--  MODAL TSPB -->
<!--  MODAL estagiário -->
<div class="modal fade" id="modalVideoEstag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">UNBOXIN MaxMRM - MAXRACER SKILLED</h4>
            </div>
            <div class="modal-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/jJlfHC0Xq5k" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">FECHAR</button>
            </div>
        </div>
    </div>
</div>
<!--  MODAL estagiário -->
<!--  MODAL Casa Nerd Gamer -->
<div class="modal fade" id="modalVideoCasaNerd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">UNBOXIN Casa Nerd Gamer - MAXRACER SKILLED</h4>
            </div>
            <div class="modal-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/8o6Kodjdbrg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">FECHAR</button>
            </div>
        </div>
    </div>
</div>
<!--  MODAL Casa Nerd Gamer -->
