<div class="container">
    <div class="features-1">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="title">MISSÃO, VISÃO E VALORES</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 corpoBgMissao">
                <div class="info info-horizontal">
                    <div class="description">
                        <h4 class="info-title" >Missão</h4>
                        <p style="text-align:justify">A <span class="boldTexto">MAXRACER</span> tem a missão de ser <span class="boldTexto">INOVADORA</span> no ramo de cadeiras <span class="boldTexto">gamers</span> e ser a marca mais respeitada e lucrativa do mercado nacional até 2022.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 corpoBgMissao">
                <div class="info info-horizontal">
                    <div class="description">
                        <h4 class="info-title">Visão</h4>
                        <p style="text-align:justify"><span class="boldTexto">MAXRACER</span> tem a visão de ser a marca mais visionaria e moderna do mercado nacional.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 corpoBgMissao">
                <div class="info info-horizontal">
                    <div class="description">
                        <h4 class="info-title">Valores</h4>
                        <p style="text-align:justify">A <span class="boldTexto">MAXRACER</span> tem muitos valores e destaca-se o <span class="boldTexto">respeito</span> para com todos os clientes e fornecedores. A <span class="boldTexto">generosidade</span> repartindo parte do seu lucro com instituições e com projetos sociais. A <span class="boldTexto">coragem</span> para enfrentar os obstáculos e oferecer cada vez mais o melhor a todos.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
