<div class="features-5 hidden-xs" style="background-image: url('assets/img/bg9.jpg')">
    <div class="col-md-8 col-md-offset-2 text-center">
        <h2 class=" colorBlackTitulo">A MAX SE PREOCUPA COM A SAÚDE<br>
            DE SEUS CONSUMIDORES
        </h2>
        <h5 class="description colorBlack">Selecione abaixo o tipo do seu corpo para que possamos lhe indicar a melhor cadeira!</h5>
    </div>
    <div class="row text-center" style="padding-bottom:40px">
        <div class="container">
            <div class="col-md-12 toolbar mb2 mt2">
                <button class="btn fil-cat" href="" data-rel="all" style="color:#000">Todas</button>
                <button class="btn fil-cat" data-rel="corpoId1"><img src="assets/img/imgTipoCorpo1.png" alt="MAXRACER - TIPO DE CORPO 1" title="MAXRACER - TIPO DE CORPO 1" width="80px"></button>
                <button class="btn fil-cat" data-rel="corpoId2"><img src="assets/img/imgTipoCorpo2.png" alt="MAXRACER - TIPO DE CORPO 2" title="MAXRACER - TIPO DE CORPO 2" width="80px"></button>
                <button class="btn fil-cat" data-rel="corpoId3"><img src="assets/img/imgTipoCorpo3.png" alt="MAXRACER - TIPO DE CORPO 3" title="MAXRACER - TIPO DE CORPO 3" width="80px"></button>
                <button class="btn fil-cat" data-rel="corpoId4"><img src="assets/img/imgTipoCorpo4.png" alt="MAXRACER - TIPO DE CORPO 4" title="MAXRACER - TIPO DE CORPO 4" width="80px"></button>
            </div>
            <div style="clear:both;"></div>
            <div id="portfolio">
                <div class="tile scale-anm corpoId1 all ">
                    <a href="#"><img src="assets/img/produtosFiltro/imgProdutosFiltroAggressive.png" alt="MAXRACER FILTRO AGGRESSIVE " title="MAXRACER FILTRO AGGRESSIVE "></a>
                    <a href="produtosAggressive" class="btn btn-rose btn-sm">
                    <i class="material-icons">search</i>
                    ESPECIFICAÇÕES</a>
                </div>
                <div class="tile scale-anm corpoId2 all ">
                    <img src="assets/img/produtosFiltro/imgProdutosFiltroSkilled.png" alt="MAXRACER FILTRO SKILLED " title="MAXRACER FILTRO SKILLED ">
                    <a href="produtosSkilled" class="btn btn-rose btn-sm">
                    <i class="material-icons">search</i>
                    ESPECIFICAÇÕES</a>
                </div>
                <div class="tile scale-anm corpoId3 all ">
                    <img src="assets/img/produtosFiltro/imgProdutosFiltroPixel.png" alt="MAXRACER FILTRO PIXEL " title="MAXRACER FILTRO PIXEL ">
                    <a href="produtosPixel" class="btn btn-rose btn-sm">
                    <i class="material-icons">search</i>
                    ESPECIFICAÇÕES</a>
                </div>
                <div class="tile scale-anm corpoId4 all ">
                    <img src="assets/img/produtosFiltro/imgProdutosFiltroTactical.png" alt="MAXRACER FILTRO TACTICAL " title="MAXRACER FILTRO TACTICAL ">
                    <a href="produtosTactical" class="btn btn-rose btn-sm">
                    <i class="material-icons">search</i>
                    ESPECIFICAÇÕES</a>
                </div>
                <div class="tile scale-anm corpoId4 all ">
                    <img src="assets/img/produtosFiltro/imgProdutosFiltroRegen.png" alt="MAXRACER FILTRO REGEN " title="MAXRACER FILTRO REGEN ">
                    <a href="produtosRegen" class="btn btn-rose btn-sm">
                    <i class="material-icons">search</i>
                    ESPECIFICAÇÕES</a>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
</div>
