<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
<link rel="icon" type="image/png" href="assets/img/favicon.png">
<meta name="description" content="MAXRACER CADEIRA GAMER, marca desenvolvida para GAMERS e para pessoas que querem cada vez mais conforto, mais design, mais qualidade e mais modernidade."/>
<link rel="canonical" href="https://maxracer.com.br/" />
<meta property="og:locale" content="pt_BR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="MAXRACER - Cadeira gamer" />
<meta property="og:description" content="MAXRACER CADEIRA GAMER, marca desenvolvida para GAMERS e para pessoas que querem cada vez mais conforto, mais design, mais qualidade e mais modernidade." />
<meta property="og:url" content="https://maxracer.com.br/" />
<meta property="og:site_name" content="MAXRACER" />
<meta property="og:image"  content="http://maxracer.com.br/maxracer.png" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="MAXRACER CADEIRA GAMER, marca desenvolvida para GAMERS e para pessoas que querem cada vez mais conforto, mais design, mais qualidade e mais modernidade." />
<meta name="twitter:title" content="MAXRACER - Cadeira gamer" />

<title>MAXRACER - PREMIUM GAMING CHAIR</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<?php include("includes/styles.php"); ?>
<?php include("includes/tracking.php"); ?>