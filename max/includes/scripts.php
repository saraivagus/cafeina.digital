<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js"></script>
<script src="assets/js/nouislider.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap-tagsinput.js"></script>
<script src="assets/js/sweet-scroll.js" type="text/javascript"></script>
<script src="assets/js/material-kit.js?v=1.1.0" type="text/javascript"></script>
<script src="assets/js/pre.js" type="text/javascript"></script>
