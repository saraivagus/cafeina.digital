<!doctype html>
<html lang="en">
<head>
    <?php include("includes/head.php"); ?>
</head>
<?php include("includes/preloader.php"); ?>
<body>
        <div class="header-1">
              <?php include("includes/navbar.php"); ?>
            <?php include("pages/pixel/header.php"); ?>
        </div>
        <div class="wrapper">
            <div class="header">
            </div>
            <!-- you can use the class main-raised if you want the main area to be as a page with shadows -->
            <ul class="list-inline text-center" style="padding-top:30px">
            <li data-toggle="modal" data-target="#modalPixel1"><a href="#myGallery" data-slide-to="0">
                    <img class="img-thumbnail" src="assets/img/pgProduto/pixel/produtos/modeloPixel180.png" alt="CADEIRA MAXRACER PIXEL DEITADA 180 GRAUS" title="MAXRACER PIXEL"><br>
            </a></li>
            <li data-toggle="modal" data-target="#modalPixel2"><a href="#myGallery" data-slide-to="1">
                    <img class="img-thumbnail" src="assets/img/pgProduto/pixel/produtos/modeloPixelDiagonal.png" alt="CADEIRA MAXRACER PIXEL DETALHE DIAGONAL" title="MAXRACER PIXEL"><br>
            </a></li>
            <li data-toggle="modal" data-target="#modalPixel3"><a href="#myGallery" data-slide-to="2">
                    <img class="img-thumbnail" src="assets/img/pgProduto/pixel/produtos/modeloPixelFrente.png" alt="CADEIRA MAXRACER PIXEL DETALHE FRENTE" title="MAXRACER PIXEL"><br>
            </a></li>
            <li data-toggle="modal" data-target="#modalPixel4"><a href="#myGallery" data-slide-to="3">
                    <img class="img-thumbnail" src="assets/img/pgProduto/pixel/produtos/modeloPixelLateral.png" alt="CADEIRA MAXRACER PIXEL DETALHE LATERAL" title="MAXRACER PIXEL"><br>
            </a></li>
            <!--end of thumbnails-->
            </ul>
            <?php include("pages/pixel/medidas.php"); ?>


        </div>
        <?php include("includes/footer.php"); ?>
        <?php include("pages/pixel/modal/modalPixel.php"); ?>

    </body>
<?php include("includes/scripts.php"); ?>
</html>
