<!doctype html>
<html lang="en">
<head>
    <?php include("includes/head.php"); ?>
</head>
<?php include("includes/preloader.php"); ?>
<body>
<div class="header-1">
          <?php include("includes/navbar.php"); ?>

            <?php include("pages/podium/header.php"); ?>
        </div>
        <div class="wrapper">
            <div class="header">
            </div>
            <!-- you can use the class main-raised if you want the main area to be as a page with shadows -->

            <div class="main">
                <div class="container">
                </div>
                <?php include("pages/podium/medidas.php"); ?>
            </div>


        </div>
        <?php include("includes/footer.php"); ?>
    </body>
<?php include("includes/scripts.php"); ?>
</html>
