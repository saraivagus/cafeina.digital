


<div class="modal fade" id="modalSkilled1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">MAXRACER SKILLED</h4>
			</div>
			<div class="modal-body">
				<p><img class="img-thumbnail" src="assets/img/pgProduto/skilled/produtos/Skilled-Branca-diagonal.png">
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modalSkilled2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">MAXRACER SKILLED</h4>
			</div>
			<div class="modal-body">
				<p><img class="img-thumbnail" src="assets/img/pgProduto/skilled/produtos/branca-detalhe-costas-com-almofadas-1.png">
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalSkilled3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">MAXRACER SKILLED</h4>
			</div>
			<div class="modal-body">
				<p><img class="img-thumbnail" src="assets/img/pgProduto/skilled/produtos/modeloSkilledDetalhe01.png">
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalSkilled4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">MAXRACER SKILLED</h4>
			</div>
			<div class="modal-body">
				<p><img class="img-thumbnail" src="assets/img/pgProduto/skilled/produtos/modeloSkilledDetalhe02.png">
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>
