<div class="page-headerProdutos" style="background-image: url('assets/img/pgProduto/pixel/imgPixelBg.png');">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center">
              <img src="assets/img/pgProduto/pixel/imgLogoPixelBlack.png" style="padding-top:120px" class="img-responsive hidden-sm hidden-md hidden-lg">
              <img src="assets/img/pgProduto/pixel/imgPixelFull.png" id="imgPixel" style="padding-top:150px" class="img-responsive hidden-xs">
              <img src="assets/img/pgProduto/pixel/imgPixelFull.png" id="imgPixel2" style="padding-top:20px" class="img-responsive hidden-sm hidden-md hidden-lg">
            </div>
            <div class="col-md-6 text-center">
                <img src="assets/img/pgProduto/pixel/imgLogoPixel.png" style="padding-top:180px" class="img-responsive hidden-xs">
                <h5 class="description" style="padding-top:80px; text-align: justify">Essa​ ​MAXRACER​ ​tem​ ​um​ ​design​ ​mais​ ​forte/colorido,​ ​para​ ​jogadores​ ​e​ ​usuários​ ​em​ ​geral​ ​mais agressivos.​ ​Cadeira​ ​confortável​ ​e​ ​mais​ ​firme​ ​no​ ​assento.
                    <br><br>
                    <div class="hidden-xs">
                    Cores Disponíveis:
                    <a href="#" onclick="document.getElementById('imgPixel').src='assets/img/pgProduto/pixel/imgPixelFullPretoVermelho.png'"><img src="assets/img/pgProduto/pixel/imgCoresPretoVermelho.png"></a>
                    <a href="#" onclick="document.getElementById('imgPixel').src='assets/img/pgProduto/pixel/imgPixelFull.png'"><img src="assets/img/pgProduto/pixel/imgCoresVermelhoPreto.png"></a>
                  </div>
                  <div class="hidden-sm hidden-md hidden-lg">
                  Cores Disponíveis:
                  <a href="#" onclick="document.getElementById('imgPixel2').src='assets/img/pgProduto/pixel/imgPixelFullPretoVermelho.png'"><img src="assets/img/pgProduto/pixel/imgCoresPretoVermelho.png"></a>
                  <a href="#" onclick="document.getElementById('imgPixel2').src='assets/img/pgProduto/pixel/imgPixelFull.png'"><img src="assets/img/pgProduto/pixel/imgCoresVermelhoPreto.png"></a>
                </div>
                </h5>
                <div class="dropdown text-left">
	<a href="#" class="btn dropdown-toggle btn-success" data-toggle="dropdown">
<i class="material-icons">add_shopping_cart</i>
    	COMPRAR AGORA
    	<b class="caret"></b>
	</a>
	<ul class="dropdown-menu">
    <li><a href="#">Cores</a></li>
    <li class="divider"></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-pixel-vermelho/">Vermelha e Preto</a></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-pixel-preto/">Preto e Vermelha</a></li>

	</ul>
</div>
                <div class="panel-group text-left" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h4 class="panel-title ">
                                    Espuma
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                Mold​ ​Form​ ​Injetada​ ​-​ ​Densidade​ ​50Kg/m³​ ​-​ ​Baixíssima​ ​deformação.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <h4 class="panel-title">
                                    Estrutura​ ​Interna
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                Aço​ ​interligado​ ​por​ ​solda​ ​industrial​ ​MIG.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <a c role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <h4 class="panel-title ">
                                    Revestimento
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                Sintético​ ​PVC Acabamento:​ ​Costura​ ​laterais​ ​tripla
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <h4 class="panel-title ">
                                    Braços
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                                 ​Três​ ​regulagens​ ​(apoio​ ​para​ ​frente​ ​e​ ​para​ ​traz​ ​-​ ​apoio​ ​rotacional​ ​-​ ​braço​ ​sobe​ ​e​ ​desce). Braço:​ ​Apoio​ ​em​ ​PU​ ​macio. Coluna​ ​a​ ​gás:​ ​Classe​ ​4​ ​-​ ​24h.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <h4 class="panel-title ">
                                    Base
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body">
                              Aço​ ​curvado,​ ​pintura​ ​eletrostática​ ​epóxi-pó
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <h4 class="panel-title ">
                                  Rodas
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                            <div class="panel-body">
                                100%Nylon​ ​-​ ​Duplo​ ​giro​ ​-​ ​Da​ ​cor​ ​da​ ​cadeira
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <h4 class="panel-title ">
                                    Almofada
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                            <div class="panel-body">
                            ​Lombar​ ​​ ​-​ ​Espuma​ ​D28Kg/m²​ ​-​ ​Viscoelásticas​ ​-​ ​Com​ ​logo<br>
                            Cervical​ ​-​ ​​ ​Plumante​ ​super​ ​macio​ ​-​ ​Com​ ​logo
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingNine">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <h4 class="panel-title ">
                                  Recursos
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                            <div class="panel-body">
                              Regulagem​ ​de​ ​inclinação​ ​do​ ​encosto​ ​até​ ​180º. <br>
                              Regulagem​ ​da​ ​altura​ ​do​ ​assento,​ ​10cm​ ​de​ ​variação.<br>
                              Regulagem​ ​do​ ​mecanismo​ ​relax​ ​(mesma​ ​alavanca​ ​do​ ​pistão),​ ​inclinação​ ​12º.<br>
                              Regulagem​ ​do​ ​braço:​ ​Sobe/desce​ ​-​ ​deslize​ ​frontal​ ​-​ ​rotação.<br>


                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTen">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <h4 class="panel-title ">
                                    Limite de Peso
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                            <div class="panel-body">
                            130 KG
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
