<div class="page-headerProdutos" style="background-image: url('assets/img/pgProduto/aggressive/imgAggressiveBg.png');">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center">
              <img src="assets/img/pgProduto/aggressive/imgLogoAggressive.png" style="padding-top:80px" class="img-responsive hidden-sm hidden-md hidden-lg">
              <img src="assets/img/pgProduto/aggressive/imgAggressiveFull.png" id="imgAggressive" style="padding-top:150px" class="img-responsive hidden-xs">
              <img src="assets/img/pgProduto/aggressive/imgAggressiveFull.png" id="imgAggressive2" style="padding-top:20px" class="img-responsive hidden-sm hidden-md hidden-lg">
            </div>
            <div class="col-md-6 text-center">
                <img src="assets/img/pgProduto/aggressive/imgLogoAggressive.png" style="padding-top:180px" class="img-responsive hidden-xs">
                <h5 class="description" style="padding-top:80px; text-align: justify">Projetado por brasileiros apaixonados por gamers e quem entendem suas necessidades!

                    <br><br>
                    <div class="hidden-xs">
                    Cores Disponíveis:
                    <a href="#" onclick="document.getElementById('imgAggressive').src='assets/img/pgProduto/aggressive/imgAggressiveFull.png'"><img src="assets/img/pgProduto/aggressive/imgCoresPretoBranco.png"></a>
                    <a href="#" onclick="document.getElementById('imgAggressive').src='assets/img/pgProduto/aggressive/imgAggressiveFullVermelho.png'"><img src="assets/img/pgProduto/aggressive/imgCoresPretoVermelho.png"></a>
                    <a href="#" onclick="document.getElementById('imgAggressive').src='assets/img/pgProduto/aggressive/imgAggressiveFullLaranja.png'"><img src="assets/img/pgProduto/aggressive/imgCoresPretoLaranja.png"></a>
                    <a href="#" onclick="document.getElementById('imgAggressive').src='assets/img/pgProduto/aggressive/imgAggressiveFullAzul.png'"><img src="assets/img/pgProduto/aggressive/imgCoresPretoAzul.png"></a>
                  </div>
                  <div class="hidden-sm hidden-md hidden-lg">
                    Cores Disponíveis:
                    <a href="#" onclick="document.getElementById('imgAggressive2').src='assets/img/pgProduto/aggressive/imgAggressiveFull.png'"><img src="assets/img/pgProduto/aggressive/imgCoresPretoBranco.png"></a>
                    <a href="#" onclick="document.getElementById('imgAggressive2').src='assets/img/pgProduto/aggressive/imgAggressiveFullVermelho.png'"><img src="assets/img/pgProduto/aggressive/imgCoresPretoVermelho.png"></a>
                    <a href="#" onclick="document.getElementById('imgAggressive2').src='assets/img/pgProduto/aggressive/imgAggressiveFullLaranja.png'"><img src="assets/img/pgProduto/aggressive/imgCoresPretoLaranja.png"></a>
                    <a href="#" onclick="document.getElementById('imgAggressive2').src='assets/img/pgProduto/aggressive/imgAggressiveFullAzul.png'"><img src="assets/img/pgProduto/aggressive/imgCoresPretoAzul.png"></a>
                  </div>
                </h5>
                <div class="dropdown text-left">
	<a href="#" class="btn dropdown-toggle btn-success" data-toggle="dropdown">
<i class="material-icons">add_shopping_cart</i>
    	COMPRAR AGORA
    	<b class="caret"></b>
	</a>
	<ul class="dropdown-menu">
    <li><a href="#">Cores</a></li>
    <li class="divider"></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-aggressive-pretobranco/">Branca</a></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-aggressive-pretoazul/">Azul</a></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-aggressive-pretolaranja/">Laranja</a></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-aggressive-pretovermelho/">Vermelha</a></li>
	</ul>
</div>
                <div class="panel-group text-left" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h4 class="panel-title ">
                                    Assento e Encosto
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                Confeccionados em espuma Injetada MOLD FORM MACIA (não deforma)
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <h4 class="panel-title">
                                    Revestimento
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                              SINTÉTICO de ALTA PERFORMANCE (anti ressecamento)
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <a c role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <h4 class="panel-title ">
                                    Acabamento
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                              Costura REBATIDA ITALIANA
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <h4 class="panel-title ">
                                    Braços
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                              Datados com regulagem de altura e Apoio de braço em P.U macio
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <h4 class="panel-title ">
                                  Apoio de lombar e cervical
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body">
                                Espuma MACIA D33
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <h4 class="panel-title ">
                                    Ajustes
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                            <div class="panel-body">
                              Altura do assento, altura dos braços e Inclinação do encosto até 180º
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <h4 class="panel-title ">
                                  Gás Lifth
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                            <div class="panel-body">
                              Class 4 – Suporta até 150Kg
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEight">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <h4 class="panel-title ">
                                  Base e Rodas
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                            <div class="panel-body">
                                Base: Confeccionado em NYLON, acabamento em capa PP com ponta colorida da cor da cadeira<br>
                                Rodas: Sistema de DUPLO GIRO, 100% nylon, rodas da cor da cadeira
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingNine">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <h4 class="panel-title ">
                                    Limite de Peso
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                            <div class="panel-body">
                            150 KG
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
