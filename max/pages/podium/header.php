<div class="page-headerProdutos" style="background-image: url('assets/img/pgProduto/tactical/imgTacticalBg.png');">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center">
              <img src="assets/img/pgProduto/podium/imgLogoPodiumBlack.png" style="padding-top:180px" class="img-responsive hidden-sm hidden-md hidden-lg">
              <img src="assets/img/pgProduto/podium/imgPodiumFull.png" id="imgPodium" style="padding-top:150px" class="img-responsive hidden-xs">

                <img src="assets/img/pgProduto/podium/imgPodiumFull.png" id="imgPodium" style="padding-top:20px" class="img-responsive hidden-sm hidden-md hidden-lg">
            </div>
            <div class="col-md-6 text-center">
                <img src="assets/img/pgProduto/podium/imgLogoPodium.png" style="padding-top:60px" class="img-responsive hidden-xs">
                <h5 class="description" style="padding-top:80px; text-align: justify">Projetado por brasileiros apaixonados por gamers e quem entendem suas necessidades! Um modelo básico e moderno que atende as necessidade do GAMER!
                    <br><br>
                    Cores Disponíveis:
                    <a href="#" onclick="document.getElementById('imgPodium').src='assets/img/pgProduto/podium/imgPodiumFullBranco.png'"><img src="assets/img/pgProduto/tactical/imgCoresPretoBranco.png"></a>
                    <a href="#" onclick="document.getElementById('imgPodium').src='assets/img/pgProduto/podium/imgPodiumFullVermelho.png'"><img src="assets/img/pgProduto/tactical/imgCoresPretoVermelho.png"></a>
                    <a href="#" onclick="document.getElementById('imgPodium').src='assets/img/pgProduto/podium/imgPodiumFull.png'"><img src="assets/img/pgProduto/tactical/imgCoresPretoAzul.png"></a>
                </h5>
                <div class="dropdown text-left">
	<a href="#" class="btn dropdown-toggle btn-success" data-toggle="dropdown">
<i class="material-icons">add_shopping_cart</i>
    	COMPRAR AGORA
    	<b class="caret"></b>
	</a>
	<ul class="dropdown-menu">
    <li><a href="#">Cores</a></li>
    <li class="divider"></li>
		<li><a href="http://maxracer.com.br/loja/loja/mesa-gamer-maxracer-podium-branca/">Branca</a></li>
		<li><a href="http://maxracer.com.br/loja/loja/mesa-gamer-maxracer-podium-azul/">Azul</a></li>
		<li><a href="http://maxracer.com.br/loja/loja/mesa-gamer-maxracer-podium/">Vermelha</a></li>
	</ul>
</div>
                <div class="panel-group text-left" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h4 class="panel-title ">
                                    Características
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                              Mesa com design arrojado e moderno.<br>
  Acabamento com ponta caída para maior conforto.<br>
  Tampo revestido em PVC na cor preta.<br>
  Estrutura em aço com alta taxa de carbono, RESISTENTE.<br>
  Soldagem robótica, 100% de padronização.<br>
  Pintura: Eletrostática epóxi-pó.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <h4 class="panel-title">
                                    Recursos
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                               ​Suporte para copos.<br>
Suporte para headset .
                            </div>
                        </div>
                    </div>






                </div>
            </div>
        </div>
    </div>
</div>
