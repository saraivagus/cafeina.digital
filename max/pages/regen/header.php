<div class="page-headerProdutos" style="background-image: url('assets/img/pgProduto/regen/imgRegenlBg.png');">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center">
              <img src="assets/img/pgProduto/regen/imgLogoRegenBlack.png" style="padding-top:150px" class="img-responsive hidden-sm hidden-md hidden-lg">
              <img src="assets/img/pgProduto/regen/imgRegenFull.png" id="imgRegen" style="padding-top:150px" class="img-responsive hidden-xs">
                <img src="assets/img/pgProduto/regen/imgRegenFull.png" id="imgRegen2" style="padding-top:20px" class="img-responsive hidden-sm hidden-md hidden-lg">
            </div>
            <div class="col-md-6 text-center">
                <img src="assets/img/pgProduto/regen/imgLogoRegen.png" style="padding-top:180px" class="img-responsive hidden-xs">
                <h5 class="description hidden-sm hidden-md hidden-lg" style="padding-top:20px; text-align: justify">
                  Primeira cadeira GAMER DO MUNDO COM SMI (Sistema de Massagem Integrado).
                  Projetado por brasileiros apaixonados por gamers e quem entendem suas
                  necessidades!
                    <br><br>
                    Cores Disponíveis:
                    <a href="#" onclick="document.getElementById('imgRegen2').src='assets/img/pgProduto/regen/imgRegenFull.png'"><img src="assets/img/pgProduto/regen/imgCoresPretoBranco.png"></a>
                    <a href="#" onclick="document.getElementById('imgRegen2').src='assets/img/pgProduto/regen/imgRegenFullVermelho.png'"><img src="assets/img/pgProduto/regen/imgCoresPretoVermelho.png"></a>
                    <a href="#" onclick="document.getElementById('imgRegen2').src='assets/img/pgProduto/regen/imgRegenFullVerde.png'"><img src="assets/img/pgProduto/regen/imgCoresPretoVerde.png"></a>
                    <a href="#" onclick="document.getElementById('imgRegen2').src='assets/img/pgProduto/regen/imgRegenFullAzul.png'"><img src="assets/img/pgProduto/regen/imgCoresPretoAzul.png"></a>
                </h5>
                <h5 class="description hidden-xs" style="padding-top:180px; text-align: justify">
                  Primeira cadeira GAMER DO MUNDO COM SMI (Sistema de Massagem Integrado).
                  Projetado por brasileiros apaixonados por gamers e quem entendem suas
                  necessidades!
                    <br><br>
                    Cores Disponíveis:
                    <a href="#" onclick="document.getElementById('imgRegen').src='assets/img/pgProduto/regen/imgRegenFull.png'"><img src="assets/img/pgProduto/regen/imgCoresPretoBranco.png"></a>
                    <a href="#" onclick="document.getElementById('imgRegen').src='assets/img/pgProduto/regen/imgRegenFullVermelho.png'"><img src="assets/img/pgProduto/regen/imgCoresPretoVermelho.png"></a>
                    <a href="#" onclick="document.getElementById('imgRegen').src='assets/img/pgProduto/regen/imgRegenFullVerde.png'"><img src="assets/img/pgProduto/regen/imgCoresPretoVerde.png"></a>
                    <a href="#" onclick="document.getElementById('imgRegen').src='assets/img/pgProduto/regen/imgRegenFullAzul.png'"><img src="assets/img/pgProduto/regen/imgCoresPretoAzul.png"></a>
                </h5>
                <div class="dropdown text-left">
	<a href="#" class="btn dropdown-toggle btn-success" data-toggle="dropdown">
<i class="material-icons">add_shopping_cart</i>
    	COMPRAR AGORA
    	<b class="caret"></b>
	</a>
	<ul class="dropdown-menu">
    <li><a href="#">Cores</a></li>
    <li class="divider"></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-regen-branca/">Branca</a></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-regen-azul/">Azul</a></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-regen-verde/">Verde</a></li>
		<li><a href="http://maxracer.com.br/loja/loja/cadeira-gamer-max-racer-regen-vermelha/">Vermelha</a></li>
	</ul>
</div>

                <div class="panel-group text-left" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h4 class="panel-title ">
                                    Assento e Encosto
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                Encosto com quatro pontos de MASSAGEM tido vibroterapia, controlado por controle remoto.<br>
                                Assento com dois pontos de MASSAGEM tido vibroterapia, controlado por controle remoto.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <h4 class="panel-title">
                                    Revestimento
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                              Sintético P.U + Acabamento em sintético CARBON.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <a c role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <h4 class="panel-title ">
                                    Acabamento
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                Costura laterais tripla (super reforçada) italiana.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <h4 class="panel-title ">
                                    Braços
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                              Quatro regulagens.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <h4 class="panel-title ">
                                    Espuma e Gás Lift
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body">
                              Espuma:  Mold Form Injetada – Densidade 50Kg/m³ – Baixíssima deformação. <br>
                              Gás Lift: Classe 4 – 24h.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <h4 class="panel-title ">
                                    Recursos
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                            <div class="panel-body">
                              Regulagem de inclinação do encosto até 180º.<br>
                              Regulagem da altura do assento, 10cm de variação.<br>
                              Regulagem do mecanismo relax (mesma alavanca do pistão), inclinação 12º.<br>
                              Regulagem do braço: Sobe/desce – deslize frontal – deslize lateral – rotação
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <h4 class="panel-title ">
                                    Base e Rodas
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                            <div class="panel-body">
                                Base: Nylon <br>
                                Rodas: 100% Nylon – Duplo giro.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEight">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <h4 class="panel-title ">
                                    SMI​ ​- Sistema Integrado de Massagem
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                            <div class="panel-body">
                              Regulagem do sistema de massagem: On/Off, pontos de massagem, intensidade da vibração, através de controle.<br>
                              Voltagem: Bivolt.<br>
                              Tomada: Padrão ABNT.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingNine">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <h4 class="panel-title ">
                                    Limite de Peso
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                            </a>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                            <div class="panel-body">
                            130 KG
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
