


<div class="modal fade" id="modalTactical1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">MAXRACER TACTICAL</h4>
			</div>
			<div class="modal-body">
				<p><img class="img-thumbnail" src="assets/img/pgProduto/tactical/produtos/cadeira-maxracer-tactical-branca-lateral-deitada.jpg">
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modalTactical2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">MAXRACER TACTICAL</h4>
			</div>
			<div class="modal-body">
				<p><img class="img-thumbnail" src="assets/img/pgProduto/tactical/produtos/cadeira-maxracer-tactical-branca-frente-sem-almofada.jpg">
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalTactical3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">MAXRACER TACTICAL</h4>
			</div>
			<div class="modal-body">
				<p><img class="img-thumbnail" src="assets/img/pgProduto/tactical/produtos/cadeira-maxracer-tactical-branca-frente-com-almofada.jpg">
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalTactical4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">MAXRACER TACTICAL</h4>
			</div>
			<div class="modal-body">
				<p><img class="img-thumbnail" src="assets/img/pgProduto/tactical/produtos/cadeira-maxracer-tactical-branca-lateral.jpg">
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>
