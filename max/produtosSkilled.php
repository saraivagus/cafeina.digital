<!doctype html>
<html lang="en">
<head>
    <?php include("includes/head.php"); ?>
</head>
<?php include("includes/preloader.php"); ?>
<body>
        <div class="header-1">
          <?php include("includes/navbar.php"); ?>
          <?php include("pages/skilled/header.php"); ?>

        </div>
        <div class="wrapper">
            <div class="header">
            </div>
            <!-- you can use the class main-raised if you want the main area to be as a page with shadows -->
            <div class="main">
                <div class="container">
                  <ul class="list-inline text-center" style="padding-top:30px">
                  <li data-toggle="modal" data-target="#modalSkilled1"><a href="#myGallery" data-slide-to="0">
                          <img class="img-thumbnail" src="assets/img/pgProduto/skilled/produtos/modeloSkilledLateral.png" alt="CADEIRA MAXRACER SKILLED DETALHE LATERAL" title="MAXRACER SKILLED"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalSkilled2"><a href="#myGallery" data-slide-to="1">
                          <img class="img-thumbnail" src="assets/img/pgProduto/skilled/produtos/modeloSkilledCostas.png" alt="CADEIRA MAXRACER SKILLED DETALHE COSTAS" title="MAXRACER SKILLED"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalSkilled3"><a href="#myGallery" data-slide-to="2">
                          <img class="img-thumbnail" src="assets/img/pgProduto/skilled/produtos/modeloSkilledDetalhe01.png" alt="CADEIRA MAXRACER SKILLED DETALHE RODINHA" title="MAXRACER SKILLED"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalSkilled4"><a href="#myGallery" data-slide-to="3">
                          <img class="img-thumbnail" src="assets/img/pgProduto/skilled/produtos/modeloSkilledDetalhe02.png" alt="CADEIRA MAXRACER SKILLED DETALHE APOIO DE BRAÇO" title="MAXRACER SKILLED"><br>
                </a></li>
                <!--end of thumbnails-->
                </ul>
                </div>
                <?php include("pages/skilled/medidas.php"); ?>
            </div>
        </div>
        <?php include("includes/footer.php"); ?>
        <?php include("pages/skilled/modal/modalSkilled.php"); ?>
    </body>
<?php include("includes/scripts.php"); ?>
</html>
