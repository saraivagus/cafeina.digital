<!doctype html>
<html lang="en">
    <head>
        <?php include("includes/head.php"); ?>
    </head>
    <?php include("includes/preloader.php"); ?>
    <body>
        <div class="header-1">
              <?php include("includes/navbar.php"); ?>
              <?php include("pages/aggressive/header.php"); ?>
        </div>
        <div class="wrapper">
            <div class="header">
            </div>
            <div class="main">
                <div class="container">
                  <ul class="list-inline text-center" style="padding-top:30px">
                  <li data-toggle="modal" data-target="#modalAgg1"><a href="#myGallery" data-slide-to="0">
                          <img class="img-thumbnail" src="assets/img/pgProduto/aggressive/produtos/modeloAggressiveFrente.png" alt="Cadeira MAXRACER Aggressive Frente" title="MAXRACER AGRESSIVE"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalAgg2"><a href="#myGallery" data-slide-to="1">
                          <img class="img-thumbnail" src="assets/img/pgProduto/aggressive/produtos/modeloAggressiveCostas.png" alt="Cadeira MAXRACER Aggressive Costas" title="MAXRACER AGRESSIVE"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalAgg3"><a href="#myGallery" data-slide-to="2">
                          <img class="img-thumbnail" src="assets/img/pgProduto/aggressive/produtos/modeloAggressiveDetalhe01.png" alt="Cadeira MAXRACER Aggressive Detalhe Apoio Lateral" title="MAXRACER AGRESSIVE"><br>
                </a></li>
                  <li data-toggle="modal" data-target="#modalAgg4"><a href="#myGallery" data-slide-to="3">
                          <img class="img-thumbnail" src="assets/img/pgProduto/aggressive/produtos/modeloAggressiveDetalhe02.png" alt="Cadeira MAXRACER Aggressive Detalhe Apoio de Braço" title="MAXRACER AGRESSIVE"><br>
                </a></li>
                <!--end of thumbnails-->
                </ul>
                </div>
                <?php include("pages/aggressive/medidas.php"); ?>
            </div>
        </div>
        <?php include("includes/footer.php"); ?>
    </body>
      <?php include("pages/aggressive/modal/modalAgg.php"); ?>
<?php include("includes/scripts.php"); ?>
</html>
