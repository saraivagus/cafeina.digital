<?php
    include("sistema/conexao/conectax.php");

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
            GSARAIVA - DESIGN & MARKETING DIGITAL
        </title>

        <meta property="og:type" content="website">
        <meta property="og:locale" content="pt_BR">
        <meta property="og:description" content="Assessoria de comunicação e marketing para micro, pequenas e médias empresas. Agência de publicidade e propaganda em Campinas, visando atender sua empresa com eficiência, melhor prazo e custo-benefício.">
        <meta property="og:title" content="GSARAIVA DESIGN E MARKETING DIGITAL">
        <meta property="og:url" content="http://www.gsaraiva.com">
        <meta property="og:site_name" content=" Agência de Publicidade e Propaganda em Campinas | Agência especializada em micro, pequenas e médias empresas. Criação de identidade visual, criação de websites, desenvolvimento de site, posicionamento e gestão de marcas, através das ferramentas do marketing, da comunicação, do monitoramento e gestão de mídias sociais, da propaganda, design gráfico, adwords, e do marketing digital gerando valor agregado e lucro para sua empresa. Uma agência de publicidade e propaganda em Campinas. Transformamos ideias em publicidade. Atendemos, Campinas, interior de são paulo, Sousas, Joaquim edigio, barão geraldo, hortolandia, Indaiatuba, Salto, lavras, minas gerais, poços de caldas, Sorocaba, Americana, sumare, mogi mirin, mogi guacu Jundiaí, Valinhos, Vinhedo, Itatiba, Itu, Sorocaba, Cosmópolis, Itupeva, Atibaia, Paulinia, Holambra, São Paulo | Oferecemos ao cliente Planejamento de marketing, de Midia, Relacionamento, desenvolvimento de websites, desenvolvimento de site, e-commerce, monitoramento de facebook, Endomarketing, Promocao, Feiras,  Exposicoes, Merchandising, Ponto-de-Venda, Design Grafico, Web Digital, Identidade Visual, Impressão, E-mail Marketing, Monitoramento de Midias Sociais">
        <meta content="height=device-height, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
        <!-- style start -->
        <link href="css/plugins.css" media="all" rel="stylesheet" type="text/css">
        <link href="css/style.css" media="all" rel="stylesheet" type="text/css">
        <link href="css/font-awesome-4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"><!-- style end -->
        <!-- google fonts start -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900%7CMontserrat:400,700%7CDosis:200,300,400,500,600,700,800" rel="stylesheet" type="text/css">
        <!-- google fonts end -->
        <!--[if lt IE 9]>
            <script src="bower_components/html5shiv/dist/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body id="home">
        <!-- preloader start -->
        <div id="preloader">
            <div id="preloader-status">
                <div class="preloader-position loader">
                    <span></span>
                </div>
            </div>
        </div><!-- preloader end -->
        <!-- logo start -->
         <a class="logo logo-dark scrollToTop">
        <div class="logo-img"></div></a><!-- logo end -->
         <!-- navigation trigger start -->
        <div class="menu-trigger" id="menu-mobile-btn">
            <button class="lines-button minus menu-toggle" id="menu-mobile-caller" type="button"><span class="lines inverse"></span></button>
        </div><!-- navigation trigger end -->
        <!-- navigation start -->
        <div id="ui-layer">
            <div id="menu-wrapper">
                <div id="menu-mobile">
                    <div class="menu-nav-wrapper">
                        <nav class="menu-nav">
                            <ul>
                                <li class="menu-mobile">
                                    <a class="menu-state active" data-scroll-to="home">Home</a>
                                </li>
                                <li class="menu-mobile">
                                    <a class="menu-state" data-scroll-to="about">Sobre mim</a>
                                </li>
                                <li class="menu-mobile">
                                    <a class="menu-state" data-scroll-to="services">Serviços</a>
                                </li>
                                <li class="menu-mobile">
                                    <a class="menu-state" data-scroll-to="works">Portfólio</a>
                                </li>
                                <li class="menu-mobile">
                                    <a class="menu-state" data-scroll-to="news">Novidades</a>
                                </li>
                                <li class="menu-mobile">
                                    <a class="menu-state" data-scroll-to="contact">Contato</a>
                                </li>
                                <li class="credits">
                                    <a href="#">&copy;2017 Todos Direitos Reservados</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div><!-- navigation end -->
        <!-- home page start -->
        <div class="home-page dark-logo">
            <!-- hero bg start -->
            <div class="hero-fullscreen">
                <!-- single image start -->
                <div class="parallax-window" data-image-src="img/background/SINGLE-bg-light.jpg" data-parallax="scroll"></div><!-- single image end -->
            </div><!-- hero bg end -->
            <!-- home page overlay start -->
            <div class="home-page-overlay light"></div><!-- home page overlay end -->
            <!-- center container start -->
            <div class="center-container">
                <!-- center block start -->
                <div class="center-block">
                    <h1 class="home-page-title light">
                        GSARAIVA
                    </h1>
                    <div class="home-page-subtitle-carousel owl-carousel">
                        <h2 class="home-page-title light">
                            <a data-scroll-to="about" href="#">A simplicidade é complexa</a>
                        </h2>
                        <h2 class="home-page-title light">
                            <a data-scroll-to="about" href="#">Design é um processo</a>
                        </h2>
                        <h2 class="home-page-title light">
                            <a data-scroll-to="about" href="#">A estética é uma decisão</a>
                        </h2>
                        <h2 class="home-page-title light">
                            <a data-scroll-to="about" href="#">Estilo é tudo</a>
                        </h2>
                    </div>
                </div><!-- center block end -->
            </div><!-- center container end -->
        </div><!-- home page end -->
        <!-- page wrapper start -->
        <div class="page-wrapper">
            <!-- about page start -->
            <div class="section section-about" id="about">
                <!-- container start -->
                <div class="container">
                    <!-- row start -->
                    <div class="row alt">
                        <h2 class="section-title">
                            Sobre mim
                        </h2>
                    </div><!-- row end -->
                    <!-- row start -->
                    <div class="row">
                        <div class="section-all-subtitle">
                            <span class="section-all-subtitle-first">Te ajudamos a projetar produtos e serviços inovadores.</span><br>
                           Você só precisa se preocupar com seu negócio.
                        </div>
                    </div><!-- row end -->
                    <!-- row start -->
                    <div class="row">
                        <!-- col left start -->
                        <div class="col-md-4">
                            <div class="quotations">
                                <span class="quote-mark-l fa fa-quote-left"></span>Se você quer chegar one a maioria não chega, faça o que a maioria não faz. - Bill Gates<span class="quote-mark-r fa fa-quote-right"></span>
                            </div>
                        </div><!-- col left end -->
                        <!-- divider start -->
                        <div class="divider-post"></div><!-- divider end -->
                        <!-- col right start -->
                        <div class="col-md-8">
                            <h3 class="section-all-subtitle-second">
                                Olá, como vai ?
                            </h3>
                            <p>
                            Meu nome é Gustavo Saraiva, sou Designer Gráfico e Web Designer freelancer na cidade de Campinas - SP.
                            </p>
                            <p>
                               Apaixonado pelo meu trabalho, projeto web sites criativos utilizando as mais recentes tecnologias dentro dos padrões da W3C e Web Standard.
                            </p>
                            <p>
                              A experiência, característica do nosso aprendizado contínuo, permite que nosso processo criativo atenda projetos de diferentes tamanhos e complexidade de forma eficiente, comunicativa e com fina qualidade técnica. <a class="section-links"
                                data-scroll-to="works" href="#">Baixe meu Currículo</a>.
                            </p>
                        </div><!-- col right end -->
                    </div><!-- row end -->
                    <!-- row start -->
                    <div class="row alt">
                        <!-- col left start -->
                        <div class="col-md-6">
                            <!-- skills bar start -->
                            <h3>
                                Know-how
                            </h3>
                            <div class="show-skillbar">
                                <div class="skillbar" data-percent="95">
                                    <span class="skillbar-title">Pacote Adobe</span>
                                    <div class="skillbar-bar"></div><span class="skill-bar-percent"></span>
                                </div><!-- skill 5 end -->
                                <!-- skill 1 start -->
                                <div class="skillbar" data-percent="95">
                                    <span class="skillbar-title">HTML5</span>
                                    <div class="skillbar-bar"></div><span class="skill-bar-percent"></span>
                                </div><!-- skill 1 end -->
                                <!-- skill 2 start -->
                                <div class="skillbar" data-percent="90">
                                    <span class="skillbar-title">CSS3</span>
                                    <div class="skillbar-bar"></div><span class="skill-bar-percent"></span>
                                </div><!-- skill 2 end -->
                            </div><!-- skills bar end -->
                        </div><!-- col left end -->
                        <!-- divider start -->
                        <div class="divider-post"></div><!-- divider end -->
                        <!-- col right start -->
                        <div class="col-md-6">
                            <!-- skills bar start -->
                            <!-- divider start -->
                            <div class="divider-skillbar"></div><!-- divider end -->
                            <div class="show-skillbar">
                                <!-- skill 3 start -->
                                <div class="skillbar" data-percent="85">
                                    <span class="skillbar-title">jQuery</span>
                                    <div class="skillbar-bar"></div><span class="skill-bar-percent"></span>
                                </div><!-- skill 3 end -->
                                <!-- skill 4 start -->
                                <div class="skillbar" data-percent="76">
                                    <span class="skillbar-title">PHP</span>
                                    <div class="skillbar-bar"></div><span class="skill-bar-percent"></span>
                                </div><!-- skill 4 end -->
                                <div class="skillbar" data-percent="100">
                                    <span class="skillbar-title">Inglês</span>
                                    <div class="skillbar-bar"></div><span class="skill-bar-percent"></span>
                                </div><!-- skill 4 end -->
                            </div><!-- skills bar end -->
                        </div><!-- col right end -->
                    </div><!-- row end -->
                </div><!-- container end -->
                <!-- about page images start -->
            </div><!-- about page end -->
            <!-- facts page start -->
            <div class="section section-facts light-logo" id="facts">
                <!-- container start -->
                <div class="container">
                    <!-- row start -->
                    <div class="row section">
                        <!-- facts counter start -->
                        <div class="facts-counter-wrapper">
                            <div class="col-xs-6 col-sm-3">
                                <div class="facts-counter-number">
                                    67
                                </div>
                                <div class="facts-counter-description">
                                    <i class="fa fa-briefcase"></i> <span class="facts-counter-title">Projetos finalizados</span>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="facts-counter-number">
                                    32
                                </div>
                                <div class="facts-counter-description">
                                    <i class="fa fa-users"></i> <span class="facts-counter-title">Clientes Satisfeitos</span>
                                </div>
                            </div><!-- clearfix start -->
                            <div class="clearfix visible-xs-block"></div><!-- clearfix end -->
                            <div class="col-xs-6 col-sm-3">
                                <div class="facts-counter-number">
                                    2
                                </div>
                                <div class="facts-counter-description">
                                    <i class="fa fa-globe"></i> <span class="facts-counter-title">Paises Atendidos</span>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="facts-counter-number">
                                    5424
                                </div>
                                <div class="facts-counter-description">
                                    <i class="fa fa-coffee"></i> <span class="facts-counter-title">Xícaras de Café</span>
                                </div>
                            </div>
                        </div><!-- facts counter end -->
                    </div><!-- row end -->
                </div><!-- container end -->
            </div><!-- facts page end -->
            <!-- services page start -->
            <div class="section section-services" id="services">
                <!-- container start -->
                <div class="container">
                    <!-- row start -->
                    <div class="row alt">
                        <h2 class="section-title">
                            Serviços
                        </h2> 

                    </div><!-- row end -->
                    <!-- row start -->
                    <div class="row">
                        <div class="section-all-subtitle">
                            <span class="section-all-subtitle-first">Aqui, cada cliente e projeto é único e especial.</span><br>
                            Não inventamos propostas sem antes conhecer os requisitos do seu projeto.
                        </div>
                    </div><!-- row end -->
                    <!-- row start -->
                    <div class="row">
                        <!-- col left start -->
                        <div class="col-md-6 services-hover">
                            <div class="services-img">
                                <span class="fa fa-briefcase"></span>
                            </div>
                            <h3 class="services-title">
                              Mídia Online
                            </h3>
                            <div class="services-description">
                            Desenvolvimento de E-mail marketing, Banners, Estratégias de Marketing Online, Google adwords, Páginas no facebook, blog e Redes Sociais no geral.
                            </div>
                        </div><!-- col left end -->
                        <!-- col right start -->
                        <div class="col-md-6 services-hover services-move-down">
                            <div class="services-img">
                                <span class="fa fa-users"></span>
                            </div>
                            <h3 class="services-title">
                               Branding
                            </h3>
                            <div class="services-description">
                            Desenvolvimento de Identidade Visual, Projetos Gráficos, diagramação e editoração. Criação de logotipo e sua aplicação em papelaria (cartões de visita, papel carta, envelopes), desenvolvimento de catálogos de produtos, mostruários, embalagens, uniformes e adesivação de veículos.                            </div>
                        </div><!-- col right end -->
                        <!-- col left start -->
                        <div class="col-md-6 services-hover">
                            <div class="services-img">
                                <span class="fa fa-globe"></span>
                            </div>
                            <h3 class="services-title">
                               Web Design
                            </h3>
                            <div class="services-description">
                               Criação e desenvolvimento de Web Sites, Landing Pages, Hot sites. Desenvolvimento de portais corporativos, sites institucionais para empresas, advogados, engenheiros, prestadores de serviço em geral e profissionais liberais.
                            </div>
                        </div><!-- col left end -->
                        <!-- col right start -->
                        <div class="col-md-6 services-hover services-move-down">
                            <div class="services-img">
                                <span class="fa fa-camera-retro"></span>
                            </div>
                            <h3 class="services-title">
                              Mídia Impressa
                            </h3>
                            <div class="services-description services-description-last">
                             Folders, Catálogos, Malas-diretas, Panfletos, Folhetos, Encartes, Anúncios em jornais e revistas.
                            </div>
                        </div><!-- col right end -->
                    </div><!-- row end -->
                </div><!-- container end -->
            </div><!-- services page end -->
            <!-- works page start -->
            <div class="section section-works" id="works">
                <!-- container start -->
                <div class="container">
                    <!-- row start -->
                    <div class="row alt">
                        <h2 class="section-title">
                            Portfólio
                        </h2>
                    </div><!-- row end -->
                    <!-- row start -->
                    <div class="row">
                        <div class="section-all-subtitle">
                            <span class="section-all-subtitle-first">Confira os últimos trabalhos realizados</span><br>
                        </div>
                    </div><!-- row end -->
                </div><!-- container end -->
                <!-- photoSwipe start -->
                <div class="legendary-gallery">
                    <!-- gallery item 1 start -->
                    <figure class="col-xl-6 col-lg-6 col-md-6 col-sm-6 image-works light-logo">
                        <a class="effect-bubba-photos" data-size="1920x1280" href="photoswipe/photos/large/1.jpg"><img alt="Image description" class="img-responsive" src=
                        "photoswipe/photos/small/1.jpg"></a>
                        <figcaption>
                            <span class="img-caption">Timbrado - Ávila Ribeiro e Fujii</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="#"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption>
                    </figure><!-- gallery item 1 end -->
                    <!-- gallery item 2 start -->
                    <figure class="col-xl-6 col-lg-6 col-md-6 col-sm-6 image-works light-logo">
                        <a class="effect-bubba-photos" data-size="1920x1280" href="photoswipe/photos/large/2.jpg"><img alt="Image description" class="img-responsive" src=
                        "photoswipe/photos/small/2.jpg"></a>
                        <figcaption>
                            <span class="img-caption">Cartão de Visita - Ávila Ribeiro e Fujii</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="#"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption>
                    </figure><!-- gallery item 2 end -->
                    <!-- gallery item 3 start -->
                    <figure class="col-xl-6 col-lg-6 col-md-6 col-sm-6 image-works light-logo">
                        <a class="effect-bubba-photos" data-size="1920x1280" href="photoswipe/photos/large/3.jpg"><img alt="Image description" class="img-responsive" src=
                        "photoswipe/photos/small/3.jpg"></a>
                        <figcaption>
                            <span class="img-caption">Site - Ávila Ribeiro e Fujii</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="#"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption>
                    </figure><!-- gallery item 3 end -->
                    <!-- gallery item 4 start -->
                    <figure class="col-xl-6 col-lg-6 col-md-6 col-sm-6 image-works light-logo">
                        <a class="effect-bubba-photos" data-size="1920x1280" href="photoswipe/photos/large/4.jpg"><img alt="Image description" class="img-responsive" src=
                        "photoswipe/photos/small/4.jpg"></a>
                        <figcaption>
                            <span class="img-caption">Envelope - Ávila Ribeiro e Fujii</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="#"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption>
                    </figure><!-- gallery item 4 end -->
                    
                     <!-- gallery item 5 start -->
                    
                    
                      <!-- gallery item 5 start -->
                    
                </div><!-- photoSwipe end -->
            </div><!-- works page end -->
            <!-- testimonials page start -->
            
            <!-- news page start -->
            
            <!-- contact page start -->
            <div class="section section-contact" id="contact">
                <!-- container start -->
                <div class="container">
                    <!-- row start -->
                    <div class="row alt">
                        <h2 class="section-title">
                            Contato
                        </h2>
                    </div><!-- row end -->
                    <!-- row start -->
                    <div class="row">
                        <div class="section-all-subtitle">
                            <span class="section-all-subtitle-first">Entre em contato conosco</span><br>
                            Deixe sua mensagem, entrarei em contato em breve.
                        </div>
                    </div><!-- row end -->
                    <!-- row start -->
                    <div class="row">
                        <div class="footer-contact-block">
                            <!-- col left start -->
                            <div class="col-md-4">
                                <!-- contact details start -->
                                <span class="footer-contact-block-label">Endereço:</span> Campinas,Sp <!-- contact details end -->
                            </div><!-- col left end -->
                            <!-- divider start -->
                            <div class="divider-post-contact"></div><!-- divider end -->
                            <!-- col center start -->
                            <div class="col-md-4">
                                <!-- contact details start -->
                                <span class="footer-contact-block-label">Telefone:</span> (19) 9 8769 6132 <!-- contact details end -->
                            </div><!-- col center end -->
                            <!-- divider start -->
                            <div class="divider-post-contact"></div><!-- divider end -->
                            <!-- col right start -->
                            <div class="col-md-4">
                                <!-- contact details start -->
                                <span class="footer-contact-block-label">Email:</span> <a class="email" href="mailto:contato@gsaraiva.com">contato@gsaraiva.com</a> 
                                <!-- contact details end -->
                            </div><!-- col right end -->
                        </div>
                    </div><!-- row end -->
                    <!-- row start -->
                    <div class="row">
                    <?php 
             if(isset($_POST['cadastrarFront'])){
            
                 $strFrontNome = trim(strip_tags($_POST['strFrontNome']));
                 $strFrontEmail = trim(strip_tags($_POST['strFrontEmail']));
                 $strFrontAssunto = trim(strip_tags($_POST['strFrontAssunto']));
                 $strFrontMsg = trim(strip_tags($_POST['strFrontMsg']));

                  $insert = "INSERT into tb_form_frontend (strFrontNome, strFrontEmail, strFrontAssunto, strFrontMsg) VALUES (:strFrontNome, :strFrontEmail, :strFrontAssunto, :strFrontMsg)";
                    try {
                    $result = $conexao->prepare($insert);
                    $result->bindParam(':strFrontNome', $strFrontNome, PDO::PARAM_STR);
                    $result->bindParam(':strFrontEmail', $strFrontEmail, PDO::PARAM_STR);
                    $result->bindParam(':strFrontAssunto', $strFrontAssunto, PDO::PARAM_STR);
                    $result->bindParam(':strFrontMsg', $strFrontMsg, PDO::PARAM_STR);
                  
                    $result->execute();
                    $contar = $result->rowCount();
                    if($contar>0){
                    
                    echo '<div class="alert media fade in alert-success">
                                <p><strong>Usuário inserido com sucesso!</strong></p>
                                </div>';
                        
                    }else{
                        echo'<div class="alert media fade in alert-danger">
                                <p><strong>Erro!</strong></p>
                                </div>';
                    }

                }catch(PDOException $e){
                echo $e;
                }
                    }
             
             ?>
                        <!-- contact form start -->
                        <div id="contact-form">
                            <form action="#" id="form" method="post" name="send">
                                <div class="col-md-4">
                                    <input class="requiredField name" name="strFrontNome" placeholder="Nome" type="text">
                                </div>
                                <div class="col-md-4">
                                    <input class="requiredField email" name="strFrontEmail" placeholder="Email" type="text">
                                </div>
                                <div class="col-md-4">
                                    <input class="requiredField subject"  name="strFrontAssunto" placeholder="Assunto" type="text">
                                </div>
                                <div class="make-space">
                                    <textarea class="requiredField message"  name="strFrontMsg" placeholder="Mensagem"></textarea>
                                </div>
                                <div>
                                    <button type="submit" name="cadastrarFront" class="submit-button" >Enviar</button>
                                </div>
                            </form>
                        </div><!-- contact form end -->
                    </div><!-- row end -->
                </div><!-- container end -->
            </div><!-- contact page end -->
            <!-- google maps page start -->

            <!-- footer page start -->
            <div class="section-footer">
                <!-- container start -->
                <div class="container">
                    <!-- row start -->
                    <div class="row">
                        <div class="section-content">
                            <!-- logo footer start -->
                            <div class="footer-logo scrollToTop"></div><!-- logo footer end -->
                            <!-- social icons footer start -->
                            <div class="social-icons-footer-wrapper">
                                <ul class="social-icons-footer">
                                  <!--  <li>
                                        <a class="fa fa-twitter" href="#"></a>
                                    </li> -->
                                    <li class="social-icon">
                                        <a class="fa fa-facebook" href="#"></a>
                                    </li>
                                    <!-- <li class="social-icon">
                                        <a class="fa fa-google-plus" href="#"></a>
                                    </li> -->
                                   <!--  <li class="social-icon">
                                        <a class="fa fa-youtube" href="#"></a>
                                    </li> -->
                                    <li class="social-icon">
                                        <a class="fa fa-linkedin" href="#"></a>
                                    </li>
                                    <li class="social-icon">
                                        <a class="fa fa-instagram" href="#"></a>
                                    </li>
                                    <!-- <li class="social-icon">
                                        <a class="fa fa-behance" href="#"></a>
                                    </li>
                                    <li class="social-icon">
                                        <a class="fa fa-pinterest" href="#"></a>
                                    </li>
                                    <li class="social-icon">
                                        <a class="fa fa-dribbble" href="#"></a>
                                    </li> -->
                                </ul>
                            </div><!-- social icons footer end -->
                            <div class="lineOT-footer"></div><!-- copyrights start -->
                            <div class="copy">
                                <a href="#">Todos direitos reservados &copy; gsaraiva.com</a>
                            </div><!-- copyrights end -->
                        </div>
                    </div><!-- row end -->
                </div><!-- container end -->
            </div><!-- footer page end -->
        </div><!-- page wrapper end -->
        <!-- photoSwipe background start -->
        <div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
            <div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap">
                <div class="pswp__container">
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                </div>
                <div class="pswp__ui pswp__ui--hidden">
                    <div class="pswp__top-bar">
                        <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
                        "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
                        "Zoom in/out"></button>
                        <div class="pswp__preloader">
                            <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                        <div class="pswp__share-tooltip"></div>
                    </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
                    "Next (arrow right)"></button>
                    <div class="pswp__caption">
                        <div class="pswp__caption__center"></div>
                    </div>
                </div>
            </div>
        </div><!-- photoSwipe background end -->
        <!-- scripts start -->
        <script src="https://maps.googleapis.com/maps/api/js" type="text/javascript">
        </script> 
        <script src="js/plugins.js" type="text/javascript">
        </script> 
        <script src="js/youth.js" type="text/javascript">
        </script><!-- scripts end -->
    </body>
</html>