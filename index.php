
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
            CAFEÍNA DIGITAL - DESIGN &amp; MARKETING DIGITAL
        </title>
        <meta property="og:type" content="website">
        <meta name="google-site-verification" content="6ib9wMha3rq__1Zr-3vHAMKgOJSzK-FkhAUXVkf80wY" />
        <meta property="og:locale" content="pt_BR">
        <meta property="og:description" content="Assessoria de comunicação e marketing para micro, pequenas e médias empresas. Agência de publicidade e propaganda em Campinas, visando atender sua empresa com eficiência, melhor prazo e custo-benefício.">
        <meta property="og:title" content="CAFEÍNA - DESIGN E MARKETING DIGITAL">
        <meta property="og:url" content="http://www.cafeina.digital">
        <meta property="og:site_name" content=" Agência de Publicidade e Propaganda em Campinas | Agência especializada em micro, pequenas e médias empresas. Criação de identidade visual, criação de websites, desenvolvimento de site, posicionamento e gestão de marcas, através das ferramentas do marketing, da comunicação, do monitoramento e gestão de mídias sociais, da propaganda, design gráfico, adwords, e do marketing digital gerando valor agregado e lucro para sua empresa. Uma agência de publicidade e propaganda em Campinas. Transformamos ideias em publicidade. Atendemos, Campinas, interior de são paulo, Sousas, Joaquim edigio, barão geraldo, hortolandia, Indaiatuba, Salto, lavras, minas gerais, poços de caldas, Sorocaba, Americana, sumare, mogi mirin, mogi guacu Jundiaí, Valinhos, Vinhedo, Itatiba, Itu, Sorocaba, Cosmópolis, Itupeva, Atibaia, Paulinia, Holambra, São Paulo | Oferecemos ao cliente Planejamento de marketing, de Midia, Relacionamento, desenvolvimento de websites, desenvolvimento de site, e-commerce, monitoramento de facebook, Endomarketing, Promocao, Feiras,  Exposicoes, Merchandising, Ponto-de-Venda, Design Grafico, Web Digital, Identidade Visual, Impressão, E-mail Marketing, Monitoramento de Midias Sociais">
        <meta content="height=device-height, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
        <meta property="og:image"  content="http://www.cafeina.digital/cafeinaImgOg.png" />
        <link href="css/plugins.css" media="all" rel="stylesheet" type="text/css">
        <link href="css/style.css" media="all" rel="stylesheet" type="text/css">
        <link href="css/font-awesome-4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900%7CMontserrat:400,700%7CDosis:200,300,400,500,600,700,800" rel="stylesheet" type="text/css">
        <!-- google fonts end -->
        <!--[if lt IE 9]>
        <script src="bower_components/html5shiv/dist/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body id="home">
        <div id="preloader">
            <div id="preloader-status">
                <div class="preloader-position loader">
                    <span></span>
                </div>
            </div>
        </div>
        <a class="logo logo-dark scrollToTop">
            <div class="logo-img"></div>
        </a>
        <button class="navigation-toggle" data-navigation-toggle="toggle"><span class="navigation-toggle-icon"><span class="navigation-toggle-bars navigation-toggle-bars-light"></span> <span class=
            "navigation-toggle-bars navigation-toggle-bars-light"></span> <span class="navigation-toggle-bars navigation-toggle-bars-light"></span></span></button> <!-- navigation button end -->
        <div class="navigation-overlay"></div>
        <nav class="navigation">
            <div class="center-container">
                <div class="center-block">
                    <ul class="menu main-menu">
                        <li class="menu-item">
                            <a class="menu-link" data-scroll-section="home" href="#">Home</a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" data-scroll-section="about" href="#">Sobre nós</a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" data-scroll-section="services" href="#">Serviços</a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" data-scroll-section="works" href="#">Portifólio</a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="https://www.facebook.com/agenciacafeinadigital/">Contato</a>
                        </li>
                    </ul>
                    <div class="navigation-social-icons">
                        <ul class="social-icons">
                            <li class="social-icon">
                                <a class="fa fa-linkedin" href="#"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="home-page dark-logo">
            <div class="hero-fullscreen">
                <div class="parallax-window" data-image-src="img/bgcafe.jpg" data-parallax="scroll"></div>
            </div>
            <div class="home-page-overlay light"></div>
            <div class="center-container">
                <div class="center-block">
                    <h1 class="home-page-title light">
                        CAFEÍNA DIGITAL
                    </h1>
                    <div class="home-page-subtitle-carousel owl-carousel">
                        <h2 class="home-page-title light">
                            <a data-scroll-to="about" href="#">A simplicidade é complexa</a>
                        </h2>
                        <h2 class="home-page-title light">
                            <a data-scroll-to="about" href="#">Design é um processo</a>
                        </h2>
                        <h2 class="home-page-title light">
                            <a data-scroll-to="about" href="#">A estética é uma decisão</a>
                        </h2>
                        <h2 class="home-page-title light">
                            <a data-scroll-to="about" href="#">Estilo é tudo</a>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-wrapper">
            <div class="section section-about" id="about">
                <div class="container">
                    <div class="row alt">
                        <h2 class="section-title">
                            Sobre NÓS
                        </h2>
                    </div>
                    <div class="row">
                        <div class="section-all-subtitle">
                            <span class="section-all-subtitle-first">Te ajudamos a projetar produtos e serviços inovadores para
                            você só pensar em seu negócio.</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="quotations">
                                <span class="quote-mark-l fa fa-quote-left"></span>Se você quer chegar onde a maioria não chega, faça o que a maioria não faz. - Bill Gates<span class="quote-mark-r fa fa-quote-right"></span>
                            </div>
                        </div>
                        <div class="divider-post"></div>
                        <div class="col-md-8">
                            <h3 class="section-all-subtitle-second">
                                Olá, como vai ?
                            </h3>

                            <p>A Cafeína nasceu na era digital especializada e empenhada em oferecer as melhores soluções em marketing digital, a Cafeína foi criada com o objetivo de oferecer aos seus clientes ferramentas que tragam resultados positivos e tangíveis para os negócios.
                            </p>
                            <p>
                              Hoje, uma empresa estar inserida no universo on-line vai muito além de ter um site ou perfil nas principais redes sociais, você deve estar lá sim, mas tem que participar!
Nosso negócio é colocar você e sua empresa nesta nova realidade com estratégias claras e bem definidas através de Business Intelligence e ferramentas de Marketing digital, gerando relatórios e indicadores para você tomar decisões assertivas potencializando seus investimentos e
</p>
<p>
  Se você quer dar um upgrade na sua marca, aumentar sua presença e relevância digital, potencializar sua conversão ou até mesmo criar uma marca ou um business plan desde o início, marque um café com a gente!
                            </p>
                            <p>
                                Venha tomar um café conosco!
                            </p>
                        </div>
                    </div>
                    <!-- <div class="row alt">
                        <div class="col-md-6">

                           <h3>

                              Know-how

                           </h3>

                           <div class="show-skillbar">

                              <div class="skillbar" data-percent="95">

                                 <span class="skillbar-title">Pacote Adobe</span>

                                 <div class="skillbar-bar"></div>

                                 <span class="skill-bar-percent"></span>

                              </div>



                              <div class="skillbar" data-percent="95">

                                 <span class="skillbar-title">HTML5</span>

                                 <div class="skillbar-bar"></div>

                                 <span class="skill-bar-percent"></span>

                              </div>

                              <div class="skillbar" data-percent="90">

                                 <span class="skillbar-title">CSS3</span>

                                 <div class="skillbar-bar"></div>

                                 <span class="skill-bar-percent"></span>

                              </div>

                           </div>

                        </div>

                        <div class="divider-post"></div>

                        <div class="col-md-6">

                           <div class="divider-skillbar"></div>

                           <div class="show-skillbar">

                              <div class="skillbar" data-percent="85">

                                 <span class="skillbar-title">jQuery</span>

                                 <div class="skillbar-bar"></div>

                                 <span class="skill-bar-percent"></span>

                              </div>

                              <div class="skillbar" data-percent="76">

                                 <span class="skillbar-title">PHP</span>

                                 <div class="skillbar-bar"></div>

                                 <span class="skill-bar-percent"></span>

                              </div>

                              <div class="skillbar" data-percent="100">

                                 <span class="skillbar-title">Inglês</span>

                                 <div class="skillbar-bar"></div>

                                 <span class="skill-bar-percent"></span>

                              </div>

                           </div>

                        </div>

                        </div>
                        -->
                </div>
            </div>
            <div class="section section-facts light-logo" id="facts">
                <div class="container">
                    <div class="row section">
                        <div class="facts-counter-wrapper">
                            <div class="col-xs-6 col-sm-3">
                                <div class="facts-counter-number">
                                    127
                                </div>
                                <div class="facts-counter-description">
                                    <i class="fa fa-briefcase"></i> <span class="facts-counter-title">Projetos finalizados</span>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="facts-counter-number">
                                    84
                                </div>
                                <div class="facts-counter-description">
                                    <i class="fa fa-users"></i> <span class="facts-counter-title">Clientes Satisfeitos</span>
                                </div>
                            </div>
                            <div class="clearfix visible-xs-block"></div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="facts-counter-number">
                                    4
                                </div>
                                <div class="facts-counter-description">
                                    <i class="fa fa-globe"></i> <span class="facts-counter-title">Paises Atendidos</span>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="facts-counter-number">
                                    15424
                                </div>
                                <div class="facts-counter-description">
                                    <i class="fa fa-coffee"></i> <span class="facts-counter-title">Xícaras de Café</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-services" id="services">
                <div class="container">
                    <div class="row alt">
                        <h2 class="section-title">
                            Serviços
                        </h2>
                    </div>
                    <div class="row">
                        <div class="section-all-subtitle">
                            <span class="section-all-subtitle-first">Aqui, cada cliente e projeto é único e especial.</span><br>
                            Não inventamos propostas sem antes conhecer os requisitos do seu projeto.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 services-hover">
                            <div class="services-img">
                                <span class="fa fa-briefcase"></span>
                            </div>
                            <h3 class="services-title">
                                Mídia Online
                            </h3>
                            <div class="services-description">
                                Desenvolvimento de E-mail marketing, Banners, Estratégias de Marketing Online, Google adwords, Páginas no facebook, blog e Redes Sociais no geral.
                            </div>
                        </div>
                        <div class="col-md-6 services-hover services-move-down">
                            <div class="services-img">
                                <span class="fa fa-users"></span>
                            </div>
                            <h3 class="services-title">
                                Branding
                            </h3>
                            <div class="services-description">
                                Desenvolvimento de Identidade Visual, Projetos Gráficos, diagramação e editoração. Criação de logotipo e sua aplicação em papelaria (cartões de visita, papel carta, envelopes), desenvolvimento de catálogos de produtos, mostruários, embalagens, uniformes e adesivação de veículos.
                            </div>
                        </div>
                        <div class="col-md-6 services-hover">
                            <div class="services-img">
                                <span class="fa fa-globe"></span>
                            </div>
                            <h3 class="services-title">
                                Web Design
                            </h3>
                            <div class="services-description">
                                Criação e desenvolvimento de Web Sites, Landing Pages, Hot sites. Desenvolvimento de portais corporativos, sites institucionais para empresas, advogados, engenheiros, prestadores de serviço em geral e profissionais liberais.
                            </div>
                        </div>
                        <div class="col-md-6 services-hover services-move-down">
                            <div class="services-img">
                                <span class="fa fa-camera-retro"></span>
                            </div>
                            <h3 class="services-title">
                                Mídia Impressa
                            </h3>
                            <div class="services-description services-description-last">
                                Folders, Catálogos, Malas-diretas, Panfletos, Folhetos, Encartes, Anúncios em jornais e revistas.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-works" id="works">
                <div class="container">
                    <div class="row alt">
                        <h2 class="section-title">
                            Clientes
                        </h2>
                    </div>
                    <div class="row">
                        <div class="section-all-subtitle">
                            <span class="section-all-subtitle-first">Nossos clientes</span><br>
                        </div>
                    </div>
                </div>
                <div class="legendary-gallery">
                    <figure class="col-xl-3 col-lg-3 col-md-3 col-sm-3 image-works light-logo">
                        <img alt="Image description" class="img-responsive" src=
                            "photoswipe/photos/small/imgClientesMax.png">
                        <!-- <figcaption>
                            <span class="img-caption">MAXRACER</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="http://www.maxracer.com.br"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption> -->
                    </figure>
                    <figure class="col-xl-3 col-lg-3 col-md-3 col-sm-3 image-works light-logo">
                        <img alt="Image description" class="img-responsive" src="photoswipe/photos/small/imgClientesMecolour.png">
                        <!-- <figcaption>
                            <span class="img-caption">MECOLOUR</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="https://www.mecolour.com.br/"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption> -->
                    </figure>
                    <figure class="col-xl-3 col-lg-3 col-md-3 col-sm-3 image-works light-logo">
                        <img alt="Image description" class="img-responsive" src="photoswipe/photos/small/imgClientesPCM.png">
                        <!-- <figcaption>
                            <span class="img-caption">PCM Consultoria</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="http://www.pcmconsultoria.com.br/"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption> -->
                    </figure>
                    <figure class="col-xl-3 col-lg-3 col-md-3 col-sm-3 image-works light-logo">
                        <img alt="Image description" class="img-responsive" src="photoswipe/photos/small/imgClientesIPEC.png">
                        <!-- <figcaption>
                            <span class="img-caption">Envelope - Ávila Ribeiro e Fujii</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="#"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption> -->
                    </figure>
                    <figure class="col-xl-3 col-lg-3 col-md-3 col-sm-3 image-works light-logo">
                        <img alt="Image description" class="img-responsive" src="photoswipe/photos/small/imgClientesPiron.png">
                        <!-- <figcaption>
                            <span class="img-caption">Envelope - Ávila Ribeiro e Fujii</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="#"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption> -->
                    </figure>
                    <figure class="col-xl-3 col-lg-3 col-md-3 col-sm-3 image-works light-logo">
                        <img alt="Image description" class="img-responsive" src="photoswipe/photos/small/imgClientesAssioni.png">
                        <!-- <figcaption>
                            <span class="img-caption">Envelope - Ávila Ribeiro e Fujii</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="#"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption> -->
                    </figure>
                    <figure class="col-xl-3 col-lg-3 col-md-3 col-sm-3 image-works light-logo">
                        <img alt="Image description" class="img-responsive" src="photoswipe/photos/small/imgClientesGM.png">
                        <!-- <figcaption>
                            <span class="img-caption">Envelope - Ávila Ribeiro e Fujii</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="#"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption> -->
                    </figure>
                    <figure class="col-xl-3 col-lg-3 col-md-3 col-sm-3 image-works light-logo">
                        <img alt="Image description" class="img-responsive" src="photoswipe/photos/small/imgClientesAvila.png">
                        <!-- <figcaption>
                            <span class="img-caption">Envelope - Ávila Ribeiro e Fujii</span>
                            <div class="hover-effect"></div>
                            <div class="icon-works">
                                <a class="iw-slide-right fa fa-plus" href="#"></a> <a class="iw-slide-left fa fa-share-alt" href="#"></a>
                            </div>
                        </figcaption> -->
                    </figure>
                </div>
            </div>
            <!-- <div class="section section-contact" id="contact">
                <div class="container">
                    <div class="row alt">
                        <h2 class="section-title">
                            Contato
                        </h2>
                    </div>
                    <div class="row">
                        <div class="section-all-subtitle">
                            <span class="section-all-subtitle-first">Entre em contato conosco</span><br>
                            Deixe sua mensagem, entrarei em contato em breve.
                        </div>
                    </div>
                    <div class="row">
                        <div class="footer-contact-block">
                            <div class="col-md-4">
                                <span class="footer-contact-block-label">Endereço:</span> Campinas/SP
                            </div>
                            <div class="divider-post-contact"></div>
                            <div class="col-md-4">
                                <span class="footer-contact-block-label">Telefone:</span> (19) 9 9440 5623
                            </div>
                            <div class="divider-post-contact"></div>
                            <div class="col-md-4">
                                <span class="footer-contact-block-label">Email:</span> <a class="email" href="mailto:agencia@cafeina.digital">agencia@cafeina.digital</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="contact-form">

                            <form action="" method="post" >
                                <div class="col-md-4">
                                    <input class="requiredField name" id="name" name="name" placeholder="Nome" type="text" style="position: relative;width: 100%;height: 40px;border-bottom: 1px solid #111;border-left: none;border-right: none;border-top: none;padding: 5px 5px;background: none;margin: 5px;-webkit-transition: all 0.3s;   -moz-transition: all 0.3s;    -ms-transition: all 0.3s;        transition: all 0.3s;font-family: 'Raleway', sans-serif;font-size: 14px;line-height: 1.5;font-style: normal;font-weight: normal;text-align: center;">
                                </div>
                                <div class="col-md-4">
                                    <input class="requiredField email" id="email" name="email" placeholder="E-mail" type="text" style="position: relative;width: 100%;height: 40px;border-bottom: 1px solid #111;border-left: none;border-right: none;border-top: none;padding: 5px 5px;background: none;margin: 5px;-webkit-transition: all 0.3s;   -moz-transition: all 0.3s;    -ms-transition: all 0.3s;        transition: all 0.3s;font-family: 'Raleway', sans-serif;font-size: 14px;line-height: 1.5;font-style: normal;font-weight: normal;text-align: center;">
                                </div>
                                <div class="col-md-4">
                                    <input class="requiredField subject" id="subject" name="subject" placeholder="Assunto" type="text" style="position: relative;width: 100%;height: 40px;border-bottom: 1px solid #111;border-left: none;border-right: none;border-top: none;padding: 5px 5px;background: none;margin: 5px;-webkit-transition: all 0.3s;   -moz-transition: all 0.3s;    -ms-transition: all 0.3s;        transition: all 0.3s;font-family: 'Raleway', sans-serif;font-size: 14px;line-height: 1.5;font-style: normal;font-weight: normal;text-align: center;">
                                </div>
                                <div class="make-space">
                                    <textarea class="requiredField message" id="message" name="message" placeholder="Mensagem"></textarea>
                                </div>
                                <div>
                                    <button class="submit-button" name="submit" id="submit" type="submit">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="section-footer">
                <div class="container">
                    <div class="row">
                        <div class="section-content">
                            <div class="footer-logo scrollToTop"></div>
                            <div class="social-icons-footer-wrapper">
                                <ul class="social-icons-footer">
                                    <li class="social-icon">
                                        <a class="fa fa-facebook" href="https://www.facebook.com/agenciacafeinadigital"></a>
                                    </li>
                                    <!-- <li class="social-icon">
                                        <a class="fa fa-linkedin" href="#"></a>

                                        </li>

                                        <li class="social-icon">

                                        <a class="fa fa-instagram" href="#"></a>

                                        </li>
                                        -->
                                </ul>
                            </div>
                            <div class="lineOT-footer"></div>
                            <div class="copy">
                                <a href="#">Todos direitos reservados &copy; cafeina.digital</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
            <div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap">
                <div class="pswp__container">
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                </div>
                <div class="pswp__ui pswp__ui--hidden">
                    <div class="pswp__top-bar">
                        <div class="pswp__counter"></div>
                        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
                            "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
                            "Zoom in/out"></button>
                        <div class="pswp__preloader">
                            <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                        <div class="pswp__share-tooltip"></div>
                    </div>
                    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
                        "Next (arrow right)"></button>
                    <div class="pswp__caption">
                        <div class="pswp__caption__center"></div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/plugins.js" type="text/javascript"></script>
        <script src="js/youth.js" type="text/javascript"></script>
    </body>
</html>
