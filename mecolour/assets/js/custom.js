$(document).ready(function() {
    $('#p-btn1, #p-btn2, #p-btn3, #p-btn4, #p-btn5, #p-btn6, #p-btn7, #p-btn8, #p-btn9, #p-btn10, #p-btn11').click(function(){
        $(this).toggleClass('icon-like', 500);
    });

});

$(document).ready(function() {

    // Default Datatable
    $('#datatable').DataTable();

    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf'],
    });

    // Key Tables

    $('#key-table').DataTable({
        keys: true
    });

    // Responsive Datatable
    $('#responsive-datatable').DataTable();

    // Multi Selection Datatable
    $('#selection-datatable').DataTable({
        select: {
            style: 'multi'
        }
    });

    table.buttons().container()
        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );

