$(document).ready(function() {

    handleStatusChanged();

});

function handleStatusChanged() {
    $('#checkbox-10').on('change', function () {
        toggleStatus();
    });
}

function toggleStatus() {
    if ($('#checkbox-10').is(':checked')) {
        $("#elementsToOperateOn").removeClass("d-none");
    } else {
        $("#elementsToOperateOn").addClass("d-none");
    }

}

handleStatusChanged2();


function handleStatusChanged2() {
    $('#checkbox-11').on('change', function () {
        toggleStatus2();
    });
}

function toggleStatus2() {
    if ($('#checkbox-11').is(':checked')) {
        $("#elementsToOperateOn2").removeClass("d-none");
    } else {
        $("#elementsToOperateOn2").addClass("d-none");
    }

}

handleStatusChanged3();


function handleStatusChanged3() {
    $('#checkbox-12').on('change', function () {
        toggleStatus3();
    });
}

function toggleStatus3() {
    if ($('#checkbox-12').is(':checked')) {
        $("#elementsToOperateOn3").removeClass("d-none");
    } else {
        $("#elementsToOperateOn3").addClass("d-none");
    }

}
handleStatusChanged4();


function handleStatusChanged4() {
    $('#checkbox-13').on('change', function () {
        toggleStatus4();
    });
}

function toggleStatus4() {
    if ($('#checkbox-13').is(':checked')) {
        $("#elementsToOperateOn4").removeClass("d-none");
    } else {
        $("#elementsToOperateOn4").addClass("d-none");
    }

}

handleStatusChanged5();


function handleStatusChanged5() {
    $('#checkbox-14').on('change', function () {
        toggleStatus5();
    });
}

function toggleStatus5() {
    if ($('#checkbox-14').is(':checked')) {
        $("#elementsToOperateOn5").removeClass("d-none");
    } else {
        $("#elementsToOperateOn5").addClass("d-none");
    }

}

handleStatusChanged6();


function handleStatusChanged6() {
    $('#checkbox-15').on('change', function () {
        toggleStatus6();
    });
}

function toggleStatus6() {
    if ($('#checkbox-15').is(':checked')) {
        $("#elementsToOperateOn6").removeClass("d-none");
    } else {
        $("#elementsToOperateOn6").addClass("d-none");
    }

}

