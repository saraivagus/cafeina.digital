
<?php
// load up your config file
require_once("../dbConfig.php");

include_once (CONTROLLERS_PATH . "/ControllersCadastraClientes.php");
?>

<div class="content">
    <div class="container-fluid">



        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title">Cadastro de Clientes</h4>
                    <?php echo $msgClientesSucesso; ?>
                    <?php echo $msgClientesErro; ?>
                    <?php echo $e; ?>

                    <form  role="form" class="form-signin" action="" method="post" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="strClientesEmpresa" class="col-form-label">Empresa</label>
                                <input type="text" class="form-control" id="strClientesEmpresa" name="strClientesEmpresa" placeholder="Insira o nome da Empresa" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="strClientesEmail" class="col-form-label">E-mail</label>
                                <input type="email" class="form-control" id="strClientesEmail" name="strClientesEmail" placeholder="Insira o E-mail" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="strClientesTelefoneFixo" class="col-form-label">Telefone Fixo</label>
                                <input type="text" class="form-control" id="strClientesTelefoneFixo" name="strClientesTelefoneFixo" placeholder="Insira o Telefone Fixo" data-mask="(99) 99999-999">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="strClientesTelefoneMovel" class="col-form-label">Telefone Móvel</label>
                                <input type="text" class="form-control" id="strClientesTelefoneMovel" name="strClientesTelefoneMovel" placeholder="Insira o Telefone Móvel" data-mask="(99) 99999-9999" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="strClientesCNPJ" class="col-form-label">CNPJ</label>
                                <input type="text" data-mask="99.999.999/9999-99" class="form-control" id="strClientesCNPJ" name="strClientesCNPJ" placeholder="Insira o CNPJ do Cliente" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="strClientesResponsavel" class="col-form-label">Responsável</label>
                                <input type="text" class="form-control" id="strClientesResponsavel" name="strClientesResponsavel" placeholder="Insira o Nome do Responsável" required>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="strClientesEndereco" class="col-form-label">Endereço</label>
                            <input type="text" class="form-control" id="strClientesEndereco" name="strClientesEndereco" placeholder="Insira o Endereço" required>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="strClientesEstado" class="col-form-label">Estado</label>
                                <select id="estados" name="strClientesEstado" class="form-control" required><option> Choose</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="strClientesCidade" class="col-form-label">Cidade</label>
                                <select id="cidades" name="strClientesCidade" class="form-control" required>
                                <option> Choose</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="strClientesCEP" class="col-form-label">CEP</label>
                                <input type="text" class="form-control" id="strClientesCEP" name="strClientesCEP" data-mask="99999-999" placeholder="Insira o CEP" required>
                            </div>
                        </div>

                        <div class="form-row">

                            <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisCadastro/CadastraRedeSocialYoutube.php"); ?>

                            <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisCadastro/CadastraRedeSocialFacebook.php"); ?>

                            <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisCadastro/CadastraRedeSocialInstagram.php"); ?>

                            <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisCadastro/CadastraRedeSocialAnalytics.php"); ?>

                            <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisCadastro/CadastraRedeSocialHospedagem.php"); ?>

                            <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisCadastro/CadastraRedeSocialOutros.php"); ?>

                        </div>

                        <button type="submit" name="cadastrar" class="btn btn-primary">Cadastrar</button>
                    </form>
                </div>
            </div>
        </div>

</div>


    <?php include_once (CONTROLLERS_PATH . "/ControllersIncludeFooterPages.php"); ?>
