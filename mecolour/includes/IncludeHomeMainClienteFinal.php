
        <?php include_once(CONTROLLERS_PATH . "/ControllersDeletaLeadCF.php"); ?>


                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Últimos Leads <span class="text-purple">Cliente Final</span></h4>

                            <?php echo $usuarioDeletadoSucessoCF; ?>
                            <?php echo $usuarioDeletadoErroCF; ?>

                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-12 text-sm-center form-inline">
                                        <div class="form-group mr-2">
                                            <select id="demo-foo-filter-status" class="custom-select">
                                                <option value="">Ver Todos</option>
                                                <option value="ativo">Ativo</option>
                                                <option value="desativado">Desativado</option>
                                                <option value="suspenso">Suspensos</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table id="demo-foo-filtering" class="table table-striped table-bordered toggle-circle m-b-0" data-page-size="7">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Tipo de Cliente</th>
                                    <th>Estado</th>
                                    <th>Cidade</th>
                                    <th>Configuração</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(empty($_GET['pgcf'])){}
                                else{ $pgcf = $_GET['pgcf'];
                                    if(!is_numeric($pgcf)){
                                        echo '<script language="JavaScript">
                                           location.href=" ViewClientesVisualizar.php"; </script>';
                                    }

                                }
                                if(isset($pgcf)){ $pgcf = $_GET['$pgcf'];}else{ $pgcf = 1;}

                                $quantidade = 5;
                                $inicio = ($pgcf*$quantidade) - $quantidade;
                                $select = "SELECT * from dbformcf ORDER BY dbFormIDcf DESC LIMIT $inicio, $quantidade";
                                $contagem =1;
                                try {
                                    $result = $conexao->prepare($select);
                                    $result->execute();
                                    $contar = $result->rowCount();
                                    if($contar>0){
                                        while($show = $result->FETCH(PDO::FETCH_OBJ)){

                                            $date = date_create($show->strClientesDataCadastro);
                                            $date = date_format($date, 'd-m-Y');
                                            $dateToday = date('d-m-Y', strtotime("-1 days"));

                                            ?>
                                            <tr>
                                                <td>

                                                    <?php if($date > $dateToday){ ?>
                                                        <span class="badge badge-purple">Novo</span>
                                                    <?php } ?>
                                                    <?php echo $show->strNome;?>
                                                </td>
                                                <td><?php echo $show->strEmail;?></td>
                                                <td><?php echo $show->strCheckboxCF;?></td>
                                                <td><?php echo $show->strEstado;?></td>
                                                <td><?php echo $show->strCidade;?></td>

                                                <td>
                                                    <a href="ViewLeadsClienteFinalUnico.php?id=<?php echo $show->dbFormIDcf;?>" class="icon-eye btn btn-purple" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar Usuário"> </a>
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                    }else{
                                        echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Desculpe, não existem dados cadastrados no momento !
                                        </div>';
                                    }
                                }catch(PDOException $e){
                                    echo $e;
                                }
                                ?>

                                </tbody>
                                <tfoot>
                                <tr class="active">
                                    <td colspan="6">
                                        <a href="ViewLeadsClienteFinal.php" class="btn btn-sm btn-purple waves-light waves-effect pull-right">Ver todos</a>

                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

