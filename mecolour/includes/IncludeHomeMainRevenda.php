
<?php include_once(CONTROLLERS_PATH . "/ControllersDeletaLeadRV.php"); ?>


<div class="col-12">
    <div class="card-box">
        <h4 class="m-t-0 header-title">Últimos Leads <span class="text-pink">Revendedores</span></h4>

        <?php echo $usuarioDeletadoSucessoRV; ?>
        <?php echo $usuarioDeletadoErroRV; ?>

        <div class="mb-3">
            <div class="row">
                <div class="col-12 text-sm-center form-inline">
                    <div class="form-group mr-2">
                        <select id="demo-foo-filter-status" class="custom-select">
                            <option value="">Ver Todos</option>
                            <option value="ativo">Ativo</option>
                            <option value="desativado">Desativado</option>
                            <option value="suspenso">Suspensos</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <table id="demo-foo-filtering" class="table table-striped table-bordered toggle-circle m-b-0" data-page-size="7">
            <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Tipo de Cliente</th>
                <th>Estado</th>
                <th>Cidade</th>
                <th>Configuração</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(empty($_GET['pgrv'])){}
            else{ $pgrv = $_GET['pgrv'];
                if(!is_numeric($pgrv)){
                    echo '<script language="JavaScript">
                                           location.href=" ViewClientesVisualizar.php"; </script>';
                }

            }
            if(isset($pgrv)){ $pgrv = $_GET['pgrv'];}else{ $pgrv = 1;}

            $quantidade = 5;
            $inicio = ($pgrv*$quantidade) - $quantidade;
            $select = "SELECT * from dbformrv ORDER BY dbFormIDRv DESC LIMIT $inicio, $quantidade";
            $contagem =1;
            try {
                $result = $conexao->prepare($select);
                $result->execute();
                $contar = $result->rowCount();
                if($contar>0){
                    while($show = $result->FETCH(PDO::FETCH_OBJ)){

                        $date = date_create($show->strClientesDataCadastro);
                        $date = date_format($date, 'd-m-Y');
                        $dateToday = date('d-m-Y', strtotime("-1 days"));

                        ?>
                        <tr>
                            <td>

                                <?php if($date > $dateToday){ ?>
                                    <span class="badge badge-pink">Novo</span>
                                <?php } ?>
                                <?php echo $show->strNome;?>
                            </td>
                            <td><?php echo $show->strEmail;?></td>
                            <td><?php echo $show->strCheckboxRV;?></td>
                            <td><?php echo $show->strEstado;?></td>
                            <td><?php echo $show->strCidade;?></td>

                            <td>
                                <a href="ViewLeadsRevendaUnico.php?id=<?php echo $show->dbFormIDRv;?>" class="icon-eye btn btn-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar Usuário"> </a>
                            </td>
                        </tr>

                        <?php
                    }
                }else{
                    echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Desculpe, não existem dados cadastrados no momento !
                                        </div>';
                }
            }catch(PDOException $e){
                echo $e;
            }
            ?>

            </tbody>
            <tfoot>
            <tr class="active">
                <td colspan="6">
                    <a href="ViewLeadsRevenda.php" class="btn btn-sm btn-pink waves-light waves-effect pull-right">Ver todos</a>

                </td>
            </tr>
            </tfoot>

        </table>

    </div>
</div>

