<div class="topbar">
    <?php
    if(isset($_GET['acao'])){
        $acao = $_GET['acao'];

        if($acao=='welcome'){
            echo
                '<div class="alert alert-info">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      Olá <strong>'.$nomeLogado.'</strong>,  bem vindo ao sistema! Abaixo estão suas últimas atualizações
               </div>';
        }
    }
    ?>
    <nav class="navbar-custom">

        <ul class="list-unstyled topbar-right-menu float-right mb-0">

<!--            <li class="hide-phone app-search">-->
<!--                <form>-->
<!--                    <input type="text" placeholder="Buscar..." class="form-control">-->
<!--                    <button type="submit"><i class="fa fa-search"></i></button>-->
<!--                </form>-->
<!--            </li>-->





            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                   aria-haspopup="false" aria-expanded="false">
                    <img src="../data/uploads/perfil/<?php echo $imagem?>" alt="user" class="rounded-circle"> <span class="ml-1"><?php echo $nomeLogado ?> <i class="mdi mdi-chevron-down"></i> </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h6 class="text-overflow m-0"><script language="JavaScript">
                                var myDate = new Date(); if ( myDate.getHours() < 12 )
                                { document.write("Bom dia!");
                                }else  /* Hour is from noon to 5pm (actually to 5:59 pm) */
                                if ( myDate.getHours() >= 12 && myDate.getHours() <= 17 )
                                { document.write("Boa tarde!");
                                } else  /* the hour is after 5pm, so it is between 6pm and midnight */
                                if ( myDate.getHours() > 17 && myDate.getHours() <= 24 )
                                { document.write("Boa noite!"); } else  /* the hour is not between 0 and 24, so something is wrong */
                                { document.write("I'm not sure what time it is!"); }
                            </script> </h6>
                    </div>

                    <!-- item-->
<!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
<!--                        <i class="fi-head"></i> <span>Minha Conta</span>-->
<!--                    </a>-->

<!--                    <!-- item-->
<!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
<!--                        <i class="fi-cog"></i> <span>Configurações</span>-->
<!--                    </a>-->
<!---->
<!--                    <!-- item-->
<!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
<!--                        <i class="fi-help"></i> <span>Support</span>-->
<!--                    </a>-->
<!---->
<!--                    <!-- item-->
<!--                    <a href="javascript:void(0);" class="dropdown-item notify-item">-->
<!--                        <i class="fi-lock"></i> <span>Lock Screen</span>-->
<!--                    </a>-->

                    <!-- item-->
                    <a href="?sair" onclick="return confirm('Deseja realmente sair?');" class="dropdown-item notify-item">
                        <i class="fi-power"></i> <span>Sair</span>
                    </a>

                </div>
            </li>

        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left disable-btn">
                    <i class="dripicons-menu"></i>
                </button>
            </li>
            <li>
                <div class="page-title-box">
                    <h4 class="page-title">Sistema Cafeína</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"> V 1.1B </li>
                    </ol>
                </div>
            </li>

        </ul>

    </nav>

</div>