<!-- Hospedagem -->
<div class="col-md-5">
    <div class="alert alert-secondary " role="alert">
        <div class="checkbox checkbox-warning checkbox-circle">
            <input id="checkbox-14" type="checkbox" name="toggle5" onchange="YouTube()">
            <label for="checkbox-14" style="position: relative; bottom: 5px">

            </label>
            Hospedagem <i class=" icon-drawar"></i>
        </div>
        <div id="elementsToOperateOn5" class="d-none">
            <div class="form-group col-md-12">
                <label for="strClientesCEP" class="col-form-label" style="font-size: 12px"><i class="icon-drawar"></i> Tipo</label>
                <select class="custom-select " name="strClientesSocialHospedagemTipo" id="strClientesSocialHospedagemTipo">
                    <option selected="">Selecione</option>
                    <option value="Cafeína">Cafeína</option>
                    <option value="Hostgator">Hostgator</option>
                    <option value="Godaddy">Godaddy</option>
                    <option value="LocaWeb">LocaWeb</option>
                    <option value="Uol Host">Uol Host</option>
                    <option value="Umbler">Umbler</option>
                </select>
                <label for="strClientesSocialHospedagemEmail" class="col-form-label" style="font-size: 12px"><i class="icon-user"></i> Email</label>
                <input type="email" class="form-control " id="strClientesSocialHospedagemEmail" name="strClientesSocialHospedagemEmail" >
                <label for="strClientesSocialHospedagemSenha" class="col-form-label" style="font-size: 12px"><i class="icon-lock"></i> Senha</label>
                <input type="password" class="form-control " id="strClientesSocialHospedagemSenha" name="strClientesSocialHospedagemSenha" >
                <label for="strClientesSocialHospedagemLink" class="col-form-label" style="font-size: 12px"><i class="icon-link"></i> Link de Acesso</label>
                <input type="text" class="form-control" id="strClientesSocialHospedagemLink" name="strClientesSocialHospedagemLink" >
                </label>
            </div>
        </div>
    </div>
</div>