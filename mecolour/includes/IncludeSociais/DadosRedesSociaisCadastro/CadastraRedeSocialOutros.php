<!-- Outros -->
<div class="col-md-7">
    <div class="alert alert-secondary " role="alert">
        <div class="checkbox checkbox-warning checkbox-circle">
            <input id="checkbox-15" type="checkbox" name="toggle6" onchange="YouTube()">
            <label for="checkbox-15" style="position: relative; bottom: 5px">

            </label>
            Outros <i class=" icon-list"></i>
        </div>
        <div id="elementsToOperateOn6" class="d-none">
            <div class="form-group col-md-12">
                <h4 class="mb-1" style="color:red; "></h4>
                <label for="strClientesSocialOutrosTitulo" class="col-form-label" style="font-size: 12px"><i class="icon-note"></i> Título</label>
                <input type="text" class="form-control " id="strClientesSocialOutrosTitulo" name="strClientesSocialOutrosTitulo" >
                <label for="strClientesSocialOutrosMensagem" class="col-form-label" style="font-size: 12px"><i class="icon-notebook"></i> Mensagem</label>
                <textarea class="form-control" rows="5" name="strClientesSocialOutrosMensagem" id=""></textarea>


            </div>
        </div>
    </div>
</div>