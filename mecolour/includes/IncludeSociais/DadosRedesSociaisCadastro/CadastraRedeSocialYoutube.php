<!-- Youtube -->
<div class="col-md-3">
    <div class="alert alert-secondary " role="alert">
        <div class="checkbox checkbox-danger checkbox-circle">
            <input id="checkbox-10" type="checkbox" name="toggle" onchange="YouTube()">
            <label for="checkbox-10" style="position: relative; bottom: 5px">

            </label>
            YouTube <i class="icon-social-youtube "></i>
        </div>
        <div id="elementsToOperateOn" class="d-none">
            <div class="form-group col-md-12">
                <h4 class="mb-1" style="color:red; "></h4>
                <label for="strClientesSocialYouTubeEmail" class="col-form-label" style="font-size: 12px"><i class="icon-user"></i> Email</label>
                <input type="text" class="form-control " id="strClientesSocialYouTubeEmail" name="strClientesSocialYouTubeEmail" >
                <label for="strClientesSocialYouTubeSenha" class="col-form-label" style="font-size: 12px"><i class="icon-lock"></i> Senha</label>
                <input type="password" class="form-control " id="strClientesSocialYouTubeSenha" name="strClientesSocialYouTubeSenha" >
                </label>
            </div>
        </div>
    </div>
</div>