<div class="col-md-12 col-lg-3">

    <div class="company-card card-box">
        <div class="text-right">
            <a href="https://www.youtube.com.br" style="color: #000;" target="_blank"><i class="fi-open" data-toggle="tooltip" data-placement="top" title="" data-original-title="Acessar"></i> </a>
        </div>
        <img src="../assets/images/companies/youtube.png" alt="logo" class="company-logo">
        <div class="company-detail">

            <h4 class="mb-1">YouTube</h4>
            <p>Dados do Youtube</p>
        </div>
        <?php if($nivelLogado ==1){?>
            <h5 class="text-muted font-normal">Usuário:</h5>
            <div class="alert alert-custom " role="alert">
                <h5 class="text-muted font-normal">

                <span class="pull-right ">
                    <i onclick="copyToClipboard('#p1')" id="p-btn1" class="icon-docs" data-toggle="tooltip" data-placement="top"  data-original-title="Copiar" ></i>
                </span>
                     <span id="p1"><?php echo $show->strClientesSocialYouTubeEmail;?></span></h5>
            </div>

            <h5 class="text-muted font-normal">Senha:</h5>
        <div class="alert alert-custom" role="alert">
            <h5 class="text-muted font-normal">
                <span class="pull-right ">
                    <i onclick="copyToClipboard('#p2')" id="p-btn2"  class="icon-docs"  data-toggle="tooltip" data-placement="top" data-original-title="Copiar" ></i>
                </span>
                <span id="p2"><?php echo $show->strClientesSocialYouTubeSenha;?></span></h5>
        </div>
        <?php }?>
        <?php if($nivelLogado ==0){?>
            <h5 class="text-muted font-normal">Usuário:</h5>
            <div class="alert alert-custom " role="alert">
                <h5 class="text-muted font-normal">

                <span class="pull-right ">
                </span>
                    <span>Privado</span></h5>
            </div>
            <h5 class="text-muted font-normal">Senha:</h5>
            <div class="alert alert-custom" role="alert">
                <h5 class="text-muted font-normal">
                <span class="pull-right ">
                </span>   <span>Privado</span></h5>
            </div>
        <?php }?>

    </div>
</div>
