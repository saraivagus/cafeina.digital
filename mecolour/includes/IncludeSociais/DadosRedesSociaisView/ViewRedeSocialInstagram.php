<div class="col-md-6 col-lg-3">
    <div class="company-card card-box">
        <div class="text-right">
            <a href="https://www.youtube.com.br" style="color: #000;" target="_blank"><i class="fi-open" data-toggle="tooltip" data-placement="top" title="" data-original-title="Acessar"></i> </a>
        </div>
        <img src="../assets/images/companies/instagram.png" alt="logo" class="company-logo">
        <div class="company-detail">
            <h4 class="mb-1">Instagram</h4>
            <p>Dados do Instagram</p>
        </div>
        <?php if($nivelLogado ==1){?>
            <h5 class="text-muted font-normal">Usuário:</h5>
            <div class="alert alert-custom " role="alert">
                <h5 class="text-muted font-normal">

                <span class="pull-right ">
                    <i onclick="copyToClipboard('#p5')"  id="p-btn5" class="icon-docs" data-toggle="tooltip" data-placement="top"  data-original-title="Copiar"></i>
                </span>
                    <span id="p5"><?php echo $show->strClientesSocialInstagramUsuario;?></span></h5>
            </div>
            <h5 class="text-muted font-normal">Senha:</h5>
            <div class="alert alert-custom" role="alert">
                <h5 class="text-muted font-normal">
                <span class="pull-right ">
                    <i onclick="copyToClipboard('#p6')" id="p-btn6"  class="icon-docs" data-toggle="tooltip" data-placement="top"  data-original-title="Copiar"></i>
                </span>   <span id="p6"><?php echo $show->strClientesSocialInstagramSenha;?></span></h5>
            </div>
        <?php }?>
        <?php if($nivelLogado ==0){?>
            <h5 class="text-muted font-normal">Usuário:</h5>
            <div class="alert alert-custom " role="alert">
                <h5 class="text-muted font-normal">

                <span class="pull-right ">
                </span>
                    <span>Privado</span></h5>
            </div>


            <h5 class="text-muted font-normal">Senha:</h5>
            <div class="alert alert-custom" role="alert">
                <h5 class="text-muted font-normal">
                <span class="pull-right ">
                </span>   <span>Privado</span></h5>
            </div>        <?php }?>

    </div>
</div>

