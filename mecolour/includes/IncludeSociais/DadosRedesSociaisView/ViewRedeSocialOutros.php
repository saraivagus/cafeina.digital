<div class="col-md-6 col-lg-9" >
    <div class="company-card card-box" >
        <img src="../assets/images/companies/dadosgerais.png" alt="logo" class="company-logo">
        <div class="company-detail">
            <h4 class="mb-1">Outros</h4>
            <p>Dados Gerais</p>
        </div>
        <h4 class="mb-1"><?php echo $show->strClientesSocialOutrosTitulo;?></h4>
        <p><?php echo $show->strClientesSocialOutrosMensagem;?></p>

    </div>
</div>