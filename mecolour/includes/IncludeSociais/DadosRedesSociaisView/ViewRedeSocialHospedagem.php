<div class="col-md-6 col-lg-3">
    <div class="company-card card-box">
        <div class="text-right">
            <a href="<?php echo $show->strClientesSocialHospedagemLink;?>" style="color: #000;" target="_blank"><i class="fi-open" data-toggle="tooltip" data-placement="top" title="" data-original-title="Acessar"></i> </a>
        </div>
        <img src="../assets/images/companies/hospedagem.png" alt="logo" class="company-logo">
        <div class="company-detail">
            <h4 class="mb-1">Hospedagem</h4>
            <p>Dados de Hospedagem</p>
        </div>
        <?php if($nivelLogado ==1){?>
            <h5 class="text-muted font-normal">Tipo:</h5>
            <div class="alert alert-custom " role="alert">
                <h5 class="text-muted font-normal">

                <span class="pull-right ">
                    <i onclick="copyToClipboard('#p9')" id="p-btn9"   class="icon-docs"  data-toggle="tooltip" data-placement="top"  data-original-title="Copiar"></i>
                </span>
                    <span id="p9"><?php echo $show->strClientesSocialHospedagemTipo;?></span></h5>
            </div>
            <h5 class="text-muted font-normal">E-mail:</h5>
            <div class="alert alert-custom" role="alert">
                <h5 class="text-muted font-normal">
                <span class="pull-right ">
                    <i onclick="copyToClipboard('#p10')"  id="p-btn10" class="icon-docs"  data-toggle="tooltip" data-placement="top" data-original-title="Copiar"></i>
                </span>   <span id="p10"><?php echo $show->strClientesSocialHospedagemEmail;?></span></h5>
            </div>

            <h5 class="text-muted font-normal">Senha:</h5>
            <div class="alert alert-custom" role="alert">
                <h5 class="text-muted font-normal">
                <span class="pull-right ">
                    <i onclick="copyToClipboard('#p11')" id="p-btn11"  class="icon-docs"  data-toggle="tooltip" data-placement="top" data-original-title="Copiar"></i>
                </span>   <span id="p11"><?php echo $show->strClientesSocialHospedagemSenha;?></span></h5>
            </div>

        <?php }?>
        <?php if($nivelLogado ==0){?>
            <h5 class="text-muted font-normal">Tipo:</h5>
            <div class="alert alert-custom " role="alert">
                <h5 class="text-muted font-normal">

                <span class="pull-right ">
                </span>
                    <span >Privado</span></h5>
            </div>
            <h5 class="text-muted font-normal">E-mail:</h5>
            <div class="alert alert-custom" role="alert">
                <h5 class="text-muted font-normal">
                <span class="pull-right ">
                </span>   <span>Privado</span></h5>
            </div>
            <h5 class="text-muted font-normal">Senha:</h5>
            <div class="alert alert-custom" role="alert">
                <h5 class="text-muted font-normal">
                <span class="pull-right ">
                </span>   <span >Privado</span></h5>
            </div>
        <?php }?>

    </div>
</div>

