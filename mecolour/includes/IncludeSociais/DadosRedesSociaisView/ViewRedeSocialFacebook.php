<div class="col-md-6 col-lg-3">
    <div class="company-card card-box">
        <div class="text-right">
            <a href="https://www.youtube.com.br" style="color: #000;" target="_blank"><i class="fi-open" data-toggle="tooltip" data-placement="top" title="" data-original-title="Acessar"></i> </a>
        </div>
        <img src="../assets/images/companies/facebook.png" alt="logo" class="company-logo">
        <div class="company-detail">
            <h4 class="mb-1">Facebook</h4>
            <p>Dados do Facebook</p>
        </div>
        <?php if($nivelLogado ==1){?>
            <h5 class="text-muted font-normal">Usuário:</h5>
            <div class="alert alert-custom " role="alert">
                <h5 class="text-muted font-normal">

                <span class="pull-right ">
                    <i onclick="copyToClipboard('#p3')" id="p-btn3"   class="icon-docs" data-toggle="tooltip" data-placement="top"  data-original-title="Copiar"></i>
                </span>
                    <span id="p3"><?php echo $show->strClientesSocialFacebookUsuario;?></span></h5>
            </div>
            <h5 class="text-muted font-normal">Senha:</h5>
            <div class="alert alert-custom" role="alert">
                <h5 class="text-muted font-normal">
                <span class="pull-right ">
                    <i onclick="copyToClipboard('#p4')"  id="p-btn4" class="icon-docs"  data-toggle="tooltip" data-placement="top" data-original-title="Copiar"></i>
                </span>   <span id="p4"><?php echo $show->strClientesSocialFacebookSenha;?></span></h5>
            </div>
        <?php }?>
        <?php if($nivelLogado ==0){?>
            <h5 class="text-muted font-normal">Usuário:</h5>
            <div class="alert alert-custom " role="alert">
                <h5 class="text-muted font-normal">

                <span class="pull-right ">
                </span>
                    <span id="p3">Privado</span></h5>
            </div>
            <h5 class="text-muted font-normal">Senha:</h5>
            <div class="alert alert-custom" role="alert">
                <h5 class="text-muted font-normal">
                <span class="pull-right ">
                </span>   <span>Privado</span></h5>
            </div>
        <?php }?>


    </div>
</div>

