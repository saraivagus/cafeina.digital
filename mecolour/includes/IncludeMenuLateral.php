<div class="left side-menu">

    <div class="slimscroll-menu" id="remove-scroll">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="../index.php" class="logo">
                            <span>
                                <img src="../assets/images/cafeLogo.png" alt="" height="22">
                            </span>
                <i>
                    <img src="../assets/images/logo_sm.png" alt="" height="28">
                </i>
            </a>
        </div>

        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="../data/uploads/perfil/<?php echo $imagem?>" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
            </div>
            <h5><a href="#"><?php echo $nomeLogado ?></a> </h5>
            <p class="text-muted"><?php echo $cargoLogado ?></p>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <!--<li class="menu-title">Navigation</li>-->

                <li>
                    <a href="home.php">
                        <i class="fi-air-play"></i><span> Dashboard </span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fi-head"></i> <span> Leads </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="ViewLeadsNegocioProprio.php">Negócio Próprio</a></li>
                        <li><a href="ViewLeadsClienteFinal.php">Cliente Final</a></li>
                        <li><a href="ViewLeadsRevenda.php">Revenda</a></li>
                    </ul>

                </li>
                <li>
                    <a href="javascript: void(0);"><i class="fi-paper"></i> <span> Form </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="http://192.168.15.36:8080/form/" target="_blank">Visualizar</a></li>
                    </ul>

                </li>

            </ul>

        </div>
        <!-- Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>