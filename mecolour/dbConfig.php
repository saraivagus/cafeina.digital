<?php

try{
    $conexao = new PDO('mysql:host=localhost;dbname=mecolour', 'root', '');
    $conexao ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo 'ERROR:' . $e->getMessage();


};

foreach (glob("assets/css/*.css") as $css) {
    echo "<link type='text/css' rel='stylesheet' href='$css'>\n";
}

defined("SOCIAL_PATH")
or define("SOCIAL_PATH", realpath(dirname(__FILE__) . '/includes/IncludeSociais/'));

defined("INCLUDES_PATH")
or define("INCLUDES_PATH", realpath(dirname(__FILE__) . '/includes'));

defined("TEMPLATES_PATH")
or define("TEMPLATES_PATH", realpath(dirname(__FILE__) . '/templates'));

defined("CONTROLLERS_PATH")
or define("CONTROLLERS_PATH", realpath(dirname(__FILE__) . '/controllers'));

ini_set("error_reporting", "true");
error_reporting(E_ALL|E_STRCT);

?>