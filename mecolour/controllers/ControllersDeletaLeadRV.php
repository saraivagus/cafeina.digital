

<?php
// excluir post
if(isset($_GET['delete'])){
    $id_delete = $_GET['delete'];

    // exclui o registro

    $seleciona = "DELETE from dbformrv WHERE dbFormIDRv=:id_delete";
    try{
        $result = $conexao->prepare($seleciona);
        $result->bindParam('id_delete',$id_delete, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
            $usuarioDeletadoSucessoRV = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Usuário deletado com <strong>Sucesso!</strong>
                                        </div>';
        }else{
            $usuarioDeletadoErroRV = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Erro!</strong> Não foi possível excluir o usuario.
                                      </div>';
        }
    }catch (PDOWException $erro){ echo $erro;}



}

?>