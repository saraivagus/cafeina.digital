<?php

try{
    $conexao = new PDO('mysql:host=localhost;dbname=cafeinsys', 'phpmyadmin', 'DB13579**');
    $conexao ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo 'ERROR:' . $e->getMessage();

};

if(isset($_POST['cadastrar'])){

    $nome = trim(strip_tags($_POST['nome']));
    $usuario = trim(strip_tags($_POST['usuario']));
    $email = trim(strip_tags($_POST['email']));
    $senha = trim(strip_tags($_POST['senha']));

                    $insert = "INSERT INTO dbusers (nome, email, usuario, senha) VALUES (:nome, :email, :usuario, :senha)";
                    try{

                        $result = $conexao->prepare($insert);
                        $result->bindParam(':nome', $nome, PDO::PARAM_STR);
                        $result->bindParam(':email', $email, PDO::PARAM_STR);
                        $result->bindParam(':usuario', $usuario, PDO::PARAM_STR);
                        $result->bindParam(':senha', $senha, PDO::PARAM_STR);
                        $result->execute();
                        $contar = $result->rowCount();
                        if($contar>0){

                            {
                                $sucesso ='<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Usuário</strong> cadastrado com sucesso
                            </div>';
                            }
                        }else{
                            $erro ='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                 <strong>Erro</strong> ao cadastrar o usuário.
                          </div>';
                        }
                    }catch(PDOException $e){
                        echo $e;
                    }

                }else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>