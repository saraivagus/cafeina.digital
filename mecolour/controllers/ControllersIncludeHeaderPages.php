<?php
require_once("../dbConfig.php");
ob_start();
session_start();
if(!isset($_SESSION['usuariosistema']) && !isset($_SESSION['senhasistema'])){
    header("Location: ../index.php?acao=negado");exit;
}

include_once (CONTROLLERS_PATH . "/ControllersLogOut.php");


$usuarioLogado = $_SESSION['usuariosistema'];
$senhaLogado = $_SESSION['senhasistema'];

// seleciona a usuario logado

$selecionaLogado = "SELECT * FROM dbusers WHERE usuario=:usuarioLogado AND senha=:senhaLogado";
try{
    $result = $conexao->prepare($selecionaLogado);
    $result->bindParam('usuarioLogado',$usuarioLogado, PDO::PARAM_STR);
    $result->bindParam('senhaLogado',$senhaLogado, PDO::PARAM_STR);
    $result->execute();
    $contar = $result->rowCount();
    if($contar =1){
        $loop = $result->fetchAll();
        foreach ($loop as $show){
            $idLogado  = $show['strId'];
            $nomeLogado  = $show['nome'];
            $userLogado  = $show['usuario'];
            $emailLogado = $show['email'];
            $cargoLogado = $show['cargo'];
            $nivelLogado = $show['nivel'];
            $imagem = $show['imagem'];


        }
    }

}catch (PDOWException $erro){ echo $erro;}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Sistema de Gestão - Cafeína Digital</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- DataTables -->
    <link href="../plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="../plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="../plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Multi Item Selection examples -->
    <link href="../plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />+

    <!-- App favicon -->
    <link rel="shortcut icon" href="../assets/images/favicon.ico">
    <link href="../plugins/jquery-toastr/jquery.toast.min.css" rel="stylesheet" />
    <link href="../plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../plugins/tooltipster/tooltipster.bundle.min.css">
    <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />


    <!-- App css -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../plugins/custombox/css/custombox.min.css" rel="stylesheet">


    <script src="../assets/js/modernizr.min.js"></script>

</head>