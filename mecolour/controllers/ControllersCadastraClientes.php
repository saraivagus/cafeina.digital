<?php

if(isset($_POST['cadastrar'])){

    $strClientesEmpresa = trim(strip_tags($_POST['strClientesEmpresa']));
    $strClientesEmail = trim(strip_tags($_POST['strClientesEmail']));
    $strClientesTelefoneFixo = trim(strip_tags($_POST['strClientesTelefoneFixo']));
    $strClientesTelefoneMovel = trim(strip_tags($_POST['strClientesTelefoneMovel']));
    $strClientesCNPJ = trim(strip_tags($_POST['strClientesCNPJ']));
    $strClientesResponsavel = trim(strip_tags($_POST['strClientesResponsavel']));
    $strClientesEstado = trim(strip_tags($_POST['strClientesEstado']));
    $strClientesCidade = trim(strip_tags($_POST['strClientesCidade']));
    $strClientesEndereco = trim(strip_tags($_POST['strClientesEndereco']));
    $strClientesCEP = trim(strip_tags($_POST['strClientesCEP']));
    $strClientesSocialYouTubeEmail = trim(strip_tags($_POST['strClientesSocialYouTubeEmail']));
    $strClientesSocialYouTubeSenha   = trim(strip_tags($_POST['strClientesSocialYouTubeSenha']));
    $strClientesSocialFacebookUsuario  = trim(strip_tags($_POST['strClientesSocialFacebookUsuario']));
    $strClientesSocialFacebookSenha  = trim(strip_tags($_POST['strClientesSocialFacebookSenha']));
    $strClientesSocialInstagramUsuario  = trim(strip_tags($_POST['strClientesSocialInstagramUsuario']));
    $strClientesSocialInstagramSenha  = trim(strip_tags($_POST['strClientesSocialInstagramSenha']));
    $strClientesSocialAnalyticsUsuario = trim(strip_tags($_POST['strClientesSocialAnalyticsUsuario']));
    $strClientesSocialAnalyticsSenha  = trim(strip_tags($_POST['strClientesSocialAnalyticsSenha']));
    $strClientesSocialHospedagemTipo  = trim(strip_tags($_POST['strClientesSocialHospedagemTipo']));
    $strClientesSocialHospedagemEmail  = trim(strip_tags($_POST['strClientesSocialHospedagemEmail']));
    $strClientesSocialHospedagemSenha  = trim(strip_tags($_POST['strClientesSocialHospedagemSenha']));
    $strClientesSocialHospedagemLink  = trim(strip_tags($_POST['strClientesSocialHospedagemLink']));
    $strClientesSocialOutrosTitulo  = trim(strip_tags($_POST['strClientesSocialOutrosTitulo']));
    $strClientesSocialOutrosMensagem  = trim(strip_tags($_POST['strClientesSocialOutrosMensagem']));

    $insert = "INSERT INTO dbclientes (strClientesEmpresa, strClientesEmail, strClientesTelefoneFixo, strClientesTelefoneMovel, strClientesCNPJ, strClientesResponsavel, strClientesEndereco, strClientesCidade, strClientesEstado, strClientesCEP, strClientesSocialYouTubeEmail, strClientesSocialYouTubeSenha, strClientesSocialFacebookUsuario, strClientesSocialFacebookSenha, strClientesSocialInstagramUsuario, strClientesSocialInstagramSenha, strClientesSocialAnalyticsUsuario, strClientesSocialAnalyticsSenha, strClientesSocialHospedagemTipo, strClientesSocialHospedagemEmail, strClientesSocialHospedagemSenha, strClientesSocialHospedagemLink, strClientesSocialOutrosTitulo, strClientesSocialOutrosMensagem) VALUES (:strClientesEmpresa, :strClientesEmail, :strClientesTelefoneFixo, :strClientesTelefoneMovel, :strClientesCNPJ, :strClientesResponsavel, :strClientesEndereco, :strClientesCidade, :strClientesEstado, :strClientesCEP, :strClientesSocialYouTubeEmail, :strClientesSocialYouTubeSenha, :strClientesSocialFacebookUsuario, :strClientesSocialFacebookSenha, :strClientesSocialInstagramUsuario, :strClientesSocialInstagramSenha, :strClientesSocialAnalyticsUsuario, :strClientesSocialAnalyticsSenha, :strClientesSocialHospedagemTipo, :strClientesSocialHospedagemEmail, :strClientesSocialHospedagemSenha, :strClientesSocialHospedagemLink, :strClientesSocialOutrosTitulo, :strClientesSocialOutrosMensagem)";
    try{

        $result = $conexao->prepare($insert);
        $result->bindParam(':strClientesEmpresa', $strClientesEmpresa, PDO::PARAM_STR);
        $result->bindParam(':strClientesEmail', $strClientesEmail, PDO::PARAM_STR);
        $result->bindParam(':strClientesTelefoneFixo', $strClientesTelefoneFixo, PDO::PARAM_STR);
        $result->bindParam(':strClientesTelefoneMovel', $strClientesTelefoneMovel, PDO::PARAM_STR);
        $result->bindParam(':strClientesCNPJ', $strClientesCNPJ, PDO::PARAM_STR);
        $result->bindParam(':strClientesResponsavel', $strClientesResponsavel, PDO::PARAM_STR);
        $result->bindParam(':strClientesEndereco', $strClientesEndereco, PDO::PARAM_STR);
        $result->bindParam(':strClientesEstado', $strClientesEstado, PDO::PARAM_STR);
        $result->bindParam(':strClientesCidade', $strClientesCidade, PDO::PARAM_STR);
        $result->bindParam(':strClientesCEP', $strClientesCEP, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialYouTubeEmail', $strClientesSocialYouTubeEmail, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialYouTubeSenha', $strClientesSocialYouTubeSenha, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialFacebookUsuario', $strClientesSocialFacebookUsuario, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialFacebookSenha', $strClientesSocialFacebookSenha, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialInstagramUsuario', $strClientesSocialInstagramUsuario, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialInstagramSenha', $strClientesSocialInstagramSenha, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialAnalyticsUsuario', $strClientesSocialAnalyticsUsuario, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialAnalyticsSenha', $strClientesSocialAnalyticsSenha, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialHospedagemTipo', $strClientesSocialHospedagemTipo, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialHospedagemEmail', $strClientesSocialHospedagemEmail, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialHospedagemSenha', $strClientesSocialHospedagemSenha, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialHospedagemLink', $strClientesSocialHospedagemLink, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialOutrosTitulo', $strClientesSocialOutrosTitulo, PDO::PARAM_STR);
        $result->bindParam(':strClientesSocialOutrosMensagem', $strClientesSocialOutrosMensagem, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                $msgClientesSucesso = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Usuário</strong> cadastrado com sucesso
                            </div>'; header("Refresh:3");
            }
        }else{
            $msgClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                 <strong>Erro</strong> ao cadastrar o usuário.
                          </div>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>

