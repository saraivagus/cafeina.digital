<?php
require_once("dbConfig.php");

if(isset($_GET['acao'])){

    if(!isset($_POST['logar'])){
        $acao = $_GET['acao'];
        if($acao=='negado'){
            $erroLogar = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Erro ao acessar!</strong> <br>Você precisa estar logado p/ acessar o sistema.
                                    </div>';
        }
    }
}
if(isset($_POST['logar'])){

    //Recuperar Dados do Form
    $usuario = trim(strip_tags($_POST['usuario']));
    $senha = trim(strip_tags($_POST['senha']));

    //Selecionar Banco de Dados
    $select = "SELECT * from dbusers WHERE BINARY usuario=:usuario AND BINARY senha=:senha";

    try{
        $result = $conexao->prepare($select);
        $result->bindParam(':usuario', $usuario, PDO::PARAM_STR);
        $result->bindParam(':senha', $senha, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
            $usuario = $_POST['usuario'];
            $senha = $_POST['senha'];
            $_SESSION['usuariosistema'] = $usuario;
            $_SESSION['senhasistema'] = $senha;
            $logadoSucesso = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Você foi logado com sucesso!</strong> <br>Redirecionando para a página inicial.
                                    </div>';

            echo "<script type='text/javascript'>    
                setTimeout(function () {
                window.location.href = \"views/home.php?acao=welcome\"; 
            }, 2000);  </script>";

        }else{
            $logadoErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Erro ao Logar!</strong> Os Dados Estão Incorretos.
                                    </div>';
        }
    }catch(PDOException $e){
        echo $e;
    }
}//Se Clicar no Botão Entra no Sistema
?>