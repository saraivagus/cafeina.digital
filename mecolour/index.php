
<?php
ob_start();
session_start();
if(isset($_SESSION['usuariosistema']) && isset($_SESSION['senhasistema'])){
    header("Location: views/home.php");exit;
}
require_once("dbConfig.php");

require_once(TEMPLATES_PATH . "/TemplatesHeader.php");
include_once (CONTROLLERS_PATH . "/ControllersLogaUsuarioHome.php");


?>

    <body class="account-pages">

        <!-- Begin page -->
        <div class="accountbg" style="background: url('assets/images/bg-1.png');background-size: cover;"></div>

        <div class="wrapper-page account-page-full">

            <div class="card">
                <div class="card-block">

                    <div class="account-box">

                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-4">
                                <a href="#" class="text-success">
                                    <span><img src="assets/images/cafeLogo.png" alt="" height="26"></span>
                                </a>
                            </h2>
                            <?php echo $erroLogar; ?>
                            <?php echo $logadoSucesso; ?>
                            <?php echo $logadoErro; ?>
                            <form class="form-signin" action="#" method="post" enctype="multipart/form-data">

                                <div class="form-group m-b-20 row">
                                    <div class="col-12">
                                        <label for="email">Usuário</label>
                                        <input class="form-control" type="txt" id="usuario" name="usuario" required="" placeholder="Insira seu Usuário">
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
<!--                                        <a href="page-recoverpw.html" class="text-muted pull-right"><small>Forgot your password?</small></a>-->
                                        <label for="password">Senha</label>
                                        <input class="form-control" type="password" required="" id="senha" name="senha" placeholder="Insira sua Senha">
                                    </div>
                                </div>

<!--                                <div class="form-group row m-b-20">-->
<!--                                    <div class="col-12">-->
<!---->
<!--                                        <div class="checkbox checkbox-custom">-->
<!--                                            <input id="remember" type="checkbox" checked="">-->
<!--                                            <label for="remember">-->
<!--                                                Remember me-->
<!--                                            </label>-->
<!--                                        </div>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->

                                <div class="form-group row text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-custom waves-effect waves-light" type="submit" name="logar">Entrar</button>
                                    </div>
                                </div>

                            </form>

                            <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Não tem uma conta? <a href="RegistraUsuario.php" class="text-dark m-l-5"><b>Cadastrar</b></a></p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="m-t-40 text-center">
                <p class="account-copyright">2018 © Cafeína Digital - cafeina.digital</p>
            </div>

        </div>
            <?php
            require_once(TEMPLATES_PATH . "/TemplatesFooter.php");
            ?>


