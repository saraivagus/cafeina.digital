
<?php
// load up your config file
require_once("dbConfig.php");

require_once(TEMPLATES_PATH . "/TemplatesHeader.php");
include_once (CONTROLLERS_PATH . "/ControllersRegistraUsuarioHome.php");
?>

<body class="account-pages">

<!-- Begin page -->
<div class="accountbg" style="background: url('assets/images/bg-2.jpg');background-size: cover;"></div>

<div class="wrapper-page account-page-full">

    <div class="card">
        <div class="card-block">

            <div class="account-box">

                <div class="card-box p-5">
                    <h2 class="text-uppercase text-center pb-4">
                        <a href="index.html" class="text-success">
                            <span><img src="assets/images/cafeLogo.png" alt="" height="26"></span>
                        </a>
                    </h2>
                    <?php echo $sucesso; ?>
                    <?php echo $erro; ?>
                    <form  role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group row m-b-20">
                            <div class="col-12">
                                <label for="username">Nome</label>
                                <input class="form-control" type="text" id="nome" name="nome" required="" placeholder="João Kleber">
                            </div>
                        </div>

                        <div class="form-group row m-b-20">
                            <div class="col-12">
                                <label for="email">E-mail</label>
                                <input class="form-control" type="email" id="email" name="email" required="" placeholder="usuario@provedor.com">
                            </div>
                        </div>

                        <div class="form-group row m-b-20">
                            <div class="col-6">
                                <label for="usuario">Usuário</label>
                                <input class="form-control" type="text" id="usuario" name="usuario" required="" placeholder="joao.kleber">
                            </div>


                            <div class="col-6">
                                <label for="password">Senha</label>
                                <input class="form-control" type="password" required="" id="senha" name="senha" placeholder="Insira sua senha">
                            </div>
                        </div>

<!--                        <div class="form-group row m-b-20">-->
<!--                            <div class="col-12">-->
<!--                                <label for="imagem">Foto de Perfil</label>-->
<!--                                <input type="file" class="form-control" id="imagem" name="img[]">-->
<!--                            </div>-->
<!--                        </div>-->

<!--                        <div class="form-group row m-b-20">-->
<!--                            <div class="col-12">-->
<!---->
<!--                                <div class="checkbox checkbox-custom">-->
<!--                                    <input id="remember" type="checkbox" checked="">-->
<!--                                    <label for="remember">-->
<!--                                        I accept <a href="#" class="text-custom">Terms and Conditions</a>-->
<!--                                    </label>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->
<!--                        </div>-->

                        <div class="form-group row text-center m-t-10">
                            <div class="col-12">
                                <button class="btn btn-block btn-custom waves-effect waves-light" type="submit" name="cadastrar" id="cadastrar">Cadastrar</button>
                            </div>
                        </div>

                    </form>

                    <div class="row m-t-50">
                        <div class="col-sm-12 text-center">
                            <p class="text-muted">Já tem uma conta?  <a href="index.php" class="text-dark m-l-5"><b>Entrar</b></a></p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="m-t-40 text-center">
        <p class="account-copyright">2018 © Cafeína Digital - cafeina.digital</p>
    </div>

</div>
<?php
require_once(TEMPLATES_PATH . "/footer.php");
?>
