
<?php
require_once("../dbConfig.php");
include_once (CONTROLLERS_PATH . "/ControllersIncludeHeaderPages.php");

               // RECUPERA DADOS
               if(!isset($_GET['id'])){
                   header("Location: ViewClientesVisualizar.php"); exit;
               }
                   $id = $_GET['id'];
                   $select = "SELECT * from dbclientes WHERE strClientesID=:id";
                   $contagem =1;
                   try{
                     $result = $conexao->prepare($select);
                     $result->bindParam(':id', $id, PDO::PARAM_INT);
                     $result->execute();
                     $contar = $result->rowCount();
                     if($contar>0){
                       while($show = $result->FETCH(PDO::FETCH_OBJ)){
                            $strClientesEmpresa = $show->strClientesEmpresa;
                            $strClientesEmail = $show->strClientesEmail;
                            $strClientesTelefoneFixo = $show->strClientesTelefoneFixo;
                            $strClientesTelefoneMovel	 = $show->strClientesTelefoneMovel;
                            $strClientesCNPJ = $show->strClientesCNPJ;
                            $strClientesResponsavel = $show->strClientesResponsavel;
                            $strClientesEndereco = $show->strClientesEndereco;
                            $strClientesEstado = $show->strClientesEstado;
                            $strClientesCidade = $show->strClientesCidade;
                            $strClientesCEP = $show->strClientesCEP;
                            $strClientesSocialYouTubeEmail  = $show->strClientesSocialYouTubeEmail;
                            $strClientesSocialYouTubeSenha    = $show->strClientesSocialYouTubeSenha;
                            $strClientesSocialFacebookUsuario   = $show->strClientesSocialFacebookUsuario;
                            $strClientesSocialFacebookSenha   = $show->strClientesSocialFacebookSenha;
                            $strClientesSocialInstagramUsuario   = $show->strClientesSocialInstagramUsuario;
                            $strClientesSocialInstagramSenha   = $show->strClientesSocialInstagramSenha;
                            $strClientesSocialAnalyticsUsuario  = $show->strClientesSocialAnalyticsUsuario;
                            $strClientesSocialAnalyticsSenha   = $show->strClientesSocialAnalyticsSenha;
                            $strClientesSocialHospedagemTipo   = $show->strClientesSocialHospedagemTipo;
                            $strClientesSocialHospedagemEmail   = $show->strClientesSocialHospedagemEmail;
                            $strClientesSocialHospedagemSenha   = $show->strClientesSocialHospedagemSenha;
                            $strClientesSocialHospedagemLink   = $show->strClientesSocialHospedagemLink;
                            $strClientesSocialOutrosTitulo  = $show->strClientesSocialOutrosTitulo;
                            $strClientesSocialOutrosMensagem   = $show->strClientesSocialOutrosMensagem;

                       }
                     }else{
                       echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                         <strong>Aviso</strong> Não existem dados cadastrados com o id informado.
                                        </div>';exit;
                     }
                   }catch(PDOException $e){
                     $msgErroID = $e;
                   }
                                   // ATUALIZA
                          if(isset($_POST['atualizar'])){
                            $strClientesEmpresa  = trim(strip_tags($_POST['strClientesEmpresa']));
                            $strClientesEmail    = trim(strip_tags($_POST['strClientesEmail']));
                            $strClientesTelefoneFixo    = trim(strip_tags($_POST['strClientesTelefoneFixo']));
                            $strClientesTelefoneMovel    = trim(strip_tags($_POST['strClientesTelefoneMovel']));
                            $strClientesCNPJ    = trim(strip_tags($_POST['strClientesCNPJ']));
                            $strClientesResponsavel    = trim(strip_tags($_POST['strClientesResponsavel']));
                            $strClientesEndereco    = trim(strip_tags($_POST['strClientesEndereco']));
                            $strClientesEstado    = trim(strip_tags($_POST['strClientesEstado']));
                            $strClientesCidade    = trim(strip_tags($_POST['strClientesCidade']));
                            $strClientesCEP    = trim(strip_tags($_POST['strClientesCEP']));
                            $strClientesSocialYouTubeEmail = trim(strip_tags($_POST['strClientesSocialYouTubeEmail']));
                            $strClientesSocialYouTubeSenha   = trim(strip_tags($_POST['strClientesSocialYouTubeSenha']));
                            $strClientesSocialFacebookUsuario  = trim(strip_tags($_POST['strClientesSocialFacebookUsuario']));
                            $strClientesSocialFacebookSenha  = trim(strip_tags($_POST['strClientesSocialFacebookSenha']));
                            $strClientesSocialInstagramUsuario  = trim(strip_tags($_POST['strClientesSocialInstagramUsuario']));
                            $strClientesSocialInstagramSenha  = trim(strip_tags($_POST['strClientesSocialInstagramSenha']));
                            $strClientesSocialAnalyticsUsuario = trim(strip_tags($_POST['strClientesSocialAnalyticsUsuario']));
                            $strClientesSocialAnalyticsSenha  = trim(strip_tags($_POST['strClientesSocialAnalyticsSenha']));
                            $strClientesSocialHospedagemTipo  = trim(strip_tags($_POST['strClientesSocialHospedagemTipo']));
                            $strClientesSocialHospedagemEmail  = trim(strip_tags($_POST['strClientesSocialHospedagemEmail']));
                            $strClientesSocialHospedagemSenha  = trim(strip_tags($_POST['strClientesSocialHospedagemSenha']));
                            $strClientesSocialHospedagemLink  = trim(strip_tags($_POST['strClientesSocialHospedagemLink']));
                            $strClientesSocialOutrosTitulo  = trim(strip_tags($_POST['strClientesSocialOutrosTitulo']));
                            $strClientesSocialOutrosMensagem  = trim(strip_tags($_POST['strClientesSocialOutrosMensagem']));

                           $update = "UPDATE dbclientes SET strClientesEmpresa=:strClientesEmpresa, strClientesEmail=:strClientesEmail, strClientesTelefoneFixo=:strClientesTelefoneFixo, strClientesTelefoneMovel=:strClientesTelefoneMovel, strClientesCNPJ=:strClientesCNPJ, strClientesResponsavel=:strClientesResponsavel, strClientesEndereco=:strClientesEndereco, strClientesEstado=:strClientesEstado, strClientesCidade=:strClientesCidade, strClientesCEP=:strClientesCEP, strClientesSocialYouTubeEmail=:strClientesSocialYouTubeEmail, strClientesSocialYouTubeSenha=:strClientesSocialYouTubeSenha, strClientesSocialFacebookUsuario=:strClientesSocialFacebookUsuario, strClientesSocialFacebookSenha=:strClientesSocialFacebookSenha, strClientesSocialInstagramUsuario=:strClientesSocialInstagramUsuario, strClientesSocialInstagramSenha=:strClientesSocialInstagramSenha, strClientesSocialAnalyticsUsuario=:strClientesSocialAnalyticsUsuario, strClientesSocialAnalyticsSenha=:strClientesSocialAnalyticsSenha, strClientesSocialHospedagemTipo=:strClientesSocialHospedagemTipo, strClientesSocialHospedagemEmail=:strClientesSocialHospedagemEmail, strClientesSocialHospedagemSenha=:strClientesSocialHospedagemSenha, strClientesSocialHospedagemLink=:strClientesSocialHospedagemLink, strClientesSocialOutrosTitulo=:strClientesSocialOutrosTitulo, strClientesSocialOutrosMensagem=:strClientesSocialOutrosMensagem WHERE strClientesID=:id";

                           try{
                           $result = $conexao->prepare($update);
                           $result->bindParam(':id', $id, PDO::PARAM_INT);
                           $result->bindParam(':strClientesEmpresa', $strClientesEmpresa, PDO::PARAM_STR);
                           $result->bindParam(':strClientesEmail', $strClientesEmail, PDO::PARAM_STR);
                           $result->bindParam(':strClientesTelefoneFixo', $strClientesTelefoneFixo, PDO::PARAM_STR);
                           $result->bindParam(':strClientesTelefoneMovel', $strClientesTelefoneMovel, PDO::PARAM_STR);
                           $result->bindParam(':strClientesCNPJ', $strClientesCNPJ, PDO::PARAM_STR);
                           $result->bindParam(':strClientesResponsavel', $strClientesResponsavel, PDO::PARAM_STR);
                           $result->bindParam(':strClientesEndereco', $strClientesEndereco, PDO::PARAM_STR);
                           $result->bindParam(':strClientesEstado', $strClientesEstado, PDO::PARAM_STR);
                           $result->bindParam(':strClientesCidade', $strClientesCidade, PDO::PARAM_STR);
                           $result->bindParam(':strClientesCEP', $strClientesCEP, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialYouTubeEmail', $strClientesSocialYouTubeEmail, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialYouTubeSenha', $strClientesSocialYouTubeSenha, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialFacebookUsuario', $strClientesSocialFacebookUsuario, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialFacebookSenha', $strClientesSocialFacebookSenha, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialInstagramUsuario', $strClientesSocialInstagramUsuario, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialInstagramSenha', $strClientesSocialInstagramSenha, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialAnalyticsUsuario', $strClientesSocialAnalyticsUsuario, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialAnalyticsSenha', $strClientesSocialAnalyticsSenha, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialHospedagemTipo', $strClientesSocialHospedagemTipo, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialHospedagemEmail', $strClientesSocialHospedagemEmail, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialHospedagemSenha', $strClientesSocialHospedagemSenha, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialHospedagemLink', $strClientesSocialHospedagemLink, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialOutrosTitulo', $strClientesSocialOutrosTitulo, PDO::PARAM_STR);
                           $result->bindParam(':strClientesSocialOutrosMensagem', $strClientesSocialOutrosMensagem, PDO::PARAM_STR);

                           $result->execute();
                           $contar = $result->rowCount();
                           if($contar>0){

                             $msgAtualizaClientesSucesso =  '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                 <strong>Sucesso.</strong> Os dados do usuário foram atualizados
                                           </div>';
                             }else{
                               $msgAtualizaClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               <strong>Erro!</strong> Não foi possível atualizar os dados deste usuário, tente novamente.
                                         </div>';
                             }
                             }catch(PDOException $e){
                             $error = $e;
                             }



                              }

                           ?>
<div id="wrapper">
    <?php include_once (INCLUDES_PATH . "/IncludeMenuLateral.php"); ?>
    <div class="content-page">
        <?php include_once (INCLUDES_PATH . "/IncludeTopBar.php"); ?>
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Cadastro de Clientes</h4>
                            <?php echo $error; ?>
                            <?php echo $msgErroID; ?>
                            <?php echo $msgAtualizaClientesSucesso; ?>
                            <?php echo $msgAtualizaClientesErro; ?>
                            <form  role="form" class="form-signin" action="#" method="post">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="strClientesEmpresa" class="col-form-label">Empresa</label>
                                    <input type="text" class="form-control" id="strClientesEmpresa" name="strClientesEmpresa" placeholder="Insira o nome da Empresa" value="<?php echo $strClientesEmpresa;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="strClientesEmail" class="col-form-label">E-mail</label>
                                    <input type="email" class="form-control" id="strClientesEmail" name="strClientesEmail" placeholder="Insira o E-mail"  value="<?php echo $strClientesEmail;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="strClientesTelefoneFixo" class="col-form-label">Telefone Fixo</label>
                                    <input  type="text" class="form-control" id="strClientesTelefoneFixo" name="strClientesTelefoneFixo" placeholder="Insira o Telefone Fixo" data-mask="(99) 99999-999"  value="<?php echo $strClientesTelefoneFixo;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="strClientesTelefoneMovel" class="col-form-label">Telefone Móvel</label>
                                    <input type="text" class="form-control" id="strClientesTelefoneMovel" name="strClientesTelefoneMovel" placeholder="Insira o Telefone Móvel" data-mask="(99) 99999-9999"  value="<?php echo $strClientesTelefoneMovel;?>">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="strClientesCNPJ" class="col-form-label">CNPJ</label>
                                    <input type="text" data-mask="99.999.999/9999-99" class="form-control" id="strClientesCNPJ" name="strClientesCNPJ" placeholder="Insira o CNPJ do Cliente"  value="<?php echo $strClientesCNPJ;?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="strClientesResponsavel" class="col-form-label">Responsável</label>
                                    <input type="text" class="form-control" id="strClientesResponsavel" name="strClientesResponsavel" placeholder="Insira o Nome do Responsável"  value="<?php echo $strClientesResponsavel;?>">
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="strClientesEndereco" class="col-form-label">Endereço</label>
                                <input  type="text" class="form-control" id="strClientesEndereco" name="strClientesEndereco" placeholder="Insira o Endereço"  value="<?php echo $strClientesEndereco;?>">
                            </div>



                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="strClientesEstado" class="col-form-label">Estado</label>
                                    <select id="estados" name="strClientesEstado" class="form-control" placeholder="<?php echo $strClientesEstado;?>"></select>
                                    <option value="<?php echo $strClientesEstado;?>"></option>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="strClientesCidade" class="col-form-label">Cidade</label>
                                    <select id="cidades" name="strClientesCidade" class="form-control" placeholder="<?php echo $strClientesCidade;?>"></select>
                                    <option value="<?php echo $strClientesCidade;?>"></option>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="strClientesCEP" class="col-form-label">CEP</label><?php echo $strClientesSocialYouTubeEmail;?>
                                    <input  type="text" class="form-control" id="strClientesCEP" name="strClientesCEP" data-mask="99999-999" placeholder="Insira o CEP"   value="<?php echo $strClientesCEP;?>">
                                </div>

                            </div>
                                <div class="form-row">

                                    <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisEdita/EditaRedeSocialYoutube.php"); ?>

                                    <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisEdita/EditaRedeSocialFacebook.php"); ?>

                                    <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisEdita/EditaRedeSocialInstagram.php"); ?>

                                    <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisEdita/EditaRedeSocialAnalytics.php"); ?>

                                    <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisEdita/EditaRedeSocialHospedagem.php"); ?>

                                    <?php include_once (SOCIAL_PATH . "/DadosRedesSociaisEdita/EditaRedeSocialOutros.php"); ?>

                                </div>
                                <button type="submit" name="atualizar" class="btn btn-primary">Atualizar</button>
                                <a href="ViewClientesVisualizar.php" class="btn btn-success waves-light waves-effect">Voltar</a>
                            </form>


                        </div>
                    </div>
                </div>
                <!-- end row -->


            </div> <!-- content -->
        </div>
    </div>
</div>
<?php include_once (CONTROLLERS_PATH . "/ControllersIncludeFooterPages.php"); ?>
</body>
</html>