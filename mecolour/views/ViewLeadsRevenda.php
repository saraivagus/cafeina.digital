<?php
require_once("../dbConfig.php");
include_once (CONTROLLERS_PATH . "/ControllersIncludeHeaderPages.php");
?>
<body>
<div id="wrapper">
    <?php include_once (INCLUDES_PATH . "/IncludeMenuLateral.php"); ?>
    <div class="content-page">
        <?php include_once (INCLUDES_PATH . "/IncludeTopBar.php"); ?>
        <?php
        // excluir post
        if(isset($_GET['delete'])){
            $id_delete = $_GET['delete'];

            // exclui o registro

            $seleciona = "DELETE from dbformrv WHERE dbFormIDRv=:id_delete";
            try{
                $result = $conexao->prepare($seleciona);
                $result->bindParam('id_delete',$id_delete, PDO::PARAM_STR);
                $result->execute();
                $contar = $result->rowCount();
                if($contar>0){
                    $usuarioDeletadoSucessoRV = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Usuário deletado com <strong>Sucesso!</strong>
                                        </div>';
                }else{
                    $usuarioDeletadoErroRV = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Erro!</strong> Não foi possível excluir o usuario.
                                      </div>';
                }
            }catch (PDOWException $erro){ echo $erro;}


        }

        ?>
        <div class="content">
            <div class="container-fluid">

                <div class="row">

                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Leads Cadastrados <span class="text-pink">Revendedores</span></h4>
                            <a href="ViewClientesExportarRevenda.php" class="btn btn-sm btn-warning waves-light waves-effect pull-right">Exportar Leads</a>

                            <?php echo $usuarioDeletadoSucessoRV; ?>
                            <?php echo $usuarioDeletadoErroRV; ?>

                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-12 text-sm-center form-inline">
                                        <div class="form-group mr-2">
                                            <select id="demo-foo-filter-status" class="custom-select">
                                                <option value="">Ver Todos</option>
                                                <option value="ativo">Ativo</option>
                                                <option value="desativado">Desativado</option>
                                                <option value="suspenso">Suspensos</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <table id="demo-foo-filtering" class="table table-striped table-bordered toggle-circle m-b-0" data-page-size="7">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Estado</th>
                                    <th>Cidade</th>
                                    <th >Configurações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(empty($_GET['pg'])){}
                                else{ $pg = $_GET['pg'];
                                    if(!is_numeric($pg)){
                                        echo '<script language="JavaScript">
                                           location.href=" ViewClientesVisualizar.php"; </script>';
                                    }

                                }
                                if(isset($pg)){ $pg = $_GET['pg'];}else{ $pg = 1;}

                                $quantidade = 20;
                                $inicio = ($pg*$quantidade) - $quantidade;
                                $select = "SELECT * from dbformrv ORDER BY dbFormIDRv DESC LIMIT $inicio, $quantidade";
                                $contagem =1;
                                try {
                                    $result = $conexao->prepare($select);
                                    $result->execute();
                                    $contar = $result->rowCount();
                                    if($contar>0){
                                        while($show = $result->FETCH(PDO::FETCH_OBJ)){

                                            $date = date_create($show->strClientesDataCadastro);
                                            $date = date_format($date, 'd-m-Y');
                                            $dateToday = date('d-m-Y', strtotime("-1 days"));

                                            ?>
                                            <tr>
                                                <td>

                                                    <?php if($date > $dateToday){ ?>
                                                        <span class="badge badge-pink">Novo</span>
                                                    <?php } ?>
                                                    <?php echo $show->strNome;?>
                                                </td>
                                                <td><?php echo $show->strEmail;?></td>
                                                <td><?php echo $show->strEstado;?></td>
                                                <td><?php echo $show->strCidade;?></td>
                                                <td><a href="ViewLeadsRevenda.php?pg=<?php echo $pg;?>&delete=<?php echo $show->dbFormIDRv;?>" onClick="return confirm('Deseja realmente excluir este usuário ?')"  class="icon-user-unfollow btn btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deletar Usuário"> </a>
                                                    <a href="ViewLeadsRevendaUnico.php?id=<?php echo $show->dbFormIDRv;?>" class="icon-eye btn btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar Usuário"> </a>
                                                    <a href="ViewLeadsRevendaUnico.php?id=<?php echo $show->dbFormIDRv;?>" class="icon-pencil btn btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar Usuário">
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                    }else{
                                        echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Desculpe, não existem dados cadastrados no momento !
                                        </div>';
                                    }
                                }catch(PDOException $e){
                                    echo $e;
                                }
                                ?>

                                </tbody>
                                <tfoot>
                                <tr class="active">
                                    <td colspan="6">
                                        <div class="text-right">
                                            <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10 m-b-0"></ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<?php include_once (CONTROLLERS_PATH . "/ControllersIncludeFooterPages.php"); ?>
</body>
</html>