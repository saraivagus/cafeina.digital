
<?php
// load up your config file
require_once("../dbConfig.php");
include_once (CONTROLLERS_PATH . "/ControllersIncludeHeaderPages.php");

if(!isset($_GET['id'])){
header("Location: ViewClientesVisualizar.php");
exit; }
    $id = $_GET['id'];
    $select = "SELECT * FROM dbform WHERE dbFormID=:id";
    $contagem =1;
            try {
            $result = $conexao->prepare($select);
            $result->bindParam(':id',$id, PDO::PARAM_INT);
            $result->execute();
            $contar = $result->rowCount();
                if($contar>0){
                while($show = $result->FETCH(PDO::FETCH_OBJ)){
                    $dbFormID = $show->dbFormID;
                    $strNome = $show->strNome;
                    $strEmail = $show->strEmail;
                    $strEstado = $show->strEstado;
                    $strCidade = $show->strCidade;
                    $strCheckboxNP = $show->strCheckboxNP;
                    $strCheckboxCF = $show->strCheckboxCF;
                    $strCheckboxRV = $show->strCheckboxRV;
                    $dbFormOqueBuscaNP = $show->dbFormOqueBuscaNP;
                    $dbFormConheceMecolourNP = $show->dbFormConheceMecolourNP;
                    $dbFormComoSoubeNP = $show->dbFormComoSoubeNP;
                    $dbFormEstaBuscandoAlgoParaInvestirNP = $show->dbFormEstaBuscandoAlgoParaInvestirNP;
                    $dbFormNegocioProprioNP = $show->dbFormNegocioProprioNP;
                    $dbFormPossuiDuvidasEmRelacaoAoNegocioNP = $show->dbFormPossuiDuvidasEmRelacaoAoNegocioNP;
?>
<div id="wrapper">
    <?php include_once (INCLUDES_PATH . "/IncludeMenuLateral.php"); ?>
    <div class="content-page">
        <?php include_once (INCLUDES_PATH . "/IncludeTopBar.php"); ?>
<div class="content">
    <div class="container-fluid">

        <div class="row">

            <div class="col-md-12">
                <div class="card-box">
                    <a href="ViewClientesVisualizarUnicoEditar.php?id=<?php echo $show->dbFormID;?>" class="btn btn-sm btn-warning waves-light waves-effect pull-right">Editar Usuário</a>
                    <h4 class="m-t-0 header-title">Cadastro de Leads</h4>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="strClientesEmpresa" class="col-form-label">Nome</label>
                                <input  class="form-control"  disabled value="<?php echo $show->strNome;?>">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="strClientesEmail" class="col-form-label">E-mail</label>
                                <input class="form-control"  disabled value="<?php echo $show->strEmail;?>">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="strClientesTelefoneFixo" class="col-form-label">Estado</label>
                                <input  class="form-control" disabled value="<?php echo $show->strEstado;?>">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="strClientesTelefoneFixo" class="col-form-label">Cidade</label>
                                <input  class="form-control" disabled value="<?php echo $show->strCidade;?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="strClientesCNPJ" class="col-form-label">Tipo de Cliente</label>
                                <input class="form-control" disabled value="<?php echo $show->strCheckboxNP;?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="strClientesResponsavel" class="col-form-label">O que busca na feira?</label>
                                <input class="form-control" disabled value="<?php echo $show->dbFormOqueBuscaNP;?>">
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="strClientesEndereco" class="col-form-label">Conhece a Mecolour?</label>
                                <input  class="form-control"  disabled value="<?php echo $show->dbFormConheceMecolourNP;?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="strClientesCEP" class="col-form-label">Como Soube da Mecolour</label>
                                <input  class="form-control"  disabled value="<?php echo $show->dbFormComoSoubeNP;?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="strClientesEstado" class="col-form-label">Está buscando algo para investir?</label>
                                <input  class="form-control"  disabled value="<?php echo $show->dbFormEstaBuscandoAlgoParaInvestirNP;?>">
                            </div>


                        </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="strClientesEndereco" class="col-form-label">Tipo de investimento:</label>
                            <input  class="form-control"  disabled value="<?php echo $show->dbFormNegocioProprioNP;?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="strClientesCEP" class="col-form-label">Dúvidas em relação ao negócio:</label>
                            <input  class="form-control"  disabled value="<?php echo $show->dbFormPossuiDuvidasEmRelacaoAoNegocioNP;?>">
                        </div>



                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title">Cadastro de Leads</h4>


                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Estado</th>
                            <th>Cidade</th>
                            <th>Tipo</th>
                            <th>O que busca?</th>
                            <th>Como Soube</th>
                            <th>Conhece?</th>
                            <th>Quer investir?</th>
                            <th>Tipo de investimento:</th>
                            <th>Dúvidas:</th>
                        </tr>
                        </thead>


                        <tbody>
                        <tr>
                            <td><?php echo $show->strNome;?></td>
                            <td><?php echo $show->strEmail;?></td>
                            <td><?php echo $show->strEstado;?></td>
                            <td><?php echo $show->strCidade;?></td>
                            <td><?php echo $show->strCheckboxNP;?></td>
                            <td><?php echo $show->dbFormOqueBuscaNP;?></td>
                            <td><?php echo $show->dbFormComoSoubeNP;?></td>
                            <td><?php echo $show->dbFormConheceMecolourNP;?></td>
                            <td><?php echo $show->dbFormEstaBuscandoAlgoParaInvestirNP;?></td>
                            <td><?php echo $show->dbFormNegocioProprioNP;?></td>
                            <td><?php echo $show->dbFormPossuiDuvidasEmRelacaoAoNegocioNP;?></td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>



        </div>
        <a href="ViewClientesVisualizar.php" class="btn btn-success waves-light waves-effect">Voltar</a>

        <!-- end row -->
        <?php
        }
        }else{
            echo '<div class="alert media fade in alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                          <strong>Aviso!</strong> Ainda não existem posts cadastrados. Clique <a href="cadastraPostVini.php">aqui</a> para cadastrar.
                                                    </div>';
        }
        }catch(PDOException $e){
            echo $e;
        }
        ?>

    </div> <!-- content -->
</div>
    </div>
</div>
    <?php include_once (CONTROLLERS_PATH . "/ControllersIncludeFooterPages.php"); ?>
    </body>
    </html>