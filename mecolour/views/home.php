<?php
require_once("../dbConfig.php");
include_once (CONTROLLERS_PATH . "/ControllersIncludeHeaderPages.php");
?>

<body>

<div id="wrapper">
    <?php include_once (INCLUDES_PATH . "/IncludeMenuLateral.php"); ?>
    <div class="content-page">
        <?php include_once (INCLUDES_PATH . "/IncludeTopBar.php"); ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
        <?php include_once (INCLUDES_PATH . "/IncludeHomeMainContent.php"); ?>
        <?php include_once (INCLUDES_PATH . "/IncludeHomeMainClienteFinal.php"); ?>
        <?php include_once (INCLUDES_PATH . "/IncludeHomeMainRevenda.php"); ?>
                </div>
            </div>
        </div>
    </div>

</div>
    <?php include_once (CONTROLLERS_PATH . "/ControllersIncludeFooterPages.php"); ?>
</body>
</html>