<?php
require_once("../dbConfig.php");
include_once (CONTROLLERS_PATH . "/ControllersIncludeHeaderPages.php");
?>



<body>

<div id="wrapper">
    <?php include_once (INCLUDES_PATH . "/IncludeMenuLateral.php"); ?>
    <div class="content-page">
        <?php include_once (INCLUDES_PATH . "/IncludeTopBar.php"); ?>
        <?php include_once(INCLUDES_PATH . "/IncludeClientesCadastraContent.php"); ?>
    </div>

</div>
<?php include_once (CONTROLLERS_PATH . "/ControllersIncludeFooterPages.php"); ?>


</body>
</html>