<?php
require_once("../dbConfig.php");
include_once (CONTROLLERS_PATH . "/ControllersIncludeHeaderPages.php");
?>
<body>
<div id="wrapper">
    <?php include_once (INCLUDES_PATH . "/IncludeMenuLateral.php"); ?>
    <div class="content-page">
        <?php include_once (INCLUDES_PATH . "/IncludeTopBar.php"); ?>



        <div class="content">
            <div class="container-fluid">

                <div class="row">

                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title">Cadastro de Leads</h4>


                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Estado</th>
                                    <th>Cidade</th>
                                    <th>Tipo</th>
                                    <th>O que busca?</th>
                                    <th>Como Soube</th>
                                    <th>Conhece?</th>
                                    <th>Quer investir?</th>
                                    <th>Tipo de investimento:</th>
                                    <th>Dúvidas:</th>
                                </tr>
                                </thead>


                                <tbody>
                                <?php
                                if(empty($_GET['pg'])){}
                                else{ $pg = $_GET['pg'];
                                    if(!is_numeric($pg)){
                                        echo '<script language="JavaScript">
                                           location.href=" ViewClientesVisualizar.php"; </script>';
                                    }

                                }
                                if(isset($pg)){ $pg = $_GET['pg'];}else{ $pg = 1;}

                                $quantidade = 20;
                                $inicio = ($pg*$quantidade) - $quantidade;
                                $select = "SELECT * from dbform ORDER BY dbFormID DESC LIMIT $inicio, $quantidade";
                                $contagem =1;
                                try {
                                $result = $conexao->prepare($select);
                                $result->execute();
                                $contar = $result->rowCount();
                                if($contar>0){
                                while($show = $result->FETCH(PDO::FETCH_OBJ)){

                                $date = date_create($show->strClientesDataCadastro);
                                $date = date_format($date, 'd-m-Y');
                                $dateToday = date('d-m-Y', strtotime("-1 days"));

                                ?>
                                <tr>
                                    <td><?php echo $show->strNome;?></td>
                                    <td><?php echo $show->strEmail;?></td>
                                    <td><?php echo $show->strEstado;?></td>
                                    <td><?php echo $show->strCidade;?></td>
                                    <td><?php echo $show->strCheckboxNP;?></td>
                                    <td><?php echo $show->dbFormOqueBuscaNP;?></td>
                                    <td><?php echo $show->dbFormComoSoubeNP;?></td>
                                    <td><?php echo $show->dbFormConheceMecolourNP;?></td>
                                    <td><?php echo $show->dbFormEstaBuscandoAlgoParaInvestirNP;?></td>
                                    <td><?php echo $show->dbFormNegocioProprioNP;?></td>
                                    <td><?php echo $show->dbFormPossuiDuvidasEmRelacaoAoNegocioNP;?></td>

                                </tr>
                                    <?php
                                }
                                }else{
                                    echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Desculpe, não existem dados cadastrados no momento !
                                        </div>';
                                }
                                }catch(PDOException $e){
                                    echo $e;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>

<?php include_once (CONTROLLERS_PATH . "/ControllersIncludeFooterPages.php"); ?>
</body>
</html>