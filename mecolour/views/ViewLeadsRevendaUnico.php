
<?php
// load up your config file
require_once("../dbConfig.php");
include_once (CONTROLLERS_PATH . "/ControllersIncludeHeaderPages.php");

if(!isset($_GET['id'])){
    header("Location: ViewClientesVisualizar.php");
    exit; }
$id = $_GET['id'];
$select = "SELECT * FROM dbformrv WHERE dbFormIDRv=:id";
$contagem =1;
try {
$result = $conexao->prepare($select);
$result->bindParam(':id',$id, PDO::PARAM_INT);
$result->execute();
$contar = $result->rowCount();
if($contar>0){
while($show = $result->FETCH(PDO::FETCH_OBJ)){
$dbFormIDRv = $show->dbFormIDRv;
$strNome = $show->strNome;
$strEmail = $show->strEmail;
$strEstado = $show->strEstado;
$strCidade = $show->strCidade;
$strCheckboxRV = $show->strCheckboxRV;
$dbFormOqueBuscaRV = $show->dbFormOqueBuscaRV;
$dbFormConheceMecolourRV = $show->dbFormConheceMecolourRV;
$dbFormComoSoubeRV = $show->dbFormComoSoubeRV;
$dbFormNegocioProprioRVF = $show->dbFormNegocioProprioRV;
$dbFormEquipamentoRV = $show->dbFormEquipamentoRV;
$dbFormEquipamentoRVOutro = $show->dbFormEquipamentoRVOutro;
$dbFormEquipamentoRVMarca = $show->dbFormEquipamentoRVMarca;
$dbFormEquipamentoPapelGramaturaRV = $show->dbFormEquipamentoPapelGramaturaRV;
$dbFormEquipamentoPapelMarcaRV = $show->dbFormEquipamentoPapelMarcaRV;

?>
<div id="wrapper">
    <?php include_once (INCLUDES_PATH . "/IncludeMenuLateral.php"); ?>
    <div class="content-page">
        <?php include_once (INCLUDES_PATH . "/IncludeTopBar.php"); ?>
        <div class="content">
            <div class="container-fluid">

                <div class="row">

                    <div class="col-md-12">
                        <div class="card-box">
                            <a href="ViewClientesVisualizarUnicoEditar.php?id=<?php echo $show->dbFormIDRv;?>" class="btn btn-sm btn-warning waves-light waves-effect pull-right">Editar Usuário</a>
                            <h4 class="m-t-0 header-title">Cadastro de Leads</h4>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="strNome" class="col-form-label">Nome</label>
                                    <input  class="form-control"  disabled value="<?php echo $show->strNome;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="strEmail" class="col-form-label">E-mail</label>
                                    <input class="form-control"  disabled value="<?php echo $show->strEmail;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="strEstado" class="col-form-label">Estado</label>
                                    <input  class="form-control" disabled value="<?php echo $show->strEstado;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="strCidade" class="col-form-label">Cidade</label>
                                    <input  class="form-control" disabled value="<?php echo $show->strCidade;?>">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="strCheckboxRV" class="col-form-label">Tipo de Cliente</label>
                                    <input class="form-control" disabled value="<?php echo $show->strCheckboxRV;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="dbFormOqueBuscaRV" class="col-form-label">O que busca na feira?</label>
                                    <input class="form-control" disabled value="<?php echo $show->dbFormOqueBuscaRV;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="dbFormConheceMecolourRV" class="col-form-label">Conhece a Mecolour?</label>
                                    <input  class="form-control"  disabled value="<?php echo $show->dbFormConheceMecolourRV;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="dbFormComoSoubeRV" class="col-form-label">Como Soube da Mecolour</label>
                                    <input  class="form-control"  disabled value="<?php echo $show->dbFormComoSoubeRV;?>">
                                </div>

                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="dbFormNegocioProprioRV" class="col-form-label">Tipo de investimento:</label>
                                    <input  class="form-control"  disabled value="<?php echo $show->dbFormNegocioProprioRV;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="dbFormEquipamentoRV" class="col-form-label">Equipamento que usa:</label>
                                    <input  class="form-control"  disabled value="<?php echo $show->dbFormEquipamentoRV;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="dbFormEquipamentoRVOutro" class="col-form-label">Equipamento que usa (Outro)</label>
                                    <input  class="form-control"  disabled value="<?php echo $show->dbFormEquipamentoRVOutro;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="dbFormEquipamentoRVMarca" class="col-form-label">Equipamento que usa (Marca)</label>
                                    <input  class="form-control"  disabled value="<?php echo $show->dbFormEquipamentoRVMarca;?>">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="dbFormEquipamentoPapelGramaturaRV" class="col-form-label">Papel que usa:</label>
                                    <input  class="form-control"  disabled value="<?php echo $show->dbFormEquipamentoPapelGramaturaRV;?>">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="dbFormEquipamentoPapelMarcaRV" class="col-form-label">Papel que usa (Marca)</label>
                                    <input  class="form-control"  disabled value="<?php echo $show->dbFormEquipamentoPapelMarcaRV;?>">
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title">Cadastro de Leads</h4>


                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Estado</th>
                                    <th>Cidade</th>
                                    <th>Tipo de Cliente</th>
                                    <th>O que busca?</th>
                                    <th>Conhece?</th>
                                    <th>Como Soube</th>
                                    <th>Tipo de Investimento:</th>
                                    <th>Equipamento</th>
                                    <th>Equipamento  ( Outro )</th>
                                    <th>Equipamento( Marca )</th>
                                    <th>Papel Gramatura</th>
                                    <th>Papel Marca</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th><?php echo $show->strNome;?></th>
                                    <th><?php echo $show->strEmail;?></th>
                                    <th><?php echo $show->strEstado;?></th>
                                    <th><?php echo $show->strCidade;?></th>
                                    <th><?php echo $show->strCheckboxRV;?></th>
                                    <th><?php echo $show->dbFormOqueBuscaRV;?></th>
                                    <th><?php echo $show->dbFormConheceMecolourRV;?></th>
                                    <th><?php echo $show->dbFormComoSoubeRV;?></th>
                                    <th><?php echo $show->dbFormNegocioProprioRV;?></th>
                                    <th><?php echo $show->dbFormEquipamentoRV;?></th>
                                    <th><?php echo $show->dbFormEquipamentoRVOutro;?></th>
                                    <td><?php echo $show->dbFormEquipamentoRVMarca;?></td>
                                    <th><?php echo $show->dbFormEquipamentoPapelGramaturaRV;?></th>
                                    <th><?php echo $show->dbFormEquipamentoPapelMarcaRV;?></th>



                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>



                </div>
                <a href="ViewClientesVisualizar.php" class="btn btn-success waves-light waves-effect">Voltar</a>

                <!-- end row -->
                <?php
                }
                }else{
                    echo '<div class="alert media fade in alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                          <strong>Aviso!</strong> Ainda não existem posts cadastrados. Clique <a href="cadastraPostVini.php">aqui</a> para cadastrar.
                                                    </div>';
                }
                }catch(PDOException $e){
                    echo $e;
                }
                ?>

            </div> <!-- content -->
        </div>
    </div>
</div>
<?php include_once (CONTROLLERS_PATH . "/ControllersIncludeFooterPages.php"); ?>
</body>
</html>